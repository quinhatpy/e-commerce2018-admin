import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { SET_GALLERY_ITEM, SET_GALLERY_LIST } from '../constants/actionType';
import api from '../utils/axios';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import Toast from '../utils/toast';
import { resetAlert, setAlert } from './actions';

export const fetchGalleriesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`galleries?${queryParams}`)
            .then(res => {
                dispatch(setGalleryList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! '+ err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createGalleryRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('galleries', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                // history.push(linkConfig.BANNER_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchGalleryRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('galleries/' + id)
            .then(res => {
                dispatch(setGallery(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! '+ err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateGalleryRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('galleries/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            // history.push(linkConfig.BANNER_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }
        })
}

export const deleteGalleryRequest = (id, offset, limit) => dispatch => {
        dispatch(showLoading());
        api.delete('galleries/' + id)
            .then(res => {
                dispatch(hideLoading());
                let queryParams = history.location.search;
                let params = queryString.parse(queryParams);
                dispatch(fetchGalleriesRequest(offset, limit, params));
                Toast.success(res.data.message);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                if (err.response)
                    Toast.error('Error occurred! '+ err.response.data.message);
            })
}


export const setGalleryList = (data) => ({ type: SET_GALLERY_LIST, data });
export const setGallery = (data) => ({ type: SET_GALLERY_ITEM, data });