import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { SET_PRODUCT_ITEM, SET_PRODUCT_LIST, SET_PRODUCT_TOTAL } from '../constants/actionType';
import * as linkConfig from '../constants/link';
import api from '../utils/axios';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import Toast from '../utils/toast';
import { resetAlert, setAlert } from './actions';

export const fetchProductsRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`products?${queryParams}`)
            .then(res => {
                dispatch(setProductList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const searchProductsRequest = (params = {}) => {
    return dispatch => {
        params.limit = 100;
        let queryParams = queryString.stringify(params);

        api.get(`products?${queryParams}`)
            .then(res => {
                dispatch(setProductList(res.data.result));
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createProductRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('products', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.PRODUCT_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchProductRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('products/' + id)
            .then(res => {
                dispatch(setProduct(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateProductRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('products/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.PRODUCT_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteProductRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('products/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchProductsRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const deleteProductImageRequest = id => dispatch => {
    dispatch(showLoading());
    api.delete('product-images/' + id)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const resetProduct = () => dispatch => {
    dispatch(setProduct({}))
}

export const fetchProductTotalRequest = () => {
    return dispatch => {
        dispatch(showLoading());
        api.get('products-count')
            .then(res => {
                dispatch(setProductTotal(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const setProductList = (data) => ({ type: SET_PRODUCT_LIST, data });
export const setProduct = (data) => ({ type: SET_PRODUCT_ITEM, data });
export const setProductTotal = (data) => ({ type: SET_PRODUCT_TOTAL, data });