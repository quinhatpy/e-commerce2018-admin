import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import Toast from '../utils/toast';
import { getUserAuthRequest, resetAlert, setAlert } from './../actions';
import { SET_USER_ITEM, SET_USER_LIST } from './../constants/actionType';
import * as linkConfig from './../constants/link';
import api from './../utils/axios';
import history from './../utils/history';
import scrollTop from './../utils/scrollTop';

export const fetchUsersRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`users?${queryParams}`)
            .then(res => {
                dispatch(setUserList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message)
                dispatch(hideLoading());
            })
    }
}

export const createUserRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('users', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);

                history.push(linkConfig.USER_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchUserRequest = id => {
    return dispatch => {
        
        dispatch(showLoading());
        api.get('users/' + id)
            .then(res => {
                dispatch(setUser(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message)
                dispatch(hideLoading());
            })
    }
}

export const updateUserRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('users/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            dispatch(getUserAuthRequest());
            Toast.success(res.data.message);
            history.push(linkConfig.USER_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteUserRequest = (id, offset, limit) => dispatch => {
        dispatch(showLoading());
        api.delete('users/' + id)
            .then(res => {
                dispatch(hideLoading());
                let queryParams = history.location.search;
                let params = queryString.parse(queryParams);
                dispatch(fetchUsersRequest(offset, limit, params));
                Toast.success(res.data.message);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message)
            })
}


export const setUserList = (data) => ({ type: SET_USER_LIST, data });
export const setUser = (data) => ({ type: SET_USER_ITEM, data });