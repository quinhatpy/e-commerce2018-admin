import queryString from 'query-string';
import { SET_CATEGORY_LIST, SET_CATEGORY_ITEM } from '../constants/actionType';
import api from '../utils/axios';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';

export const fetchCategoriesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`categories?${queryParams}`)
            .then(res => {
                dispatch(setCategoryList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createCategoryRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('categories', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.CATEGORY_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchCategoryRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('categories/' + id)
            .then(res => {
                dispatch(setCategory(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateCategoryRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('categories/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.CATEGORY_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteCategoryRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('categories/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchCategoriesRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const resetCategories = () => dispatch =>{
    dispatch(setCategoryList({categories: [], totalRows: 0}))
}

export const setCategoryList = (data) => ({ type: SET_CATEGORY_LIST, data });
export const setCategory = (data) => ({ type: SET_CATEGORY_ITEM, data });