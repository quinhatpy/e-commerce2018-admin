import queryString from 'query-string';
import { SET_CATEGORY_BANNER_LIST, SET_CATEGORY_BANNER_ITEM } from '../constants/actionType';
import api from '../utils/axios';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';

export const fetchCategoryBannersRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`category_banners?${queryParams}`)
            .then(res => {
                dispatch(setCategoryBannerList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createCategoryBannerRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('category_banners', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.CATEGORY_BANNER_LIST + '?sort_by=-id&category_id=' + res.data.result.category.id);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchCategoryBannerRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('category_banners/' + id)
            .then(res => {
                dispatch(setCategoryBanner(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateCategoryBannerRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('category_banners/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.CATEGORY_BANNER_LIST + '?sort_by=-updated_at&category_id=' + res.data.result.category.id);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteCategoryBannerRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('category_banners/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchCategoryBannersRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setCategoryBannerList = (data) => ({ type: SET_CATEGORY_BANNER_LIST, data });
export const setCategoryBanner = (data) => ({ type: SET_CATEGORY_BANNER_ITEM, data });