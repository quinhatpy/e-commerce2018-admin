import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { SET_ARTICLE_ITEM, SET_ARTICLE_LIST, SET_ARTICLE_TOTAL } from '../constants/actionType';
import * as linkConfig from '../constants/link';
import api from '../utils/axios';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import Toast from '../utils/toast';
import { resetAlert, setAlert } from './actions';

export const fetchArticlesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`articles?${queryParams}`)
            .then(res => {
                dispatch(setArticleList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createArticleRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('articles', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.ARTICLE_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchArticleRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('articles/' + id)
            .then(res => {
                dispatch(setArticle(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateArticleRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('articles/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.ARTICLE_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteArticleRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('articles/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchArticlesRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const fetchArticleTotalRequest = () => {
    return dispatch => {
        dispatch(showLoading());
       
        api.get('articles-count')
            .then(res => {
                console.log(res.data);
                dispatch(setArticleTotal(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const setArticleList = (data) => ({ type: SET_ARTICLE_LIST, data });
export const setArticle = (data) => ({ type: SET_ARTICLE_ITEM, data });
export const setArticleTotal = (data) => ({ type: SET_ARTICLE_TOTAL, data });