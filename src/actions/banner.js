import queryString from 'query-string';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { SET_BANNER_LIST, SET_BANNER_ITEM } from '../constants/actionType';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';

export const fetchBannersRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`banners?${queryParams}`)
            .then(res => {
                dispatch(setBannerList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createBannerRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('banners', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.BANNER_LIST + '?sort_by=-id&keyword=' + res.data.result.position);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchBannerRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('banners/' + id)
            .then(res => {
                dispatch(setBanner(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateBannerRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('banners/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.BANNER_LIST + '?sort_by=-updated_at&keyword=' + res.data.result.position);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteBannerRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('banners/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchBannersRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setBannerList = (data) => ({ type: SET_BANNER_LIST, data });
export const setBanner = (data) => ({ type: SET_BANNER_ITEM, data });