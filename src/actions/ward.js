import queryString from 'query-string';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { SET_WARD_LIST, SET_WARD_ITEM } from '../constants/actionType';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';

export const fetchWardsRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`wards?${queryParams}`)
            .then(res => {
                dispatch(setWardList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createWardRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('wards', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.WARD_LIST + '?sort_by=-id&district_id=' + res.data.result.district.id);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchWardRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('wards/' + id)
            .then(res => {
                dispatch(setWard(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateWardRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('wards/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.WARD_LIST + '?sort_by=-updated_at&district_id=' + res.data.result.district.id);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteWardRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('wards/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchWardsRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setWardList = (data) => ({ type: SET_WARD_LIST, data });
export const setWard = (data) => ({ type: SET_WARD_ITEM, data });