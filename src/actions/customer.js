import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { SET_CUSTOMER_ITEM, SET_CUSTOMER_LIST, SET_CUSTOMER_TOTAL } from '../constants/actionType';
import * as linkConfig from '../constants/link';
import api from '../utils/axios';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import Toast from '../utils/toast';
import { resetAlert, setAlert } from './actions';

export const fetchCustomersRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`customers?${queryParams}`)
            .then(res => {
                dispatch(setCustomerList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const searchCustomersRequest = (params = {}) => {
    return dispatch => {
        let queryParams = queryString.stringify(params);
        api.get(`customers?${queryParams}`)
            .then(res => {
                dispatch(setCustomerList(res.data.result));
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createCustomerRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        console.log(data);
        api.post('customers', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.CUSTOMER_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchCustomerRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('customers/' + id)
            .then(res => {
                dispatch(setCustomer(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateCustomerRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('customers/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.CUSTOMER_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteCustomerRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('customers/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchCustomersRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const fetchCustomerTotalRequest = () => {
    return dispatch => {
        dispatch(showLoading());
        api.get('customers-count')
            .then(res => {
                dispatch(setCustomerTotal(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const setCustomerList = (data) => ({ type: SET_CUSTOMER_LIST, data });
export const setCustomer = (data) => ({ type: SET_CUSTOMER_ITEM, data });
export const setCustomerTotal = (data) => ({ type: SET_CUSTOMER_TOTAL, data });