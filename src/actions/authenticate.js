import { hideLoading, showLoading } from 'react-redux-loading-bar';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import Toast from '../utils/toast';
import { LOGOUT, SET_INFO_RESET_PASSWORD, SET_USER_AUTHENTICATED, SET_USER_CURRENT } from './../constants/actionType';
import api from './../utils/axios';
import isAuthenticated from './../utils/isAuthenticated';
import setAuthorizationToken from './../utils/setAuthorizationToken';

export const loginRequest = (userInfo) => {
    return dispatch => {
        dispatch(showLoading());
        api.post('auth/login', userInfo)
            .then(res => {
                let token = res.data.result.token;
                localStorage.setItem('accessToken', token);

                setAuthorizationToken(token);
                isAuthenticated();

                Toast.success('Đăng nhập thành công!');
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                let message = err.response ? err.response.data.message : err
                Toast.error('Error occurred! ' + message);
                dispatch(hideLoading());
            });
    }
}

export const getUserAuthRequest = () => {
    return dispatch => {
        api.get('auth/user')
            .then(res => {
                let dataResponse = res.data;
                dispatch(setUserCurrent(dataResponse.result));
            }).catch(err => {
                // dispatch(logout());
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
            })
    }
}

export const logoutRequest = () => dispatch => {
    api.get('auth/logout')
        .then(res => {
            dispatch(logout());
        }).catch(err => {
            console.log(err);
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const forgotPasswordRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('auth/password/create', data)
            .then(res => {
                Toast.success(res.data.message);
                history.push(linkConfig.LOGIN);
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                let message = err.response ? err.response.data.message : err
                Toast.error('Error occurred! ' + message);
                dispatch(hideLoading());
            });
    }
}

export const findResetPasswordRequest = token => {
    return dispatch => {
        dispatch(showLoading());
        api.get('auth/password/find/' + token)
            .then(res => {
                dispatch(setInfoResetPassword(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                let message = err.response ? err.response.data.message : err
                Toast.error('Error occurred! ' + message);
                dispatch(hideLoading());
            });
    }
}

export const resetPasswordRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('auth/password/reset', data)
            .then(res => {
                Toast.success(res.data.message);
                history.push(linkConfig.LOGIN);
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                let message = err.response ? err.response.data.message : err
                Toast.error('Error occurred! ' + message);
                dispatch(hideLoading());
            });
    }
}

export const setUserAuthenticated = (authInfo) => ({ type: SET_USER_AUTHENTICATED, authInfo });
export const setUserCurrent = (user) => ({ type: SET_USER_CURRENT, user });
export const logout = () => ({ type: LOGOUT });
export const setInfoResetPassword = (user) => ({ type: SET_INFO_RESET_PASSWORD, user });