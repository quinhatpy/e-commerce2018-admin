import queryString from 'query-string';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { SET_BRANCH_LIST, SET_BRANCH_ITEM } from '../constants/actionType';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';

export const fetchBranchesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`branches?${queryParams}`)
            .then(res => {
                dispatch(setBranchList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createBranchRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('branches', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.BRANCH_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchBranchRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('branches/' + id)
            .then(res => {
                dispatch(setBranch(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateBranchRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('branches/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.BRANCH_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteBranchRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('branches/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchBranchesRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const resetBranches = () => dispatch => {
    dispatch(setBranchList({branches: [], totalRows: 0}))
}

export const setBranchList = (data) => ({ type: SET_BRANCH_LIST, data });
export const setBranch = (data) => ({ type: SET_BRANCH_ITEM, data });