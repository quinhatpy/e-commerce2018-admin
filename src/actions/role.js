import { SET_ROLES, SET_ROLE_ITEM } from './../constants/actionType';
import api from './../utils/axios';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import * as linkConfig from './../constants/link';
import history from './../utils/history';
import scrollTop from './../utils/scrollTop';
import { setAlert, resetAlert, getUserAuthRequest } from './../actions';

export const fetchRolesRequest = () => {
    return dispatch => {
        dispatch(showLoading());
        api.get('roles')
            .then(res => {
                dispatch(setRoles(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createRoleRequest = data => dispatch => {
    dispatch(showLoading());
    api.post('roles', data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.ROLE_LIST);
        }).catch(err => {
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }
        })

}

export const fetchRoleRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('roles/' + id)
            .then(res => {
                dispatch(setRole(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateRoleRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('roles/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            dispatch(getUserAuthRequest());
            Toast.success(res.data.message);
            history.push(linkConfig.ROLE_LIST);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteRoleRequest = id => dispatch => {
    dispatch(showLoading());
    api.delete('roles/' + id)
        .then(res => {
            dispatch(hideLoading());
            dispatch(fetchRolesRequest());
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setRoles = data => ({ type: SET_ROLES, data });
export const setRole = data => ({ type: SET_ROLE_ITEM, data });