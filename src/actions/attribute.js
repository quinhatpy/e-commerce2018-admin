import queryString from 'query-string';
import { SET_ATTRIBUTE_LIST, SET_ATTRIBUTE_ITEM } from '../constants/actionType';
import api from '../utils/axios';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';

export const fetchAttributesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`attributes?${queryParams}`)
            .then(res => {
                dispatch(setAttributeList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createAttributeRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('attributes', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.ATTRIBUTE_LIST + '?sort_by=-id&category_id=' + res.data.result.category.id);
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchAttributeRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('attributes/' + id)
            .then(res => {
                dispatch(setAttribute(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateAttributeRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('attributes/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.ATTRIBUTE_LIST + '?sort_by=-updated_at&category_id=' + res.data.result.category.id);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteAttributeRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('attributes/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchAttributesRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const resetAttributes = () => dispatch => {
    dispatch(setAttributeList({attributes: [], totalRows: 0}))
}

export const setAttributeList = (data) => ({ type: SET_ATTRIBUTE_LIST, data });
export const setAttribute = (data) => ({ type: SET_ATTRIBUTE_ITEM, data });