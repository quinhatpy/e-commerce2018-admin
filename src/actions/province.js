import queryString from 'query-string';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { SET_PROVINCE_LIST, SET_PROVINCE_ITEM } from '../constants/actionType';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';

export const fetchProvincesRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`provinces?${queryParams}`)
            .then(res => {
                dispatch(setProvinceList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createProvinceRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('provinces', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.PROVINCE_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchProvinceRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('provinces/' + id)
            .then(res => {
                dispatch(setProvince(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateProvinceRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('provinces/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.PROVINCE_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteProvinceRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('provinces/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchProvincesRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setProvinceList = (data) => ({ type: SET_PROVINCE_LIST, data });
export const setProvince = (data) => ({ type: SET_PROVINCE_ITEM, data });