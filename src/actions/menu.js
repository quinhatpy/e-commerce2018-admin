import queryString from 'query-string';
import { SET_MENU_LIST, SET_MENU_ITEM } from '../constants/actionType';
import api from '../utils/axios';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';

export const fetchMenusRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`menus?${queryParams}`)
            .then(res => {
                dispatch(setMenuList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createMenuRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('menus', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.MENU_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchMenuRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('menus/' + id)
            .then(res => {
                dispatch(setMenu(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateMenuRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('menus/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.MENU_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteMenuRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('menus/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchMenusRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}


export const setMenuList = (data) => ({ type: SET_MENU_LIST, data });
export const setMenu = (data) => ({ type: SET_MENU_ITEM, data });