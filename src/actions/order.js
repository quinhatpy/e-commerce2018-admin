import queryString from 'query-string';
import { hideLoading, showLoading } from 'react-redux-loading-bar';
import { SET_ORDER_ITEM, SET_ORDER_LIST, SET_ORDER_TOTAL } from '../constants/actionType';
import * as linkConfig from '../constants/link';
import api from '../utils/axios';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';
import Toast from '../utils/toast';
import { resetAlert, setAlert } from './actions';

export const fetchOrdersRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`orders?${queryParams}`)
            .then(res => {
                dispatch(setOrderList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createOrderRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('orders', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.ORDER_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchOrderRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('orders/' + id)
            .then(res => {
                dispatch(setOrder(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateOrderRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('orders/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.ORDER_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteOrderRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('orders/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchOrdersRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const fetchOrderTotalRequest = () => {
    return dispatch => {
        dispatch(showLoading());
        api.get('orders-count')
            .then(res => {
                dispatch(setOrderTotal(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const setOrderList = (data) => ({ type: SET_ORDER_LIST, data });
export const setOrder = (data) => ({ type: SET_ORDER_ITEM, data });
export const setOrderTotal = (data) => ({ type: SET_ORDER_TOTAL, data });