import queryString from 'query-string';
import Toast from '../utils/toast';
import { showLoading, hideLoading } from 'react-redux-loading-bar';
import api from '../utils/axios';
import { SET_BRAND_LIST, SET_BRAND_ITEM } from '../constants/actionType';
import { setAlert, resetAlert } from './actions';
import * as linkConfig from '../constants/link';
import history from '../utils/history';
import scrollTop from '../utils/scrollTop';

export const fetchBrandsRequest = (offset, limit, params = {}) => {
    return dispatch => {
        let paramsClone = { ...params };
        paramsClone.offset = offset;
        paramsClone.limit = limit;
        let queryParams = queryString.stringify(paramsClone);
        dispatch(showLoading());

        api.get(`brands?${queryParams}`)
            .then(res => {
                dispatch(setBrandList(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const createBrandRequest = data => {
    return dispatch => {
        dispatch(showLoading());
        api.post('brands', data)
            .then(res => {
                dispatch(hideLoading());
                Toast.success(res.data.message);
                history.push(linkConfig.BRAND_LIST + '?sort_by=-id');
            }).catch(err => {
                console.log(err);
                dispatch(hideLoading());
                scrollTop();
                if (err.response) {
                    const response = err.response.data;
                    dispatch(setAlert('danger', response.message, response.data))
                    setTimeout(() => dispatch(resetAlert()), 5000);
                }

            })
    }
}

export const fetchBrandRequest = id => {
    return dispatch => {
        dispatch(showLoading());
        api.get('brands/' + id)
            .then(res => {
                dispatch(setBrand(res.data.result));
                dispatch(hideLoading());
            }).catch(err => {
                console.log(err);
                if (err.response)
                    Toast.error('Error occurred! ' + err.response.data.message);
                dispatch(hideLoading());
            })
    }
}

export const updateBrandRequest = (id, data) => dispatch => {
    dispatch(showLoading());
    api.post('brands/' + id, data)
        .then(res => {
            dispatch(hideLoading());
            Toast.success(res.data.message);
            history.push(linkConfig.BRAND_LIST + '?sort_by=-updated_at');
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            scrollTop();
            if (err.response) {
                const response = err.response.data;
                dispatch(setAlert('danger', response.message, response.data))
                setTimeout(() => dispatch(resetAlert()), 5000);
            }

        })
}

export const deleteBrandRequest = (id, offset, limit) => dispatch => {
    dispatch(showLoading());
    api.delete('brands/' + id)
        .then(res => {
            dispatch(hideLoading());
            let queryParams = history.location.search;
            let params = queryString.parse(queryParams);
            dispatch(fetchBrandsRequest(offset, limit, params));
            Toast.success(res.data.message);
        }).catch(err => {
            console.log(err);
            dispatch(hideLoading());
            if (err.response)
                Toast.error('Error occurred! ' + err.response.data.message);
        })
}

export const resetBrands = () => dispatch => {
    dispatch(setBrandList({brands: [], totalRows: 0}))
}

export const setBrandList = (data) => ({ type: SET_BRAND_LIST, data });
export const setBrand = (data) => ({ type: SET_BRAND_ITEM, data });