import React, { Component, Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import * as linkConfig from './constants/link';
import routes from './constants/routes';
import AsyncComponent from './hoc/AsyncComponent';
import Layout from './hoc/Layout';
import ProtectedRoute from './hoc/ProtectedRoute';

const AsyncLogin = AsyncComponent(() => {
    return import('./pages/Login');
});

const AsyncForgotPassword = AsyncComponent(() => {
    return import('./pages/ForgotPassword');
});

const AsyncResetPassword = AsyncComponent(() => {
    return import('./pages/ResetPassword');
});

class App extends Component {
    
    showRoutes(routes) {
        var result = null;
        if (routes.length > 0) {
            result = routes.map((route, index) => {
                return route.protected ?
                    <ProtectedRoute
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component}
                    /> : <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component}
                    />
            });
        }
        return <Switch>{result}</Switch>;
    }
    render() {
        return (
            <Fragment>
                <Switch>
                    <Route path={linkConfig.LOGIN} exact component={AsyncLogin} />
                    <Route path={linkConfig.FORGOT_PASSWORD} exact component={AsyncForgotPassword} />
                    <Route path={linkConfig.RESET_PASSWORD + ':token'} exact component={AsyncResetPassword} />
                    <Layout>
                        {this.showRoutes(routes)}
                    </Layout>
                </Switch>
            </Fragment>

        );
    }
}

export default App;
