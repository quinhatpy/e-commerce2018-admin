import React, { Component } from 'react'

export default class TableHeadColumn extends Component {
    render() {
        const { name, label, sortable, sortBy, onClickColumn } = this.props;
        let classes = '';
        if (sortable) classes = 'sorting';

        let sortValue = '', sortName = name;
        if (sortBy.indexOf('-') !== -1) sortValue = '';
        else sortValue = '-'

        if (sortBy.substring(1) === name || sortBy === name)
            classes = sortValue === '' ? 'sorting_asc' : 'sorting_desc';

        return (
            sortable ? <th className={classes} onClick={() => onClickColumn(sortName, sortValue)}>{label}</th> : <th className={classes}>{label}</th>
        )
    }
}
