import React, { Fragment } from 'react';

const LoginForm = (props) => {
    let inputElement = null;
    let hasErrorClass = '';

    if (props.invalid && props.shouldValidate && props.touched) {
        hasErrorClass = 'has-error';
    }
// console.log(props);
    switch (props.elementType) {
        case 'email':
            inputElement = (
                <Fragment>
                    <input type="email" className="form-control"
                        value={props.value}
                        {...props.elementConfig}
                        onChange={props.changed}
                        onBlur={props.changed} />
                    <span className="glyphicon glyphicon-envelope form-control-feedback" />
                    <span className="help-block">{props.errorMessage}</span>
                </Fragment>
            );
            break;
        case 'password':
            inputElement = (
                <Fragment>
                    <input type="password" className="form-control"
                        value={props.value}
                        {...props.elementConfig}
                        onChange={props.changed}
                        onBlur={props.changed} />
                    <span className="glyphicon glyphicon-lock form-control-feedback" />
                    <span className="help-block">{props.errorMessage}</span>
                </Fragment>
            );
            break;
        case 'checkbox':
            inputElement = (
                <Fragment>
                    <div className="col-xs-8">
                        <div className="checkbox icheck">
                            <label>
                                <input type="checkbox" id="remember-me" onChange={props.changed} /> Remember Me
                            </label>
                        </div>
                    </div>
                </Fragment>
            );
            break;
        default: inputElement = null;
    }

    return (
        <div className={`form-group has-feedback ${hasErrorClass}`}>
            {inputElement}
        </div>
    )
}

export default LoginForm;