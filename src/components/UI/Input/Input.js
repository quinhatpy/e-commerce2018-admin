import React, { Fragment } from 'react'
import CheckboxTree from 'react-checkbox-tree';
import BraftEditor from 'braft-editor'
import 'react-checkbox-tree/lib/react-checkbox-tree.css';
import 'react-datepicker/dist/react-datepicker.css';
import api from '../../../utils/axios';
import { DatePicker, Popconfirm, Icon, Select } from "antd";
import { formatMoney } from './../../../utils/helper';

const handleUploadImage = (param) => {
    api.defaults.onUploadProgress = (progressEvent) => {
        param.progress(progressEvent.loaded / progressEvent.total * 100)
    }
    const data = new FormData();
    data.append('file', param.file)
    data.append('reference', 'article')

    api.post('galleries', data)
        .then(res => {
            param.success(
                {
                    url: res.data.result.path,
                    meta: {
                        id: res.data.result.id,
                        title: res.data.result.title,
                        alt: res.data.result.alt,
                        // src: res.data.result.path,
                    }
                })
        }).catch(err => {
            console.log(err);
            param.error({
                msg: 'unable to upload.'
            })
        })
}

const Input = (props) => {
    let inputElement = null;
    let hasErrorClass = '';

    if (props.invalid && props.shouldValidate && props.touched) {
        hasErrorClass = 'has-error';
    }

    switch (props.elementType) {
        case 'input':
            inputElement = (
                <div className="col-sm-9">
                    <input className="form-control"
                        value={props.value}
                        onChange={props.changed}
                        onBlur={props.changed}
                        {...props.elementConfig} />
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                </div>
            );
            break;
        case 'textarea':
            inputElement = (
                <div className="col-sm-9">
                    <textarea className="form-control"
                        onChange={props.changed}
                        {...props.elementConfig} value={props.value} />
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                </div>
            );
            break;
        case 'checkbox':
            inputElement = (
                <div className="col-sm-9">
                    <div className="checkbox">
                        <label>
                            <input type="checkbox"
                                {...props.elementConfig}
                                checked={props.value}
                                // checked={props.elementConfig.checked}
                                onChange={props.changed} />
                            {props.elementConfig.label}
                        </label>
                        {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                    </div>
                </div>
            );
            break;
        case 'select':
            inputElement = (
                <div className="col-sm-9">
                    <select className="form-control"
                        value={props.value}
                        onChange={props.changed}
                        {...props.elementConfig}>
                        {
                            props.elementConfig.options.map(option => (
                                <option key={option.value} value={option.value}>
                                    {option.displayValue}
                                </option>
                            ))
                        }
                    </select>
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                </div>
            );
            break;
        case 'radio':
            inputElement = (
                <div className="col-sm-9">
                    {
                        props.elementConfig.children.map((radio, index) => {
                            return (
                                <div className="radio" key={index}>
                                    <label>
                                        <input type="radio"
                                            onChange={props.changed}
                                            {...radio.elementConfig}
                                            checked={parseInt(props.value) === radio.elementConfig.value} />
                                        {radio.label}
                                    </label>
                                </div>
                            )
                        })
                    }
                </div>
            );
            break;
        case 'date':
            const dateFormat = "DD-MM-YYYY";
            inputElement = (
                <div className="col-sm-9">
                    <DatePicker
                        className="form-control"
                        placeholder="Select date"
                        {...props.elementConfig}
                        value={props.value}
                        format={dateFormat}
                        onChange={props.changed}
                    />
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                </div>
            );
            break;
        case 'inputFileWithPreview':
            inputElement = (
                <div className="col-sm-9">
                    <input className="form-control"
                        onChange={props.changed}
                        value={props.value}
                        {...props.elementConfig} />
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                    {
                        props.elementConfig.multiple ?
                            props.elementConfig.data ?
                                props.elementConfig.data.map((image, index) => (
                                    <img key={index} src={image.data} alt="preview" className="img-responsive" style={{ margin: '10px 0', maxWidth: '200px' }} />
                                ))
                                : null
                            :
                            props.elementConfig.data ? <img src={props.elementConfig.data} alt="preview" className="img-responsive" style={{ margin: '10px 0', maxWidth: '200px' }} /> : null
                    }
                </div>
            );
            break;
        case 'checkboxInline':
            inputElement = (
                <div className="col-sm-9">
                    {
                        props.elementConfig.children.map((checkbox, index) => {
                            return (
                                <label className="checkbox-inline" key={index}>
                                    <label>
                                        <input type="checkbox"
                                            {...checkbox.elementConfig}
                                            checked={checkbox.checked}
                                            onChange={props.changed} />
                                        {checkbox.label}
                                    </label>
                                </label>
                            )
                        })
                    }
                </div>
            );
            break;
        case 'radioTree':
            inputElement = (
                <div className="col-sm-9">

                    <CheckboxTree
                        nodes={props.elementConfig.data}
                        name={props.elementConfig.name}
                        noCascade={props.elementConfig.noCascade}
                        checked={props.elementConfig.checked}
                        showNodeIcon={props.elementConfig.showNodeIcon}
                        onClick={(checked) => props.changed(checked)}
                        expanded={props.elementConfig.expanded}
                        onExpand={expanded => props.expanded(expanded)}
                    />
                </div>
            );
            break;
        case 'richText':
            inputElement = (
                <div className="col-sm-9">
                    <BraftEditor
                        language="en"
                        value={props.value}
                        media={{
                            uploadFn: handleUploadImage,
                        }}
                        onChange={props.changed}
                        style={{ border: '1px solid #ccc' }}
                        {...props.elementConfig}
                    />
                </div>
            );
            break;
        case 'multiInput':
            const data = props.elementConfig.children;
            inputElement = (
                <div className="col-sm-9">
                    {
                        data.length === 0 ? props.elementConfig.placeholder :
                            data.map((item, index) => (
                                <div className="row" key={index} style={{ marginBottom: '10px' }}>
                                    <div className="col-md-3">
                                        {item.label}
                                    </div>
                                    <div className="col-md-9">
                                        <input className="form-control"
                                            type={item.type}
                                            name={item.name}
                                            defaultValue={item.value}
                                            required={item.required}
                                        />
                                    </div>
                                </div>
                            ))
                    }
                </div>
            );
            break;
        case 'dateTimeRange':
            const dateTimeFormat = "DD-MM-YYYY HH:mm:ss";
            const { RangePicker } = DatePicker;
            inputElement = (
                <div className="col-sm-9">
                    <RangePicker
                        className="form-control"
                        placeholder={[
                            "Start time",
                            "End time"
                        ]}
                        {...props.elementConfig}
                        value={props.value}
                        format={dateTimeFormat}
                        onChange={props.changed}
                        showTime
                    />
                </div>
            );
            break;
        case 'imageList':
            function confirmDelete(id) {
                props.changed(id);
            }
            inputElement = (
                <div className="col-sm-9">
                    {
                        props.elementConfig.data.map((image, index) => (
                            <Fragment key={index} >
                                <img src={image.data} alt="preview" className="img-responsive" style={{ margin: '10px 1%', width: '80%', display: 'inline' }} />
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this image?"
                                    onConfirm={() => confirmDelete(image.id)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            </Fragment>
                        ))
                    }
                </div>
            );
            break;
        case 'selectAsync':
            const Option = Select.Option;
            inputElement = (
                <div className="col-sm-9">
                    <Select
                        showSearch
                        labelInValue
                        value={props.elementConfig.selected}
                        placeholder="Search and select"
                        notFoundContent={props.elementConfig.data.length === 0 ? 'Not found...' : null}
                        filterOption={false}
                        onSearch={props.elementConfig.onSearch}
                        onChange={props.changed}
                        style={{ width: '100%', maxWidth: '500px' }}
                    >
                        {props.elementConfig.data.map(d => <Option key={d.value}>{d.displayValue}</Option>)}
                    </Select>
                </div>
            );
            break;

        case 'cartTable':
            let totalPrice = 0;
            let i = 0;
            inputElement = (
                <div className="col-sm-12">
                    <h3 className="text-center">Cart</h3>
                    <table className="table table-bordered table-hover">
                        <thead>

                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Thumbnail</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                props.elementConfig.data.length === 0 ? <tr><td colSpan="6" align="center">No product in cart</td></tr> :
                                    props.elementConfig.data.map((item, index) => {
                                        totalPrice += item.quantity * item.price
                                        return (
                                            <tr key={index}>
                                                <td>{++i}</td>
                                                <td>{item.name}</td>
                                                <td><img src={item.thumbnail} alt="" width="50" /></td>
                                                <td>{formatMoney(item.price)}<sup>đ</sup></td>
                                                <td>
                                                    <input type="number" value={item.quantity}
                                                        onChange={(event) => props.elementConfig.onChangeQuantity(item.id, event)} />
                                                </td>
                                                <td>{formatMoney(item.quantity * item.price)}<sup>đ</sup></td>
                                                <td>
                                                    <Popconfirm
                                                        placement="topRight"
                                                        title="Are you sure to delete this image?"
                                                        onConfirm={() => props.elementConfig.onRemove(item.id)}
                                                        okText="Yes"
                                                        cancelText="No"
                                                        okType="danger"
                                                        icon={<Icon type="question-circle" />} >
                                                        <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                                    </Popconfirm>
                                                </td>
                                            </tr>
                                        )
                                    })
                            }
                        </tbody>
                        <tfoot>
                            <tr><td colSpan="6" align="right"><b>Total price:</b> {formatMoney(totalPrice)}<sup>đ</sup></td></tr>
                        </tfoot>
                    </table>
                </div>
            );
            break;
        default:
            inputElement = (
                <div className="col-sm-9">
                    <input className="form-control"
                        value={props.value}
                        onChange={props.changed}
                        {...props.elementConfig} />
                    {props.errorMessage ? <span className="help-block">{props.errorMessage}</span> : null}
                </div>
            );
            break;
    }

    return (
        <div className={`form-group ${hasErrorClass}`}>
            <label className="col-sm-3 control-label">
                {props.label}
                {props.shouldValidate.required ? <span style={{ color: 'red' }}> *</span> : null}
            </label>

            {inputElement}
        </div>
    )
}

export default Input
