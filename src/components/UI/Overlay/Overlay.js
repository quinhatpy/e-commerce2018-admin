import React, { Component } from 'react';
import { connect } from 'react-redux';

class Overlay extends Component {
    render() {
        const { loading } = this.props;
        if (!isNaN(loading) && loading !== 0) {
            return (
                <div className="overlay-wrapper">
                    <div className="overlay">
                        <i className="fa fal fa-sync-alt fa-spin" />
                    </div>
                </div>
            )
        }
        return null;
    }
}

const mapStateToProps = state => {
    return {
        loading: state.loadingBar.default
    }
}

export default connect(mapStateToProps)(Overlay);