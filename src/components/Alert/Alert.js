import React, { Component } from 'react'
import PropTypes from 'prop-types'
import isEmpty from 'lodash/isEmpty'

class Alert extends Component {
    render() {
        const { type, title, content } = this.props;

        let arrayMessage = [];
        for (let key in content) {
            for (let itemMessage of content[key]) {
                arrayMessage.push(itemMessage);
            }
        }
        
        if (isEmpty(arrayMessage)) return null;

        return (
            <div className={`alert alert-${type}`}>
                <h4><i className="icon fa fa-ban" /> {title}</h4>
                <ul>
                    {
                        arrayMessage.map((item, index) => {
                            return <li key={index}>{item}</li>
                        })
                    }
                </ul>
            </div >
        )
    }
}

Alert.propTypes = {
    type: PropTypes.string,
    title: PropTypes.string.isRequired,
    content: PropTypes.object.isRequired
}

export default Alert;