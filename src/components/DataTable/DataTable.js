import React, { Component } from 'react';
import renderTableHeadColumn from '../../utils/dataTable';

class DataTable extends Component {
    state = {
        sortBy: ''
    }

    componentDidMount() {
        const stateClone = { ...this.state };
        stateClone.sortBy = this.props.sortBy;
        this.setState(stateClone);
    }

    handleChangeSortColumn = (sortName, sortValue) => {
        const stateClone = { ...this.state };
        const sortBy = sortValue + sortName;

        stateClone.sortBy = sortBy;
        this.setState(stateClone);
        this.props.onSort(sortBy);
    }



    render() {
        const { sortBy } = this.state;
        const { renderList, tableHeadColumns } = this.props;

        return (
            <table className="table table-bordered table-hover dataTable ">
                <thead>
                    {renderTableHeadColumn(tableHeadColumns, sortBy, this.handleChangeSortColumn)}
                </thead>
                <tbody>
                    {renderList()}
                </tbody>
            </table>
        )
    }
}

export default DataTable;
