import React, { Component } from 'react'

export default class Login extends Component {
    render() {
        return (
            <a href="/" className="logo">
                {/* mini logo for sidebar mini 50x50 pixels */}
                <span className="logo-mini"><b>E</b>2018</span>
                {/* logo for regular state and mobile devices */}
                <span className="logo-lg"><b>E-Commerce</b>2018</span>
            </a>
        )
    }
}
