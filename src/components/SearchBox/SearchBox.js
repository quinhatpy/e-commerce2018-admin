import React, { Component } from 'react';
import queryString from 'query-string';
import { withRouter } from 'react-router-dom';

class SearchBox extends Component {
    state = {
        keyword: ''
    }

    componentWillReceiveProps(nextProps) {
        const queryParam = queryString.parse(nextProps.location.search);
        this.setState({
            keyword: queryParam.keyword ? queryParam.keyword : ''
        })
    }

    handleOnChangeKeyword = (event) => {
        let stateClone = { ...this.state };
        stateClone.keyword = event.target.value;
        this.setState(stateClone);
    }

    render() {
        const { keyword } = this.state;
        const {
            handleOnSearch,
            placeholder
        } = this.props;

        return (
            <div className="input-group input-group-sm" style={{ width: 250 }}>
                <input type="search" name="keyword" className="form-control pull-right" placeholder={placeholder}
                    value={keyword}
                    onChange={(event) => this.handleOnChangeKeyword(event)} />
                <div className="input-group-btn">
                    <button type="button" className="btn btn-default"
                        onClick={() => handleOnSearch(keyword)}><i className="fas fa-search" /></button>
                </div>
            </div>
        )
    }
}

export default withRouter(SearchBox);