import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Avatar } from 'antd';

export default class DropdownUserMenu extends Component {
    render() {
        const { user, logged } = this.props;
        const avatar = user.avatar ? <img src={user.avatar} className="user-image" alt="avatar" /> :  <Avatar size={22} icon="user" />
        return (
            <li className="dropdown user user-menu">
                <a href="/" className="dropdown-toggle" data-toggle="dropdown">
                    {avatar}
                    <span className="hidden-xs">{user.name}</span>
                </a>
                <ul className="dropdown-menu">
                    <li className="user-footer">
                        <div className="pull-left">
                            <a href="/" className="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div className="pull-right">
                            <button onClick={logged} className="btn btn-default btn-flat">Sign out</button>
                        </div>
                    </li>
                </ul>
            </li>

        )
    }
}

DropdownUserMenu.propTypes = {
    user: PropTypes.object.isRequired,
    logged: PropTypes.func.isRequired
}
