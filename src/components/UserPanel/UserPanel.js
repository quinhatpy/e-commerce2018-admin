import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Avatar } from 'antd';

export default class UserPanel extends Component {
    render() {
        const { user } = this.props;
        const avatar = user.avatar ? <img src={user.avatar} className="img-circle" alt="avatar" /> :  <Avatar size={40} icon="user" />
        return (
            <div className="user-panel">
                <div className="pull-left image">
                    {avatar}
                </div>
                <div className="pull-left info">
                    <p>{user.name}</p>
                    <a href="/"><i className="fa fa-circle text-success" /> Online</a>
                </div>
            </div>
        )
    }
}


UserPanel.propTypes = {
    user: PropTypes.object.isRequired,
}