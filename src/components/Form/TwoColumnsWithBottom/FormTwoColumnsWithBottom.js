import React, { Component } from 'react'
import Button from '../../UI/Button/Button';
import Alert from '../../Alert/Alert';
import Overlay from '../../UI/Overlay/Overlay';
import Input from '../../UI/Input/Input';

export default class FormTwoColumnsWithBottom extends Component {
    renderFormElements(formElementArray, inputChangedHandler) {
        return formElementArray.map(formElement => {
            return <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                label={formElement.config.label}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                errorMessage={formElement.config.errorMessage}
                changed={(event) => inputChangedHandler(event, formElement.id)}
            />
        }
        )
    }
    render() {
        const { handleSubmit,
            handleReset,
            handleInputChange,
            formElementArrayLeft,
            formElementArrayRight,
            formElementArrayTop,
            formIsValid,
            alert,
            colTop,
            colOffset,
            col1,
            col2
        } = this.props;
        let colClass = 'col-xs-';

        return (
            <form className="form-horizontal" encType="multipart/form-data"
                onSubmit={handleSubmit}
                onReset={handleReset}>
                <div className="box box-primary">
                    <div className="box-body">
                        <Overlay />
                        <div className="row">
                            <div className="col-xs-12">
                                <Alert type={alert.type} title={alert.title} content={alert.content} />
                            </div>
                            <div className={colClass + col1}>
                                {this.renderFormElements(formElementArrayLeft, handleInputChange)}
                            </div>
                            <div className={colClass + col2}>
                                {this.renderFormElements(formElementArrayRight, handleInputChange)}
                            </div>
                            <div className={colClass + colTop + ' ' + colOffset}>
                                {this.renderFormElements(formElementArrayTop, handleInputChange)}
                            </div>
                            <div className="col-xs-12">
                                <div className="box-footer text-center">
                                    <Button
                                        type="reset"
                                        classes="btn btn-default">
                                        <i className="fa fa-refresh" /> Reset
                        </Button>
                                    &nbsp; &nbsp;
                        <Button
                                        type="submit"
                                        classes="btn btn-primary"
                                        disabled={!formIsValid}
                                    >
                                        <i className="fa fa-send-o" /> Submit
                        </Button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form >
        )
    }
}
