import React, { Component } from 'react'
import Button from '../../UI/Button/Button';
import Alert from '../../Alert/Alert';
import Overlay from '../../UI/Overlay/Overlay';
import Input from '../../UI/Input/Input';
import _ from 'lodash/array';

export default class FormTwoColumnsHasCheckBoxTree extends Component {
    renderFormElements(formElementArray, handleInputChange, handleCheckedCheckboxTree, handleExpandedCheckboxTree) {
        return formElementArray.map(formElement => {
            if (_.indexOf(['radioTree'], formElement.config.elementType) !== -1) {
                return <Input
                    key={formElement.id}
                    elementType={formElement.config.elementType}
                    elementConfig={formElement.config.elementConfig}
                    label={formElement.config.label}
                    value={formElement.config.value}
                    invalid={!formElement.config.valid}
                    shouldValidate={formElement.config.validation}
                    touched={formElement.config.touched}
                    errorMessage={formElement.config.errorMessage}
                    changed={(checked) => handleCheckedCheckboxTree(formElement.id, checked)}
                    expanded={(expanded) => handleExpandedCheckboxTree(formElement.id, expanded)}
                />
            }
            
            return <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                label={formElement.config.label}
                value={formElement.config.value}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                errorMessage={formElement.config.errorMessage}
                changed={(event) => handleInputChange(event, formElement.id)}
            />
        }
        )
    }
    render() {
        const { handleSubmit,
            handleReset,
            handleInputChange,
            handleCheckedCheckboxTree,
            handleExpandedCheckboxTree,
            formElementArrayLeft,
            formElementArrayRight,
            formIsValid,
            alert,
            col1,
            col2 } = this.props;
        let colClass = 'col-xs-';

        return (
            <form className="form-horizontal" encType="multipart/form-data"
                onSubmit={handleSubmit}
                onReset={handleReset}>
                <div className="box box-primary">
                    <div className="box-body">
                        <Overlay />
                        <div className="row">
                            <div className="col-xs-12">
                                <Alert type={alert.type} title={alert.title} content={alert.content} />
                            </div>
                            <div className={colClass + col1}>
                                {this.renderFormElements(formElementArrayLeft, handleInputChange, handleCheckedCheckboxTree, handleExpandedCheckboxTree)}
                            </div>
                            <div className={colClass + col2}>
                                {this.renderFormElements(formElementArrayRight, handleInputChange, handleCheckedCheckboxTree, handleExpandedCheckboxTree)}
                            </div>
                            <div className="col-xs-12">
                                <div className="box-footer text-center">
                                    <Button
                                        type="reset"
                                        classes="btn btn-default">
                                        <i className="fa fa-refresh" /> Reset
                        </Button>
                                    &nbsp; &nbsp;
                        <Button
                                        type="submit"
                                        classes="btn btn-primary"
                                        disabled={!formIsValid}
                                    >
                                        <i className="fa fa-send-o" /> Submit
                        </Button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        )
    }
}
