import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class Breadcrumb extends Component {
    render() {
        const { breadcrumbItems } = this.props;
        const breadcrumbLength = breadcrumbItems.length;
        const breadcrumbElms = breadcrumbItems.map((breadcrumb, index) => {
            if (index === breadcrumbLength - 1)
                return <li key={index} className="active">{breadcrumb.name} </li>
            return <li key={index}>
                <Link to={breadcrumb.link}>{breadcrumb.name}</Link>
            </li>
        });
        return (
            <ol className="breadcrumb">
                <li>
                    <Link to="/dashboard"><i className="far fa-tachometer-alt" /> Dashboard</Link>
                </li>
                {breadcrumbElms}
            </ol>
        )
    }
}
