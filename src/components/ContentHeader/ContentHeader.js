import React, { Component } from 'react'
import Breadcrumb from './Breadcrumb';

export default class ContentHeader extends Component {
    render() {
        const { pageTitle, breadcrumbItems } = this.props;
        return (
            <section className="content-header">
                <h1>{pageTitle}</h1>
                <Breadcrumb breadcrumbItems={breadcrumbItems} />
            </section>
        )
    }
}
