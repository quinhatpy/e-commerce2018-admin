import React, { Component, Fragment } from 'react';
// import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import LinkCustomMenu from './../../hoc/LinkCustomMenu';
import ItemHeader from './ItemHeader';
import TreeView from './TreeView';

export default class SidebarMenu extends Component {
    state = {
        pageCurrent: '/'
    }

    handlePageChange(pageURL) {
        this.setState({ pageCurrent: pageURL })
    }

    renderMenu(menus) {
        const { user } = this.props;
        let permissions = {};
        if (user.role)
            permissions = user.role.permissions;

        return menus.map((group, index) => {
            return (
                <Fragment key={index}>
                    <ItemHeader name={group.groupName} />
                    {
                        group.menuItems.map((item, indexItem) => {
                            if ("children" in item) {
                                return <TreeView
                                    key={indexItem}
                                    itemName={item.itemName}
                                    icon={item.icon}
                                    children={item.children}
                                    to={item.link}
                                    permissions={permissions}
                                    onChangePage={(pageURL) => this.handlePageChange(pageURL)} />
                            } else {

                                return permissions[item.permission] || item.permission === 'auth' ? <LinkCustomMenu
                                    key={indexItem}
                                    to={item.link}
                                    label={<span>{item.itemName}</span>}
                                    icon={item.icon} activeOnlyWhenExact={true}
                                    onChangePage={(pageURL) => this.handlePageChange(pageURL)}
                                /> : null;
                            }

                        })
                    }
                </Fragment>
            )
        });
    }
    render() {
        const { menus } = this.props;


        return (
            <ul className="sidebar-menu" data-widget="tree">
                {this.renderMenu(menus)}
                {/* <ItemHeader /> */}

                {/* <ItemHeader /> */}
                {/* <TreeView /> */}
                {/* <li>
                    <Link to='/dashboard'><i className="fa fa-circle-o text-aqua" /> <span>Information</span></Link>
                </li>
                <li>
                    <Link to='/users/1'><i className="fa fa-circle-o text-aqua" /> <span>Information</span></Link>
                </li> */}
            </ul>

        )
    }
}

SidebarMenu.propTypes = {
    menus: PropTypes.array.isRequired,
    user: PropTypes.object.isRequired
}