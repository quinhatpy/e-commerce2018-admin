import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ItemHeader extends Component {
    render() {
        const { name } = this.props;
        return (
            <li className="header">{name}</li>
        )
    }
}

ItemHeader.propTypes = {
    name: PropTypes.string.isRequired,
}