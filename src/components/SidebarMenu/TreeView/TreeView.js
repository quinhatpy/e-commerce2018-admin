import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import LinkCustomMenu from './../../../hoc/LinkCustomMenu';
import TreeViewMenu from './TreeViewMenu';
import PropTypes from 'prop-types';

export default class TreeView extends Component {
    render() {
        const { itemName, icon, children, permissions } = this.props;
        return (
            <li className="treeview">
                <Link to="/">
                    {icon} <span>{itemName}</span>
                    <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right" />
                    </span>
                </Link>
                <TreeViewMenu
                    permissions={permissions}
                    children={children}
                    onChangePage={(pageURL) => this.props.onChangePage(pageURL)}
                />
            </li>
        )
    }
}

TreeView.propTypes = {
    itemName: PropTypes.string.isRequired,
    icon: PropTypes.object.isRequired,
    children: PropTypes.array.isRequired
}
