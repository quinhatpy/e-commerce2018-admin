import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinkCustomMenu from './../../../../hoc/LinkCustomMenu';

export default class TreeViewMenu extends Component {
    renderItem(children) {
        const { permissions } = this.props;
        return children.map((item, index) => {
            return permissions[item.permission] || item.permission === 'auth' ? <LinkCustomMenu
                key={index}
                label={item.itemName}
                icon={item.icon}
                to={item.link}
                onChangePage={(pageURL) => this.props.onChangePage(pageURL)} /> : null;

        });
    }

    render() {
        const { children } = this.props;

        return (
            <ul className="treeview-menu">
                {this.renderItem(children)}
            </ul>
        )
    }
}

TreeViewMenu.propTypes = {
    children: PropTypes.array.isRequired
}
