import { SET_MENU_LIST, SET_MENU_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_MENU_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.menus;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_MENU_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
