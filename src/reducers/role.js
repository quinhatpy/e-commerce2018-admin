import { SET_ROLES, SET_ROLE_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    roleItem: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_ROLES: {
            const stateClone = { ...state };
            stateClone.list = action.data;
            return stateClone;
        } 
        case SET_ROLE_ITEM: {
            const stateClone = {...state};
            stateClone.roleItem = action.data;
            return stateClone;
        }

        default:
            return state
    }
}
