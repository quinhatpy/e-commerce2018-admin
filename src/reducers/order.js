import { SET_ORDER_LIST, SET_ORDER_ITEM, SET_ORDER_TOTAL } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {},
    count: 0
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_ORDER_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.orders;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_ORDER_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        case SET_ORDER_TOTAL: {
            const stateClone = { ...state };
            stateClone.count = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
