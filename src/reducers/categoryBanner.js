import { SET_CATEGORY_BANNER_LIST, SET_CATEGORY_BANNER_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_CATEGORY_BANNER_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.category_banners;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_CATEGORY_BANNER_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
