import { SET_BRAND_LIST, SET_BRAND_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_BRAND_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.brands;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_BRAND_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
