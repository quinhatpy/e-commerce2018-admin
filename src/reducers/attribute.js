import { SET_ATTRIBUTE_LIST, SET_ATTRIBUTE_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_ATTRIBUTE_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.attributes;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_ATTRIBUTE_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        default:
            return state
    }
}
