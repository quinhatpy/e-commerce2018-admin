import { loadingBarReducer } from 'react-redux-loading-bar';
import { combineReducers } from 'redux';
import alert from './alert';
import article from './article';
import attribute from './attribute';
import auth from './auth';
import banner from './banner';
import branch from './branch';
import brand from './brand';
import category from './category';
import categoryBanner from './categoryBanner';
import customer from './customer';
import district from './district';
import gallery from './gallery';
import loading from './loading';
import menu from './menu';
import order from './order';
import product from './product';
import province from './province';
import role from './role';
import user from './user';
import ward from './ward';

const rootReducer = combineReducers({
    loadingBar: loadingBarReducer,
    loading,
    alert,
    auth,
    user,
    customer,
    role,
    category,
    categoryBanner,
    attribute,
    banner,
    brand,
    province,
    district,
    ward,
    branch,
    menu,
    gallery,
    article,
    product,
    order,
});

export default rootReducer;