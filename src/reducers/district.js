import { SET_DISTRICT_LIST, SET_DISTRICT_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_DISTRICT_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.districts;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_DISTRICT_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
