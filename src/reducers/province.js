import { SET_PROVINCE_LIST, SET_PROVINCE_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_PROVINCE_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.provinces;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_PROVINCE_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
