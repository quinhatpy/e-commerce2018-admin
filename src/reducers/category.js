import { SET_CATEGORY_LIST, SET_CATEGORY_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_CATEGORY_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.categories;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_CATEGORY_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
