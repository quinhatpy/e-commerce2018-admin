import { SET_ALERT } from '../constants/actionType';

const initialState = {
    type: '',
    title: '',
    content: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_ALERT: {
            return {
                type: action.typeAlert,
                title: action.title,
                content: action.content
            }
        }

        default:
            return state
    }
}
