import { SET_BANNER_LIST, SET_BANNER_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_BANNER_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.banners;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_BANNER_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
