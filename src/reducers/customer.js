import { SET_CUSTOMER_ITEM, SET_CUSTOMER_LIST, SET_CUSTOMER_TOTAL } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {},
    count: 0
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_CUSTOMER_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.customers;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_CUSTOMER_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        case SET_CUSTOMER_TOTAL: {
            const stateClone = { ...state };
            stateClone.count = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
