import { SET_BRANCH_LIST, SET_BRANCH_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_BRANCH_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.branches;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_BRANCH_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        default:
            return state
    }
}
