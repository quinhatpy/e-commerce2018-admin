import { SET_USER_LIST, SET_USER_ITEM } from './../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_USER_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.users;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_USER_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
