import { SET_ARTICLE_ITEM, SET_ARTICLE_LIST, SET_ARTICLE_TOTAL } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {},
    count: 0
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_ARTICLE_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.articles;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_ARTICLE_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        case SET_ARTICLE_TOTAL: {
            const stateClone = { ...state };
            stateClone.count = action.data;
            return stateClone;
        }

        default:
            return state
    }
}
