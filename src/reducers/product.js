import { SET_PRODUCT_ITEM, SET_PRODUCT_LIST, SET_PRODUCT_TOTAL } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {},
    count: 0
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_PRODUCT_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.products;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_PRODUCT_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        case SET_PRODUCT_TOTAL: {
            const stateClone = { ...state };
            stateClone.count = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
