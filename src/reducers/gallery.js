import { SET_GALLERY_LIST, SET_GALLERY_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_GALLERY_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.galleries;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_GALLERY_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }


        default:
            return state
    }
}
