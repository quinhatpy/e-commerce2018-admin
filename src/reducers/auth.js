import isEmpty from 'lodash/isEmpty';
import { LOGOUT, SET_INFO_RESET_PASSWORD, SET_USER_AUTHENTICATED, SET_USER_CURRENT } from '../constants/actionType';

const initialState = {
    isAuthenticated: false,
    authInfo: {},
    user: {},
    infoReset: {}
};
export default (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_AUTHENTICATED: {
            let updateState = { ...state };
            updateState.isAuthenticated = !isEmpty(action.authInfo);
            updateState.authInfo = action.authInfo;

            return updateState;
        }
        case SET_USER_CURRENT: {
            let updateState = { ...state };
            updateState.user = action.user;

            return updateState;
        }
        case LOGOUT: {
            localStorage.removeItem('accessToken');
            return {
                isAuthenticated: false,
                authInfo: {},
                user: {}
            }
        }
        case SET_INFO_RESET_PASSWORD: {
            let updateState = { ...state };
            updateState.infoReset = action.user;

            return updateState;
        }
        default:
            return state
    }
}
