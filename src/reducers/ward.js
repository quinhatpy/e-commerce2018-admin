import { SET_WARD_LIST, SET_WARD_ITEM } from '../constants/actionType';
const initialState = {
    list: [],
    totalRows: 0,
    item: {}
}

export default (state = initialState, action) => {
    switch (action.type) {

        case SET_WARD_LIST: {
            const stateClone = { ...state };
            stateClone.list = action.data.wards;
            stateClone.totalRows = action.data.totalRows;
            return stateClone;
        }

        case SET_WARD_ITEM: {
            const stateClone = { ...state };
            stateClone.item = action.data;
            return stateClone;
        }

        default:
            return state
    }
}
