import React, { Component, Fragment } from 'react'
import MainHeader from '../../containers/MainHeader';
import MainSidebar from '../../containers/MainSidebar';
import Footer from './../../containers/Footer';

export default class Layout extends Component {
    componentWillMount() {
        if (window.location.hash === "#login") {
            window.location.hash = '';
            window.location.reload();
        }
    }
    
    render() {
        return (
            <Fragment>

                <div className="wrapper">
                    <MainHeader />
                    <MainSidebar />
                    <div className="content-wrapper">
                        {this.props.children}
                    </div>
                    <Footer />
                </div>
            </Fragment>
        )
    }
}
