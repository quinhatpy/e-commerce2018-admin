import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import { PropagateLoader } from 'react-spinners';

const LoadingHOC = loadingProp => WrappedComponent => {
    return class Loading extends Component {
        render() {
            console.log(this.props);
            return isEmpty(this.props[loadingProp]) ? <PropagateLoader sizeUnit={"px"} size={24} loading={true} /> : <WrappedComponent {...this.props} />
        }
    }
}

export default LoadingHOC;