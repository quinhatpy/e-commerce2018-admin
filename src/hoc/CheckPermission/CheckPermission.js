import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as linkConfig from './../../constants/link';
import { Spin  } from 'antd';

const CheckPermission = permission => WrappedComponent => {
    class CheckPermissionClass extends Component {
        render() {
            const { user } = this.props;
            let permissions = {};
            if (user.role)
                permissions = user.role.permissions;

            if (typeof permissions[permission] === 'undefined')
                return (
                    <div className="text-center" style={{ marginTop: '80px' }}>
                    <Spin size="large" /> <Spin size="large" /> <Spin size="large" /> 
                    </div>
                )
            else return permissions[permission] ? <WrappedComponent {...this.props} /> : <Redirect to={linkConfig.PAGE403} />
        }
    }
    const mapStateToProps = state => {
        return {
            user: state.auth.user
        }
    }
    return connect(mapStateToProps)(CheckPermissionClass);
}

export default CheckPermission;