import React from 'react';
import { withRouter } from 'react-router-dom';
import { Route, Link } from 'react-router-dom';

const LinkCustom = ({ label, to, params, activeOnlyWhenExact, onPageChange, activeFirstPage, icon }) => {
    return (
        <Route
            path={to}
            exact={activeOnlyWhenExact}
            children={({ match }) => {
                let active =  match ? 'active' :'';
                if (activeFirstPage) active = 'active';
                return (
                    <li className={active}>
                        <Link to={{
                            pathname: to,
                            search: params
                        }} onClick={() => onPageChange()}>{icon} {label}</Link>
                    </li>
                )
            }} />
    )
}
export default withRouter(LinkCustom);