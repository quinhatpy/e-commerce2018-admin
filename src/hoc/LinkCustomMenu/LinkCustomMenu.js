import React from 'react';
import { Route, Link, withRouter } from 'react-router-dom';

const LinkCustomMenu = ({ label, to, activeOnlyWhenExact, icon, onChangePage}) => {
    const match = window.location.pathname === to;
    return (
        <Route
            path={to}
            exact={activeOnlyWhenExact}
            children={() => {
                // console.log(props);
                let active = match ? 'active' : '';
                return (
                    <li className={active}>
                        <Link 
                        to={to}
                         onClick={() => onChangePage(to)}>{icon} {label}</Link>
                    </li>
                )
            }} />
    )
}
export default withRouter(LinkCustomMenu);