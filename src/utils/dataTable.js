import React from 'react';
import TableHeadColumn from './../components/UI/TableHeadColumn';

const renderTableHeadColumn = (tableHeadColumns, sortBy, handleChangeSortColumn) => {
    
    return (<tr>
        {
            tableHeadColumns.map(th => {
                return <TableHeadColumn key={th.name} {...th} sortBy={sortBy} onClickColumn={(sortName, sortValue) => handleChangeSortColumn(sortName, sortValue)} />
            })
        }
    </tr>);
}
export default renderTableHeadColumn;