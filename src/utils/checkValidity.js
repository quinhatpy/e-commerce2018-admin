import validator from 'validator';
import isEmpty from 'lodash/isEmpty';

const checkValidity = (formElement, inputIdentifier) => {
    const { validation: rules, value, label } = formElement[inputIdentifier];

    let isValid = true;
    let errorMessage = '';

    if (!rules) {
        return true;
    }

    if (rules.required) {
        isValid = value.trim() !== '' && isValid;
        if (validator.isEmpty(value)) errorMessage = `Please input your ${label.toLowerCase()}`;
    }

    if (!isEmpty(value) && rules.isEmail) {
        isValid = validator.isEmail(value) && isValid;
        if (!isValid) errorMessage = 'The input is not valid E-mail!';
    }

    if (!isEmpty(value) && rules.minLength) {
        isValid = value.length >= rules.minLength && isValid;
        if (!isValid) errorMessage = `The ${label.toLowerCase()} must be at least ${rules.minLength} characters!`;
    }

    if (!isEmpty(value) && rules.maxLength) {
        isValid = value.length <= rules.maxLength && isValid;
        if (!isValid) errorMessage = `The ${label.toLowerCase()} may not be greater than ${rules.maxLength} characters!`;
    }

    if (!isEmpty(value) && rules.isEqualWith) {
        isValid = value === formElement[rules.isEqualWith].value && isValid;
        if (!isValid) errorMessage = `The ${label.toLowerCase()} does not match!`;
    }

    if (!isEmpty(value) && rules.isMobilePhone) {
        isValid = validator.isMobilePhone(value, 'vi-VN') && isValid;
        if (!isValid) errorMessage = 'The input is not valid mobile number!';
    }

    if (!isEmpty(value) && rules.isInt) {
        isValid = validator.isInt(value) && isValid;
        if (!isValid) errorMessage = `The ${label.toLowerCase()} must be an integer!`;
    }

    if (!isEmpty(value) && rules.unique) {
        let index = rules.unique.data.findIndex(item => {
            return value === item[rules.unique.key] && parseInt(rules.unique.exclude) !== item.id;
        });
        isValid = index === -1 && isValid;

        if (!isValid) errorMessage = `Duplicate entry ${value}, please input another slug!`;
    }

    if (!isEmpty(value) && rules.isURL) {
        isValid = validator.isURL(value) && isValid;
        if (!isValid) errorMessage = 'The input URL format is invalid!';
    }

    return {
        isValid,
        errorMessage
    }
}

export default checkValidity;
