import { message } from 'antd';

message.config({
    top: 51,
});

const success = (content, duration = 1.5) => {
    message.success(content, duration)
};

const warning = (content, duration = 1.5) => {
    message.success(content, duration)
};

const error = (content, duration = 1.5) => {
    message.success(content, duration)
};

const info = (content, duration = 1.5) => {
    message.success(content, duration)
};

const loading = (content, duration = 1.5) => {
    message.success(content, duration)
};

const Toast = {
    success,
    warning,
    error,
    info,
    loading,
}

export default Toast;