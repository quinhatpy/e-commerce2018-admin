const scrollTop = () => {
    let timeOut = setInterval(() => {
        if (window.scrollY === 0)
            clearTimeout(timeOut);
        window.scrollTo(0, window.scrollY - 10);
    }, 5)
}
export default scrollTop;
