const checkPermission = (permission, user, children) => {
    let permissions = {};
    if (user.role)
        permissions = user.role.permissions;

    if (typeof permissions[permission] === 'undefined') return null;

    if (permissions[permission]) return children;
    return null;
}

export default checkPermission;