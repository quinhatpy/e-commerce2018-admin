import AsyncComponent from '../hoc/AsyncComponent/AsyncComponent';
import * as linkConfig from './link';

const routes = [
    {
        path: '/',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Dashboard'))),
        protected: true
    },
    {
        path: linkConfig.DASHBOARD,
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Dashboard'))),
        protected: true
    },
    {
        path: linkConfig.USER_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/User/List'))),
        protected: true
    },
    {
        path: linkConfig.USER_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/User/Add'))),
        protected: true
    },
    {
        path: linkConfig.USER_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/User/Edit'))),
        protected: true
    },
    {
        path: linkConfig.USER_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/User/Detail'))),
        protected: true
    },
    {
        path: linkConfig.CUSTOMER_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Customer/List'))),
        protected: true
    },
    {
        path: linkConfig.CUSTOMER_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Customer/Add'))),
        protected: true
    },
    {
        path: linkConfig.CUSTOMER_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Customer/Edit'))),
        protected: true
    },
    {
        path: linkConfig.CUSTOMER_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Customer/Detail'))),
        protected: true
    },
    {
        path: linkConfig.ROLE_LIST,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Role/List'))),
        protected: true
    },
    {
        path: linkConfig.ROLE_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Role/Add'))),
        protected: true
    },
    {
        path: linkConfig.ROLE_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Role/Edit'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Category/List'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Category/Add'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Category/Edit'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Category/Detail'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_BANNER_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/CategoryBanner/List/CategoryBannerList'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_BANNER_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/CategoryBanner/Add'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_BANNER_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/CategoryBanner/Edit'))),
        protected: true
    },
    {
        path: linkConfig.CATEGORY_BANNER_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/CategoryBanner/Detail'))),
        protected: true
    },
    {
        path: linkConfig.ATTRIBUTE_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Attribute/List'))),
        protected: true
    },
    {
        path: linkConfig.ATTRIBUTE_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Attribute/Add'))),
        protected: true
    },
    {
        path: linkConfig.ATTRIBUTE_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Attribute/Edit'))),
        protected: true
    },
    {
        path: linkConfig.ATTRIBUTE_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Attribute/Detail'))),
        protected: true
    },
    {
        path: linkConfig.BANNER_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Banner/List'))),
        protected: true
    },
    {
        path: linkConfig.BANNER_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Banner/Add'))),
        protected: true
    },
    {
        path: linkConfig.BANNER_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Banner/Edit'))),
        protected: true
    },
    {
        path: linkConfig.BANNER_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Banner/Detail'))),
        protected: true
    },
    {
        path: linkConfig.BRAND_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Brand/List'))),
        protected: true
    },
    {
        path: linkConfig.BRAND_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Brand/Add'))),
        protected: true
    },
    {
        path: linkConfig.BRAND_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Brand/Edit'))),
        protected: true
    },
    {
        path: linkConfig.BRAND_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Brand/Detail'))),
        protected: true
    },
    {
        path: linkConfig.PROVINCE_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Province/List'))),
        protected: true
    },
    {
        path: linkConfig.PROVINCE_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Province/Add'))),
        protected: true
    },
    {
        path: linkConfig.PROVINCE_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Province/Edit'))),
        protected: true
    },
    {
        path: linkConfig.DISTRICT_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/District/List'))),
        protected: true
    },
    {
        path: linkConfig.DISTRICT_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/District/Add'))),
        protected: true
    },
    {
        path: linkConfig.DISTRICT_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/District/Edit'))),
        protected: true
    },
    {
        path: linkConfig.WARD_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/Ward/List/WardList'))),
        protected: true
    },
    {
        path: linkConfig.WARD_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('../pages/Ward/Add/WardAdd'))),
        protected: true
    },
    {
        path: linkConfig.WARD_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('../pages/Ward/Edit/WardEdit'))),
        protected: true
    },
    {
        path: linkConfig.BRANCH_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/Branch/List/BranchList'))),
        protected: true
    },
    {
        path: linkConfig.BRANCH_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('../pages/Branch/Add/BranchAdd'))),
        protected: true
    },
    {
        path: linkConfig.BRANCH_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('../pages/Branch/Edit/BranchEdit'))),
        protected: true
    },
    {
        path: linkConfig.BRANCH_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Branch/Detail'))),
        protected: true
    },
    {
        path: linkConfig.MENU_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Menu/List'))),
        protected: true
    },
    {
        path: linkConfig.MENU_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Menu/Add'))),
        protected: true
    },
    {
        path: linkConfig.MENU_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Menu/Edit'))),
        protected: true
    },
    {
        path: linkConfig.MENU_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Menu/Detail'))),
        protected: true
    },
    {
        path: linkConfig.GALLERY_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('./../pages/Gallery/List'))),
        protected: true
    },
    {
        path: linkConfig.GALLERY_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Gallery/Add'))),
        protected: true
    },
    {
        path: linkConfig.ARTICLE_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/Article/List/ArticleList'))),
        protected: true
    },
    {
        path: linkConfig.ARTICLE_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('../pages/Article/Add/ArticleAdd'))),
        protected: true
    },
    {
        path: linkConfig.ARTICLE_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('../pages/Article/Edit/ArticleEdit'))),
        protected: true
    },
    {
        path: linkConfig.ARTICLE_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Article/Detail'))),
        protected: true
    },
    {
        path: linkConfig.PRODUCT_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/Product/List/ProductList'))),
        protected: true
    },
    {
        path: linkConfig.PRODUCT_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('../pages/Product/Add/ProductAdd'))),
        protected: true
    },
    {
        path: linkConfig.PRODUCT_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('../pages/Product/Edit/ProductEdit'))),
        protected: true
    },
    {
        path: linkConfig.PRODUCT_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Product/Detail'))),
        protected: true
    },
    {
        path: linkConfig.ORDER_LIST + ':page?',
        exact: true,
        component: AsyncComponent(() => (import('../pages/Order/List/OrderList'))),
        protected: true
    },
    {
        path: linkConfig.ORDER_CREATE,
        exact: false,
        component: AsyncComponent(() => (import('../pages/Order/Add/OrderAdd'))),
        protected: true
    },
    {
        path: linkConfig.ORDER_EDIT + ':id',
        exact: false,
        component: AsyncComponent(() => (import('../pages/Order/Edit/OrderEdit'))),
        protected: true
    },
    {
        path: linkConfig.ORDER_DETAIL + ':id',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/Order/Detail'))),
        protected: true
    },
    {
        path: linkConfig.PAGE403,
        exact: false,
        component: AsyncComponent(() => (import('./../pages/ErrorPages/403'))),
        protected: false
    },
    {
        path: '',
        exact: false,
        component: AsyncComponent(() => (import('./../pages/ErrorPages/404'))),
        protected: false
    }
];

export default routes;

