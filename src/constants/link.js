export const LOGIN = '/login/';
export const FORGOT_PASSWORD = '/forgot-password/';
export const RESET_PASSWORD = '/reset-password/';

export const DASHBOARD = '/dashboard/';

export const PAGE403 = '/403/';

export const USER_LIST = '/users/';
export const USER_CREATE = '/user/create/';
export const USER_EDIT = '/user/edit/';
export const USER_DETAIL = '/user/detail/';

export const ROLE_LIST = '/roles/';
export const ROLE_CREATE = '/role/create/';
export const ROLE_EDIT = '/role/edit/';

export const CUSTOMER_LIST = '/customers/';
export const CUSTOMER_CREATE = '/customer/create/';
export const CUSTOMER_EDIT = '/customer/edit/';
export const CUSTOMER_DETAIL = '/customer/detail/';

export const CATEGORY_LIST = '/categories/';
export const CATEGORY_CREATE = '/category/create/';
export const CATEGORY_EDIT = '/category/edit/';
export const CATEGORY_DETAIL = '/category/detail/';

export const CATEGORY_BANNER_LIST = '/category-banners/';
export const CATEGORY_BANNER_CREATE = '/category-banner/create/';
export const CATEGORY_BANNER_EDIT = '/category-banner/edit/';
export const CATEGORY_BANNER_DETAIL = '/category-banner/detail/';

export const ATTRIBUTE_LIST = '/attributes/';
export const ATTRIBUTE_CREATE = '/attribute/create/';
export const ATTRIBUTE_EDIT = '/attribute/edit/';
export const ATTRIBUTE_DETAIL = '/attribute/detail/';

export const BANNER_LIST = '/banners/';
export const BANNER_CREATE = '/banner/create/';
export const BANNER_EDIT = '/banner/edit/';
export const BANNER_DETAIL = '/banner/detail/';

export const BRAND_LIST = '/brands/';
export const BRAND_CREATE = '/brand/create/';
export const BRAND_EDIT = '/brand/edit/';
export const BRAND_DETAIL = '/brand/detail/';

export const PROVINCE_LIST = '/provinces/';
export const PROVINCE_CREATE = '/province/create/';
export const PROVINCE_EDIT = '/province/edit/';
export const PROVINCE_DETAIL = '/province/detail/';

export const DISTRICT_LIST = '/districts/';
export const DISTRICT_CREATE = '/district/create/';
export const DISTRICT_EDIT = '/district/edit/';
export const DISTRICT_DETAIL = '/district/detail/';

export const WARD_LIST = '/wards/';
export const WARD_CREATE = '/ward/create/';
export const WARD_EDIT = '/ward/edit/';
export const WARD_DETAIL = '/ward/detail/';

export const BRANCH_LIST = '/branches/';
export const BRANCH_CREATE = '/branch/create/';
export const BRANCH_EDIT = '/branch/edit/';
export const BRANCH_DETAIL = '/branch/detail/';

export const MENU_LIST = '/menus/';
export const MENU_CREATE = '/menu/create/';
export const MENU_EDIT = '/menu/edit/';
export const MENU_DETAIL = '/menu/detail/';

export const GALLERY_LIST = '/galleries/';
export const GALLERY_CREATE = '/gallery/create/';
export const GALLERY_EDIT = '/gallery/edit/';
export const GALLERY_DETAIL = '/gallery/detail/';

export const ARTICLE_LIST = '/articles/';
export const ARTICLE_CREATE = '/article/create/';
export const ARTICLE_EDIT = '/article/edit/';
export const ARTICLE_DETAIL = '/article/detail/';

export const PRODUCT_LIST = '/products/';
export const PRODUCT_CREATE = '/product/create/';
export const PRODUCT_EDIT = '/product/edit/';
export const PRODUCT_DETAIL = '/product/detail/';

export const ORDER_LIST = '/orders/';
export const ORDER_CREATE = '/order/create/';
export const ORDER_EDIT = '/order/edit/';
export const ORDER_DETAIL = '/order/detail/';