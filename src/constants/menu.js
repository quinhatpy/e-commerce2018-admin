import * as linkConfig from './link';
import * as permission from './permission';
import React from 'react';

const menus = [
    {
        groupName: 'GENERAL',
        menuItems: [
            {
                itemName: 'Dashboard',
                icon: <i className="fal fa-tachometer-alt" />,
                link: linkConfig.DASHBOARD,
                permission: 'auth'
            }
        ]
    },
    {
        groupName: 'ACCOUNT MANAGEMENT',
        menuItems: [
            {
                itemName: 'User management',
                icon: <i className="fal fa-user-shield" />,
                link: linkConfig.USER_LIST,
                children: [
                    {
                        itemName: 'Add user',
                        icon: <i className="fal fa-user-plus" />,
                        link: linkConfig.USER_CREATE,
                        permission: permission.USER_CREATE
                    },
                    {
                        itemName: 'List user',
                        icon: <i className="fal fa-user-friends" />,
                        link: linkConfig.USER_LIST,
                        permission: permission.USER_INDEX
                    }
                ]
            },
            {
                itemName: 'Customer management',
                icon: <i className="fal fa-user-alt" />,
                link: linkConfig.CUSTOMER_LIST,
                children: [
                    {
                        itemName: 'Add customer',
                        icon: <i className="fal fa-user-plus" />,
                        link: linkConfig.CUSTOMER_CREATE,
                        permission: permission.CUSTOMER_CREATE
                    },
                    {
                        itemName: 'List customer',
                        icon: <i className="fal fa-users" />,
                        link: linkConfig.CUSTOMER_LIST,
                        permission: permission.CUSTOMER_INDEX
                    }
                ]
            },
            {
                itemName: 'Role management',
                icon: <i className="fal fa-users-cog " />,
                link: linkConfig.ROLE_LIST,
                children: [
                    {
                        itemName: 'Add role',
                        icon: <i className="fal fa-plus-hexagon" />,
                        link: linkConfig.ROLE_CREATE,
                        permission: permission.ROLE_CREATE
                    },
                    {
                        itemName: 'List role',
                        icon: <i className="fal fa-list" />,
                        link: linkConfig.ROLE_LIST,
                        permission: permission.ROLE_INDEX
                    }
                ]
            }
        ]
    },
    {
        groupName: 'PRODUCT MANAGEMENT',
        menuItems: [
            {
                itemName: 'Category management',
                icon: <i className="fal fa-list-alt" />,
                link: linkConfig.CATEGORY_LIST,
                children: [
                    {
                        itemName: 'Add category',
                        icon: <i className="fal fa-plus-circle" />,
                        link: linkConfig.CATEGORY_CREATE,
                        permission: permission.CATEGORY_CREATE
                    },
                    {
                        itemName: 'List category',
                        icon: <i className="fal fa-list" />,
                        link: linkConfig.CATEGORY_LIST,
                        permission: permission.CATEGORY_INDEX
                    },
                    {
                        itemName: 'Add category banner',
                        icon: <i className="fal fa-file-plus" />,
                        link: linkConfig.CATEGORY_BANNER_CREATE,
                        permission: permission.CATEGORY_BANNER_CREATE
                    },
                    {
                        itemName: 'List category banner',
                        icon: <i className="fal fa-images" />,
                        link: linkConfig.CATEGORY_BANNER_LIST,
                        permission: permission.CATEGORY_BANNER_INDEX
                    },
                ]
            },
            {
                itemName: 'Attribute management',
                icon: <i className="fal fa-bezier-curve" />,
                link: linkConfig.ATTRIBUTE_LIST,
                children: [
                    {
                        itemName: 'Add attribute',
                        icon: <i className="fal fa-plus-circle" />,
                        link: linkConfig.ATTRIBUTE_CREATE,
                        permission: permission.ATTRIBUTE_CREATE
                    },
                    {
                        itemName: 'List attribute',
                        icon: <i className="fal fa-container-storage" />,
                        link: linkConfig.ATTRIBUTE_LIST,
                        permission: permission.ATTRIBUTE_INDEX
                    }
                ]
            },
            {
                itemName: 'Brand management',
                icon: <i className="fal fa-badge" />,
                link: linkConfig.BRAND_LIST,
                children: [
                    {
                        itemName: 'Add brand',
                        icon: <i className="fal fa-plus-circle" />,
                        link: linkConfig.BRAND_CREATE,
                        permission: permission.BRAND_CREATE
                    },
                    {
                        itemName: 'List brand',
                        icon: <i className="fal fa-th-list" />,
                        link: linkConfig.BRAND_LIST,
                        permission: permission.BRAND_INDEX
                    }
                ]
            },
            {
                itemName: 'Product management',
                icon: <i className="fal fa-box-alt" />,
                link: linkConfig.PRODUCT_LIST,
                children: [
                    {
                        itemName: 'Add product',
                        icon: <i className="fal fa-inbox-in" />,
                        link: linkConfig.PRODUCT_CREATE,
                        permission: permission.PRODUCT_CREATE
                    },
                    {
                        itemName: 'List product',
                        icon: <i className="fal fa-boxes" />,
                        link: linkConfig.PRODUCT_LIST,
                        permission: permission.PRODUCT_INDEX
                    }
                ]
            },
            {
                itemName: 'Order management',
                icon: <i className="fal fa-file-invoice" />,
                link: linkConfig.ORDER_LIST,
                children: [
                    {
                        itemName: 'Add order',
                        icon: <i className="fal fa-cart-plus" />,
                        link: linkConfig.ORDER_CREATE,
                        permission: permission.ORDER_CREATE
                    },
                    {
                        itemName: 'List order',
                        icon: <i className="fal fa-list-alt" />,
                        link: linkConfig.ORDER_LIST,
                        permission: permission.ORDER_INDEX
                    }
                ]
            }
        ]
    },
    {
        groupName: 'GENERAL MANAGEMENT',
        menuItems: [
            {
                itemName: 'Article management',
                icon: <i className="fal fa-newspaper"/>,
                link: linkConfig.ARTICLE_LIST,
                children: [
                    {
                        itemName: 'Add Article',
                        icon: <i className="fal fa-plus-circle"/>,
                        link: linkConfig.ARTICLE_CREATE,
                        permission: permission.ARTICLE_CREATE
                    },
                    {
                        itemName: 'List Article',
                        icon: <i className="fal fa-th-list"/>,
                        link: linkConfig.ARTICLE_LIST,
                        permission: permission.ARTICLE_INDEX
                    },
                ]
            },
            {
                itemName: 'Menu management',
                icon: <i className="fal fa-bars"/>,
                link: linkConfig.BRANCH_LIST,
                children: [
                    {
                        itemName: 'Add Menu',
                        icon: <i className="fal fa-plus-circle"/>,
                        link: linkConfig.MENU_CREATE,
                        permission: permission.MENU_CREATE
                    },
                    {
                        itemName: 'List Menu',
                        icon: <i className="fal fa-th-list"/>,
                        link: linkConfig.MENU_LIST,
                        permission: permission.MENU_INDEX
                    },
                ]
            },
            {
                itemName: 'Banner management',
                icon: <i className="fal fa-images" />,
                link: linkConfig.BANNER_LIST,
                children: [
                    {
                        itemName: 'Add banner',
                        icon: <i className="fal fa-file-plus" />,
                        link: linkConfig.BANNER_CREATE,
                        permission: permission.BANNER_CREATE
                    },
                    {
                        itemName: 'List banner',
                        icon: <i className="fal fa-images" />,
                        link: linkConfig.BANNER_LIST,
                        permission: permission.BANNER_INDEX
                    },
                ]
            },
            {
                itemName: 'Gallery',
                icon: <i className="fal fa-file-alt" />,
                link: linkConfig.GALLERY_LIST,
                children: [
                    {
                        itemName: 'Upload file',
                        icon: <i className="fal fa-file-upload" />,
                        link: linkConfig.GALLERY_CREATE,
                        permission: permission.GALLERY_CREATE
                    },
                    {
                        itemName: 'Media library',
                        icon: <i className="fal fa-copy" />,
                        link: linkConfig.GALLERY_LIST,
                        permission: permission.GALLERY_INDEX
                    },
                ]
            },
            {
                itemName: 'Administrative Units',
                icon: <i className="fal fa-globe-americas" />,
                link: linkConfig.PROVINCE_LIST,
                children: [
                    {
                        itemName: 'Province',
                        icon: <i className="fab fa-fort-awesome" />,
                        link: linkConfig.PROVINCE_LIST,
                        permission: permission.PROVINCE_INDEX
                    },
                    {
                        itemName: 'District',
                        icon: <i className="fal fa-hotel" />,
                        link: linkConfig.DISTRICT_LIST,
                        permission: permission.DISTRICT_INDEX
                    },
                    {
                        itemName: 'Ward',
                        icon: <i className="fal fa-archway" />,
                        link: linkConfig.WARD_LIST,
                        permission: permission.WARD_INDEX
                    },
                ]
            },
            {
                itemName: 'Branch management',
                icon: <i className="fal fa-code-branch" />,
                link: linkConfig.BRANCH_LIST,
                children: [
                    {
                        itemName: 'Add Branch',
                        icon: <i className="fal fa-plus-circle" />,
                        link: linkConfig.BRANCH_CREATE,
                        permission: permission.BRANCH_CREATE
                    },
                    {
                        itemName: 'List Branch',
                        icon: <i className="fal fa-th-list" />,
                        link: linkConfig.BRANCH_LIST,
                        permission: permission.BRANCH_INDEX
                    },
                ]
            },
        ]
    }
]

export default menus;