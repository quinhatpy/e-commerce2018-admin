export const ARTICLE_INDEX = 'article.index';
export const ARTICLE_SHOW = 'article.show';
export const ARTICLE_CREATE = 'article.create';
export const ARTICLE_UPDATE = 'article.update';
export const ARTICLE_DELETE = 'article.delete';

export const ATTRIBUTE_INDEX = 'attribute.index';
export const ATTRIBUTE_SHOW = 'attribute.show';
export const ATTRIBUTE_CREATE = 'attribute.create';
export const ATTRIBUTE_UPDATE = 'attribute.update';
export const ATTRIBUTE_DELETE = 'attribute.delete';

export const BANNER_INDEX = 'banner.index';
export const BANNER_SHOW = 'banner.show';
export const BANNER_CREATE = 'banner.create';
export const BANNER_UPDATE = 'banner.update';
export const BANNER_DELETE = 'banner.delete';

export const BRANCH_INDEX = 'branch.index';
export const BRANCH_SHOW = 'branch.show';
export const BRANCH_CREATE = 'branch.create';
export const BRANCH_UPDATE = 'branch.update';
export const BRANCH_DELETE = 'branch.delete';

export const BRAND_INDEX = 'brand.index';
export const BRAND_SHOW = 'brand.show';
export const BRAND_CREATE = 'brand.create';
export const BRAND_UPDATE = 'brand.update';
export const BRAND_DELETE = 'brand.delete';

export const CATEGORY_INDEX = 'category.index';
export const CATEGORY_SHOW = 'category.show';
export const CATEGORY_CREATE = 'category.create';
export const CATEGORY_UPDATE = 'category.update';
export const CATEGORY_DELETE = 'category.delete';

export const CATEGORY_BANNER_INDEX = 'category_banner.index';
export const CATEGORY_BANNER_SHOW = 'category_banner.show';
export const CATEGORY_BANNER_CREATE = 'category_banner.create';
export const CATEGORY_BANNER_UPDATE = 'category_banner.update';
export const CATEGORY_BANNER_DELETE = 'category_banner.delete';

export const CUSTOMER_INDEX = 'customer.index';
export const CUSTOMER_SHOW = 'customer.show';
export const CUSTOMER_CREATE = 'customer.create';
export const CUSTOMER_UPDATE = 'customer.update';
export const CUSTOMER_DELETE = 'customer.delete';

export const DELIVERY_METHOD_INDEX = 'delivery_method.index';
export const DELIVERY_METHOD_SHOW = 'delivery_method.show';
export const DELIVERY_METHOD_CREATE = 'delivery_method.create';
export const DELIVERY_METHOD_UPDATE = 'delivery_method.update';
export const DELIVERY_METHOD_DELETE = 'delivery_method.delete';

export const DISTRICT_INDEX = 'district.index';
export const DISTRICT_SHOW = 'district.show';
export const DISTRICT_CREATE = 'district.create';
export const DISTRICT_UPDATE = 'district.update';
export const DISTRICT_DELETE = 'district.delete';

export const MENU_INDEX = 'menu.index';
export const MENU_SHOW = 'menu.show';
export const MENU_CREATE = 'menu.create';
export const MENU_UPDATE = 'menu.update';
export const MENU_DELETE = 'menu.delete';

export const ORDER_INDEX = 'order.index';
export const ORDER_SHOW = 'order.show';
export const ORDER_CREATE = 'order.create';
export const ORDER_UPDATE = 'order.update';
export const ORDER_DELETE = 'order.delete';

export const PRODUCT_INDEX = 'product.index';
export const PRODUCT_SHOW = 'product.show';
export const PRODUCT_CREATE = 'product.create';
export const PRODUCT_UPDATE = 'product.update';
export const PRODUCT_DELETE = 'product.delete';

export const PROVINCE_INDEX = 'province.index';
export const PROVINCE_SHOW = 'province.show';
export const PROVINCE_CREATE = 'province.create';
export const PROVINCE_UPDATE = 'province.update';
export const PROVINCE_DELETE = 'province.delete';

export const ROLE_INDEX = 'role.index';
export const ROLE_SHOW = 'role.show';
export const ROLE_CREATE = 'role.create';
export const ROLE_UPDATE = 'role.update';
export const ROLE_DELETE = 'role.delete';

export const USER_INDEX = 'user.index';
export const USER_SHOW = 'user.show';
export const USER_CREATE = 'user.create';
export const USER_UPDATE = 'user.update';
export const USER_DELETE = 'user.delete';

export const WARD_INDEX = 'ward.index';
export const WARD_SHOW = 'ward.show';
export const WARD_CREATE = 'ward.create';
export const WARD_UPDATE = 'ward.update';
export const WARD_DELETE = 'ward.delete';

export const WISHLIST_INDEX = 'wishlist.index';
export const WISHLIST_SHOW = 'wishlist.show';
export const WISHLIST_CREATE = 'wishlist.create';
export const WISHLIST_UPDATE = 'wishlist.update';
export const WISHLIST_DELETE = 'wishlist.delete';

export const GALLERY_INDEX = 'gallery.index';
export const GALLERY_SHOW = 'gallery.show';
export const GALLERY_CREATE = 'gallery.create';
export const GALLERY_UPDATE = 'gallery.update';
export const GALLERY_DELETE = 'gallery.delete';