import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <footer className="main-footer">
                <div className="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright © 2018 <a href="https://nhatle.net">Nhat Le</a>.</strong> All rights reserved.
            </footer>
        )
    }
}
