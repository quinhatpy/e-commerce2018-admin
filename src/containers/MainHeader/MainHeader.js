import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserAuthRequest, logoutRequest } from './../../actions';
import Logo from '../../components/Logo/Logo';
import DropdownUserMenu from '../../components/DropdownUserMenu';
import LoadingBar from 'react-redux-loading-bar';
class MainHeader extends Component {
    componentDidMount() {
        this.props.onGetUserRequest();
    }
    render() {
        return (
            <header className="main-header">
                <LoadingBar style={{ backgroundColor: '#f39c12 ', height: '5px', zIndex: 99999999 }} />
                {/* Logo */}
                <Logo />
                {/* Header Navbar: style can be found in header.less */}
                <nav className="navbar navbar-static-top">
                    {/* Sidebar toggle button*/}
                    <a href="/" className="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span className="sr-only">Toggle navigation</span>
                    </a>
                    <div className="navbar-custom-menu">
                        <ul className="nav navbar-nav">
                            {/* User Account: style can be found in dropdown.less */}
                            <DropdownUserMenu user={this.props.user} logged={() => this.props.onLogoutRequest()} />
                            {/* Control Sidebar Toggle Button */}
                        </ul>
                    </div>
                </nav>
            </header>

        )
    }
}

MainHeader.propTypes = {
    user: PropTypes.object.isRequired,
    onGetUserRequest: PropTypes.func.isRequired,
    onLogoutRequest: PropTypes.func.isRequired
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}
const mapDispatchToProps = (dispatch, props) => {
    return {
        onGetUserRequest: () => { dispatch(getUserAuthRequest()) },
        onLogoutRequest: () => { dispatch(logoutRequest()) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainHeader);