import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import menus from './../../constants/menu';
import UserPanel from '../../components/UserPanel';
import SidebarMenu from '../../components/SidebarMenu/SidebarMenu';

class MainSidebar extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        menus
      }
    }
    
    render() {
        const { user } = this.props;
        const { menus } = this.state;

        return (
            <aside className="main-sidebar">
                {/* sidebar: style can be found in sidebar.less */}
                <section className="sidebar">
                    {/* Sidebar user panel */}
                    <UserPanel user={user} />
                    {/* sidebar menu: : style can be found in sidebar.less */}
                    <SidebarMenu menus={menus} user={user} />
                </section>
                {/* /.sidebar */}
            </aside>
        )
    }
}

MainSidebar.propTypes = {
    user: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
    return {
        user: state.auth.user
    }
}


export default connect(mapStateToProps)(MainSidebar);