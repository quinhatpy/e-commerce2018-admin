import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCustomerRequest, updateCustomerRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { CUSTOMER_LIST } from '../../../constants/link';
import { CUSTOMER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class CustomerEdit extends Component {
    state = {
        pageTitle: 'Edit Customer',
        breadcrumbItems: [
            {
                name: 'Customer management',
                link: CUSTOMER_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Full name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    name: 'email',
                    placeholder: 'Enter email',
                    readOnly: true
                },
                label: 'Email address',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password',
                    placeholder: "Leave blank if you don't want to change"
                },
                value: '',
                validation: {
                    minLength: 6
                },
                label: 'Password',
                errorMessage: '',
                valid: true,
                touched: false
            },
            password_confirmation: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password_confirmation',
                    placeholder: 'Enter password confirmation'
                },
                label: 'Password Confirmation',
                value: '',
                validation: {
                    isEqualWith: 'password'
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'phone',
                    placeholder: 'Enter mobile phone in the format 0xxxxxxxxx'
                },
                label: 'Mobile phone',
                value: '',
                validation: {
                    isMobilePhone: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            address: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'address',
                    placeholder: 'Enter address'
                },
                label: 'Address',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: true,

    }

    componentDidMount() {
        const customerId = this.props.match.params.id;
        this.props.onFetchCustomerRequest(customerId);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { item } = nextProps;

        if (item) {
            stateClone.formElements.name.value = item.name;
            stateClone.formElements.email.value = item.email;
            stateClone.formElements.phone.value = item.phone;
            stateClone.formElements.address.value = item.address;
        }
        this.setState(stateClone);

    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateCustomerRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;
        this.setState(stateClone);
        

        const customerId = this.props.match.params.id;
        this.props.onFetchCustomerRequest(customerId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                        <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert,
        item: state.customer.item
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCustomerRequest: id => { dispatch(fetchCustomerRequest(id)) },
        onUpdateCustomerRequest: (id, data) => { dispatch(updateCustomerRequest(id, data)) }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CUSTOMER_UPDATE)(CustomerEdit));