import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCustomerRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { CUSTOMER_LIST } from '../../../constants/link';
import { CUSTOMER_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class CustomerDetail extends Component {
    state = {
        pageTitle: 'Customer Detail',
        breadcrumbItems: [
            {
                name: 'Customer management',
                link: CUSTOMER_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchCustomerRequest
        } = this.props;

        fetchCustomerRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { customer } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{customer.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>{customer.email}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Phone Number</th>
                                                        <td>{customer.phone}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Address</th>
                                                        <td>{customer.address}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(customer.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(customer.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        customer: state.customer.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCustomerRequest: id => { dispatch(fetchCustomerRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CUSTOMER_SHOW)(CustomerDetail));