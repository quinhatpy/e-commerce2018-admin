import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createCustomerRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { CUSTOMER_LIST } from '../../../constants/link';
import { CUSTOMER_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class CustomerAdd extends Component {
    state = {
        pageTitle: 'Add Customer',
        breadcrumbItems: [
            {
                name: 'Customer management',
                link: CUSTOMER_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Full name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    name: 'email',
                    placeholder: 'Enter email'
                },
                label: 'Email address',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password',
                    placeholder: 'Enter password must be at least 6 characters'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                label: 'Password',
                errorMessage: '',
                valid: false,
                touched: false
            },
            password_confirmation: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password_confirmation',
                    placeholder: 'Enter password confirmation'
                },
                label: 'Password Confirmation',
                value: '',
                validation: {
                    required: true,
                    isEqualWith: 'password'
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'phone',
                    placeholder: 'Enter mobile phone in the format 0xxxxxxxxx'
                },
                label: 'Mobile phone',
                value: '',
                validation: {
                    isMobilePhone: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            address: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'address',
                    placeholder: 'Enter address'
                },
                label: 'Address',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: false,

    }

    componentDidMount() {
        document.title = this.state.pageTitle;
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        if (inputIdentifier === 'birthday')
            updatedFormElement.value = event.format('YYYY-MM-DD');
        else {
            updatedFormElement.value = event.target.value;
            // get image preview
            if (event.target.files && event.target.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    updatedFormElement.elementConfig.data = e.target.result;
                    const stateClone = { ...this.state };
                    updatedForm[inputIdentifier] = updatedFormElement;
                    stateClone.formElements = updatedForm;
                    this.setState(stateClone);

                }
                reader.readAsDataURL(event.target.files[0]);
            }
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        // for (var pair of formData.entries()) {
        //     console.log(pair[0] + ', ' + pair[1]);
        // }
        this.props.onCreateCustomerRequest(formData);
    }

    handleReset = (event) => {
        // event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';

            if (_.indexOf(['name', 'email', 'password', 'password_confirmation', 'phone'], key) !== -1)
            stateClone.formElements[key].valid = false;
        }
        
        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateCustomerRequest: data => { dispatch(createCustomerRequest(data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CUSTOMER_CREATE)(CustomerAdd));