import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GALLERY_UPDATE } from '../../../constants/permission';
import { updateGalleryRequest } from '../../../actions/actions';
import checkPermission from '../../../utils/checkPermission';
import checkValidity from '../../../utils/checkValidity';
import FormBasic from '../../../components/Form/Basic/FormBasic';

class GalleryEdit extends Component {
    state = {
        formElements: {
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'title',
                    placeholder: 'Enter title'
                },
                label: 'Title',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            alt: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'alt',
                    placeholder: 'Enter alt'
                },
                label: 'Alternate ',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: true,

    }

    componentDidMount() {
        const stateClone = { ...this.state };
        const { item } = this.props;

        stateClone.formElements.title.value = item.title;
        stateClone.formElements.alt.value = item.alt;
        this.setState(stateClone);
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.item.id !== this.props.item.id) {
            const stateClone = { ...this.state };
            const { item } = nextProps;
    
            stateClone.formElements.title.value = item.title;
            stateClone.formElements.alt.value = item.alt;
    
            this.setState(stateClone);
        }
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        if(checkPermission(GALLERY_UPDATE, this.props.userAuth, true)) {
            let formData = new FormData(event.target);
            for (var pair of formData.entries()) {
                console.log(pair[0] + ', ' + pair[1]);
            }
            formData.set('_method', 'PUT');
            this.props.onUpdateGalleryRequest(this.props.item.id, formData);
            this.props.onRefresh(true);
        }
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        const { item } = this.props;

        stateClone.formElements.title.value = item.title;
        stateClone.formElements.alt.value = item.alt;
        this.setState(stateClone);
    }

    render() {
        const {
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <div className="row">
                <div className="col-xs-12">
                    <FormBasic
                        handleSubmit={(event) => this.handleSubmit(event)}
                        handleReset={(event) => this.handleReset(event)}
                        handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                        formElementArray={formElementArray}
                        formIsValid={formIsValid}
                        alert={alert}
                        col1={8}
                        col2={4} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateGalleryRequest: (id, data) => { dispatch(updateGalleryRequest(id, data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(GalleryEdit);