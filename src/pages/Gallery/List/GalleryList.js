import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import queryString from 'query-string';
import isEmpty from 'lodash/isEmpty';
import { Select, Modal, Popconfirm, Icon } from 'antd';
import { GALLERY_LIST } from '../../../constants/link';
import { GALLERY_INDEX, GALLERY_SHOW, GALLERY_DELETE } from '../../../constants/permission';
import { fetchGalleriesRequest, deleteGalleryRequest } from '../../../actions/actions';
import scrollTop from '../../../utils/scrollTop';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import Pagination from '../../../components/Pagination/Pagination';
import SearchBox from '../../../components/SearchBox/SearchBox';
import GalleryEdit from '../Edit/GalleryEdit';
import './style.css';

class GalleryList extends Component {
    state = {
        currentPage: 1,
        limit: 18,
        offset: 0,
        item: {},
        params: {},
        pageTitle: 'List Gallery',
        breadcrumbItems: [
            {
                name: 'Gallery management',
                link: GALLERY_LIST
            },
            {
                name: 'List'
            }
        ],
        isShowDialog: false,
        isNeedRefresh: false,
        sortBy: 'created_at'
    }

    componentDidMount() {
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }
        // if (this.state.currentPage !== 1 && !nextProps.match.params.page) {
        //     const params = queryString.parse(this.props.location.search);
        //     delete params.sort_by;

        //     this.handlePageChange(1, queryString.stringify(params));
        // }

        const queryParam = queryString.parse(this.props.location.search)
        if (queryParam.item && !this.state.isShowDialog && isEmpty(this.state.item)) {
            console.log(1);
            const item = nextProps.galleries.find(gallery => {
                return gallery.id === parseInt(queryParam.item);
            })
            this.setState({ item, isShowDialog: true });
        }
    }

    handlePageChange(pageNumber, queryParams = '') {

        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;
        stateClone.sortBy = stateClone.params.sort_by ? stateClone.params.sort_by : 'created_at';

        stateClone.isShowDialog = false;
        stateClone.isNeedRefresh = false;
        this.setState(stateClone);
        this.props.onFetchGalleriesRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: GALLERY_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;

        let queryParam = queryString.stringify(stateClone.params);
        this.setState(stateClone);

        this.props.history.push({
            pathname: GALLERY_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnShowDialog = (item) => {
        let { params } = { ...this.state };
        params.item = item.id;
        let queryParam = queryString.stringify(params);

        this.props.history.push({
            search: '?' + queryParam
        });

        console.log(item);
        this.setState({ item, isShowDialog: true })
    }

    handleOnHideDialog = (e) => {
        let { params, currentPage, isNeedRefresh } = { ...this.state };
        delete params.item;

        let queryParam = queryString.stringify(params);
        this.props.history.push({
            search: '?' + queryParam
        });

        if (isNeedRefresh) {
            this.handlePageChange(currentPage, queryParam);
        }
        else this.setState({ isShowDialog: false });
    }

    handleRefresh = (isNeedRefresh) => {
        this.setState({ isNeedRefresh })
    }

    handleDelete = (id, offset, limit) => {
        this.props.onDeleteGalleryRequest(id, offset, limit);
        this.handleOnHideDialog();
    }

    renderList(data) {
        return data.length === 0 ? <p className="text-center">No data</p>
            : data.map((item, index) => {

                return (
                    <div className="col-md-2 gallery-item" key={index}>
                        <img src={item.path} className="img-thumbnail" alt="" onClick={() => { this.handleOnShowDialog(item) }} />
                    </div>
                )
            });
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            isShowDialog,
            item,
            sortBy
        } = this.state;
        const {
            totalRows,
            galleries,
            userAuth
        } = this.props;
        const Option = Select.Option;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                        <b>Sort by: </b>
                                        <Select value={sortBy} style={{ width: 200 }} onSelect={this.handleOnSort}>
                                            <Option value="created_at">Created <i className="fal fa-sort-alpha-down"></i></Option>
                                            <Option value="-created_at">Created <i className="fal fa-sort-alpha-up"></i></Option>
                                            <Option value="title">Title <i className="fal fa-sort-alpha-down"></i></Option>
                                            <Option value="-title">Title <i className="fal fa-sort-alpha-up"></i></Option>
                                        </Select>
                                    </h3>
                                    <div className="box-tools">
                                        <SearchBox
                                            placeholder="Search by title"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>

                                </div>
                                <div className="box-body">
                                    {!isEmpty(item) ? checkPermission(
                                        GALLERY_SHOW,
                                        userAuth,
                                        <Modal
                                            width={1000}
                                            title="Attachment Details"
                                            centered
                                            visible={isShowDialog}
                                            onCancel={this.handleOnHideDialog}
                                            footer={null}
                                        >
                                            <div className="row">
                                                <div className="col-md-4">
                                                    <img src={item.path} className="img-thumbnail" alt=""
                                                        onClick={() => { this.handleOnShowDialog(item) }}
                                                        onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }} />
                                                    {checkPermission(
                                                        GALLERY_DELETE,
                                                        userAuth,
                                                        <Popconfirm
                                                            placement="top"
                                                            title="Are you sure to delete this item?"
                                                            onConfirm={() => this.handleDelete(item.id, offset, limit)}
                                                            okText="Yes"
                                                            cancelText="No"
                                                            okType="danger"
                                                            icon={<Icon type="question-circle" />} >
                                                            <button type="button" className="btn btn-danger center-block" title="delete"><i className="fal fa-trash-alt" /></button>
                                                        </Popconfirm>
                                                    )}
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-group">
                                                        <label className="col-sm-3 control-label">Link</label>
                                                        <input className="form-control" value={item.path} readOnly />
                                                    </div>
                                                    <GalleryEdit
                                                        item={item}
                                                        userAuth={userAuth}
                                                        onRefresh={(isNeedRefresh) => { this.handleRefresh(isNeedRefresh) }} />
                                                </div>
                                            </div>
                                        </Modal>
                                    ) : null}

                                    <Overlay />
                                    <div className="row">
                                        {this.renderList(galleries)}
                                    </div>
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={GALLERY_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        galleries: state.gallery.list,
        totalRows: state.gallery.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchGalleriesRequest: (offset, limit, params) => { dispatch(fetchGalleriesRequest(offset, limit, params)) },
        onDeleteGalleryRequest: (id, offset, limit) => { dispatch(deleteGalleryRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(GALLERY_INDEX)(GalleryList));