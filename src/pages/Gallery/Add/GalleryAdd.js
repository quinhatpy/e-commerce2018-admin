import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createGalleryRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns/FormTwoColumns';
import { GALLERY_LIST } from '../../../constants/link';
import { GALLERY_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class GalleryAdd extends Component {
    state = {
        pageTitle: 'Add Gallery',
        breadcrumbItems: [
            {
                name: 'Gallery management',
                link: GALLERY_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            file: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'file',
                    data: ''
                },
                label: 'File',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
            },
            title: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'title',
                    placeholder: 'Enter title'
                },
                label: 'Title',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            alt: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'alt',
                    placeholder: 'Enter alt'
                },
                label: 'Alternate ',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: false,

    }

    componentDidMount() {
        document.title = this.state.pageTitle;
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                updatedFormElement.valid = true;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;

        if (event.target.files && event.target.files[0] === undefined) {
            updatedFormElement.elementConfig.data = '';
            updatedFormElement.errorMessage = 'Please select image!';
            updatedFormElement.valid = false;
        }

        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateGalleryRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'file') {
                stateClone.formElements[key].elementConfig.data = '';
                stateClone.formElements[key].value = '';
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['file'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateGalleryRequest: data => { dispatch(createGalleryRequest(data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(GALLERY_CREATE)(GalleryAdd));