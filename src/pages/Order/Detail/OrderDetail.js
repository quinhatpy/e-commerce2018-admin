import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchOrderRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { ORDER_LIST } from '../../../constants/link';
import { ORDER_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import { formatMoney } from '../../../utils/helper';

class OrderDetail extends Component {
    state = {
        pageTitle: 'Order Detail',
        breadcrumbItems: [
            {
                name: 'Order management',
                link: ORDER_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchOrderRequest
        } = this.props;

        fetchOrderRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { order } = this.props;
        const status = ['Cancel order', 'New order', 'Confirmed', 'Being delivered', 'Delivered'];

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Code</th>
                                                        <td>{order.code}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Receiver Name</th>
                                                        <td>{order.receiver_name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Receiver Phone</th>
                                                        <td>{order.receiver_phone}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Receiver Address</th>
                                                        <td>{order.receiver_address}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Note</th>
                                                        <td>{order.note}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{status[order.status]}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Payment Status</th>
                                                        <td>{order.is_paid === 1 ? 'Paid' : 'Unpaid'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Customer</th>
                                                        <td>{order.customer ? order.customer.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Order Creator</th>
                                                        <td>{order.user ? order.user.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Product</th>
                                                        <td>
                                                            <table className="table table-hover table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Thumbnail</th>
                                                                        <th>Price</th>
                                                                        <th>Quantity</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                    {
                                                                        order.order_details ?
                                                                        order.order_details.map(item => (
                                                                                <tr key={item.product_id}>
                                                                                    <td>{item.product.name}</td>
                                                                                    <td>{item.product.thumbnail ? <img src={item.product.thumbnail} alt="avatar" width="100" onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }} /> : <img src={NoImage} alt="avatar" width="100" />}</td>
                                                                                    <td>{item.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}</td>
                                                                                    <td>{item.quantity}</td>
                                                                                    <td>{(item.price * item.quantity).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.")}</td>
                                                                                </tr>
                                                                            )) : null
                                                                    }
                                                                    <tr>
                                                                        <th colSpan="5" className="text-right">
                                                                        Total:
                                                                        {  order.order_details ?
                                                                            formatMoney( order.order_details.reduce((total, detail) => {
                                                                                return total += detail.quantity * detail.price
                                                                            }, 0)) : null
                                                                        }
                                                                        </th>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(order.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(order.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        order: state.order.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchOrderRequest: id => { dispatch(fetchOrderRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ORDER_SHOW)(OrderDetail));