import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { Popconfirm, Icon } from 'antd';
import { ORDER_LIST, ORDER_EDIT, ORDER_DETAIL } from '../../../constants/link';
import { ORDER_INDEX, ORDER_SHOW, ORDER_UPDATE, ORDER_DELETE } from '../../../constants/permission';
import { fetchOrdersRequest, deleteOrderRequest } from '../../../actions/actions';
import scrollTop from '../../../utils/scrollTop';
import { formatMoney } from '../../../utils/helper';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';
import SearchBox from '../../../components/SearchBox/SearchBox';

class OrderList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        params: {},
        pageTitle: 'List Order',
        breadcrumbItems: [
            {
                name: 'Order management',
                link: ORDER_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'code',
                label: 'Code',
                sortable: true
            },
            {
                name: 'receive_name',
                label: 'Receive Name',
                sortable: true
            },
            {
                name: 'status',
                label: 'Status',
                sortable: true
            },
            {
                name: 'is_paid',
                label: 'Is Paid',
                sortable: true
            },
            {
                name: 'customer_id',
                label: 'Customer',
                sortable: true
            },
            {
                name: 'Total',
                label: 'Total',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }
    }

    handlePageChange(pageNumber, queryParams = '') {

        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.setState(stateClone);
        this.props.onFetchOrdersRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: ORDER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: ORDER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteOrderRequest } = this.props;

        const statusLabel = [
            {
                class: 'default',
                label: 'Cancel'
            },
            {
                class: 'primary',
                label: 'New'
            },
            {
                class: 'info',
                label: 'Confirmed'
            },
            {
                class: 'info',
                label: 'Being delivered'
            },
            {
                class: 'success',
                label: 'Delivered'
            }
        ]

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.code}</td>
                    <td>{item.receiver_name}</td>
                    <td align="center"><span className={'label label-' + statusLabel[item.status].class}>{statusLabel[item.status].label}</span></td>
                    <td align="center">{item.is_paid === 1 ? <span className="label label-success">Paid</span> : <span className="label label-default">Unpaid</span>}</td>
                    <td>{item.customer ? item.customer.name : null}</td>
                    <td>
                        {
                            formatMoney(item.order_details.reduce((total, detail) => {
                                return total += detail.quantity * detail.price
                            }, 0))
                        }
                        <sup>đ</sup>
                    </td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                ORDER_SHOW,
                                userAuth,
                                <Link to={ORDER_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                ORDER_UPDATE,
                                userAuth,
                                <Link to={ORDER_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                ORDER_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this order?"
                                    onConfirm={() => onDeleteOrderRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            orders
        } = this.props;


        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                    </h3>
                                    <div className="box-tools">
                                        <SearchBox
                                            placeholder="Search by code"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(orders)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={ORDER_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        orders: state.order.list,
        totalRows: state.order.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchOrdersRequest: (offset, limit, params) => { dispatch(fetchOrdersRequest(offset, limit, params)) },
        onDeleteOrderRequest: (id, offset, limit) => { dispatch(deleteOrderRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ORDER_INDEX)(OrderList));