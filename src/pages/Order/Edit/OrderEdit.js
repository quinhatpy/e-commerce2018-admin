import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchOrderRequest, searchCustomersRequest, searchProductsRequest, updateOrderRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumnsWithBottom from '../../../components/Form/TwoColumnsWithBottom/FormTwoColumnsWithBottom';
import { ORDER_LIST } from '../../../constants/link';
import { ORDER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';


class OrderEdit extends Component {
    state = {
        pageTitle: 'Edit Order',
        breadcrumbItems: [
            {
                name: 'Order management',
                link: ORDER_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            code: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'code',
                    placeholder: 'Enter code',
                    disabled: true
                },
                label: 'Code',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            receiverName: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'receiver_name',
                    placeholder: 'Enter receiver name'
                },
                label: 'Receiver Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            receiverPhone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'receiver_phone',
                    placeholder: 'Enter receiver phone'
                },
                label: 'Receiver Phone',
                value: '',
                validation: {
                    isMobilePhone: true
                },
                valid: true,
                touched: false,
            },
            receiverAddress: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'receiver_address',
                    placeholder: 'Enter receiver address'
                },
                label: 'Receiver Address',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            note: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'note',
                    placeholder: 'Enter note'
                },
                label: 'Note',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            status: {
                elementType: 'select',
                elementConfig: {
                    name: 'status',
                    label: 'Status',
                    options: [
                        {
                            value: 1,
                            displayValue: 'New order'
                        },
                        {
                            value: 0,
                            displayValue: 'Cancel order'
                        },
                        {
                            value: 2,
                            displayValue: 'Confirmed'
                        },
                        {
                            value: 3,
                            displayValue: 'Being delivered'
                        },
                        {
                            value: 4,
                            displayValue: 'Delivered'
                        }
                    ]
                },
                label: 'Status',
                value: 1,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            isPaid: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_paid',
                    label: 'Paid',
                },
                label: 'Is Paid',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            customer: {
                elementType: 'selectAsync',
                elementConfig: {
                    name: 'customer_id',
                    onSearch: (value) => this.handleSearchCustomer(value),
                    data: [],
                    selected: { key: "0", label: "Not the customer" }
                },
                label: 'Customer',
                value: 0,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            product: {
                elementType: 'selectAsync',
                elementConfig: {
                    onSearch: (value) => this.handleSearchProduct(value),
                    data: [],
                    selected: { key: "0", label: "Select product by code to add carts" }
                },
                label: 'Product',
                value: 0,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            cart: {
                elementType: 'cartTable',
                elementConfig: {
                    onRemove: (id) => this.handleRemoveCartItem(id),
                    onChangeQuantity: (id, event) => this.handleOnChangeQuantity(id, event),
                    data: [],
                },
                value: 0,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: true,
    }

    componentDidMount() {
        const orderId = this.props.match.params.id;
        this.props.onFetchOrderRequest(orderId);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { item, customers, products } = nextProps;

        if (item) {
            stateClone.formElements.code.value = item.code;
            stateClone.formElements.receiverName.value = item.receiver_name;
            stateClone.formElements.receiverPhone.value = item.receiver_phone;
            stateClone.formElements.receiverAddress.value = item.receiver_address;
            stateClone.formElements.note.value = item.note;
            stateClone.formElements.status.value = item.status;
            stateClone.formElements.isPaid.value = item.is_paid === 1 ? true : false;
            stateClone.formElements.customer.elementConfig.selected = item.customer ? { key: item.customer.id, label: item.customer.name } : { key: "0", label: "Not the customer" };

            if (item.order_details && stateClone.formElements.cart.elementConfig.data.length === 0) {
                stateClone.formElements.cart.elementConfig.data = [];
                item.order_details.forEach(item => {
                    stateClone.formElements.cart.elementConfig.data.push({
                        id: item.product.id,
                        name: item.product.name,
                        thumbnail: item.product.thumbnail,
                        price: item.price,
                        quantity: item.quantity
                    });
                })
            }
        }


        let  dataProduct = [];

        let dataCustomer = [
            {
                value: 0,
                displayValue: 'Not the customer'
            }
        ];
        for (let customer of customers) {
            dataCustomer.push({
                value: customer.id,
                displayValue: customer.name
            });
        }

        for (let product of products) {
            dataProduct.push({
                value: product.id,
                displayValue: product.name
            });
        }

        stateClone.formElements.customer.elementConfig.data = dataCustomer;
        stateClone.formElements.product.elementConfig.data = dataProduct;

        this.setState(stateClone);
    }

    findProduct(id) {
        let product = this.props.products.find(product => {
            return product.id === parseInt(id);
        });

        return product ? product : null
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        if (inputIdentifier === 'customer') {
            updatedFormElement.elementConfig.selected = event
        } else if (inputIdentifier === 'product') {
            updatedFormElement.elementConfig.selected = { key: "0", label: "Select product by code to add carts" };

            let product = this.findProduct(event.key);

            if (product) {
                let indexCartItem = this.findIndexCartItem(product.id);
                updatedForm.cart.elementConfig.data.push({
                    id: product.id,
                    name: product.name,
                    thumbnail: product.thumbnail,
                    price: product.price,
                    quantity: updatedForm.cart.elementConfig.data[indexCartItem] ? parseInt(updatedForm.cart.elementConfig.data[indexCartItem].quantity) + 1 : 1
                })
            }
        } else {
            updatedFormElement.value = event.target.value;
            if (inputIdentifier === 'isPaid') {
                updatedFormElement.value = event.target.checked;
            }
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleSearchCustomer = value => {
        this.props.onSearchCustomersRequest({ keyword: value })
    }

    handleSearchProduct = value => {
        this.props.onSearchProductsRequest({ keyword: value })
    }

    handleOnChangeQuantity = (id, event) => {
        const stateClone = { ...this.state };
        let indexCartItem = this.findIndexCartItem(id);
        stateClone.formElements.cart.elementConfig.data[indexCartItem].quantity = parseInt(event.target.value) > 1 ? parseInt(event.target.value) : 1;
        this.setState(stateClone);
    }

    findIndexCartItem = id => {
        return this.state.formElements.cart.elementConfig.data.findIndex(item => {
            return item.id === parseInt(id);
        })
    }

    handleRemoveCartItem = id => {
        const stateClone = { ...this.state };
        let idItem = this.findIndexCartItem(id);
        stateClone.formElements.cart.elementConfig.data.splice(idItem, 1);
        if (stateClone.formElements.cart.elementConfig.data.length === 0)
        stateClone.formElements.cart.valid = false;
        let formIsValid = true;
        for (let inputIdentifier in   stateClone.formElements) {
            formIsValid =   stateClone.formElements[inputIdentifier].valid && formIsValid;
        }
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);
    }


    handleSubmit = (event) => {
        event.preventDefault();
        const formElements = this.state.formElements;
        let formData = new FormData(event.target);
        formData.set('is_paid', formData.get('is_paid') === 'on' ? 1 : 0);
        if (parseInt(formElements.customer.elementConfig.selected.key) !== 0) {
            formData.set('customer_id', formElements.customer.elementConfig.selected.key);
        }
        formElements.cart.elementConfig.data.forEach(item => {
            formData.set(`carts[${item.id}]`, item.quantity);
        })

        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateOrderRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isPaid')
                stateClone.formElements[key].value = false;
            else if (key === 'status')
                stateClone.formElements[key].value = 1;

            else if (_.indexOf(['customer', 'product', 'cart'], key) !== -1) {
                stateClone.formElements[key].elementConfig.data = [];
                stateClone.formElements[key].value = 0;
            }
            else stateClone.formElements[key].value = '';

            if (key === 'customer')
                stateClone.formElements[key].elementConfig.selected = { key: "0", label: "Not the customer" };

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        const orderId = this.props.match.params.id;
        this.props.onFetchOrderRequest(orderId);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [], formElementArrayTop = [];
console.log(this.state.formElements.cart.elementConfig.data);
        for (let key in formElements) {
            if (_.indexOf(['status', 'isPaid', 'customer'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else if (_.indexOf(['product', 'cart'], key) !== -1) {
                formElementArrayTop.push({
                    id: key,
                    config: formElements[key]
                });
            }
            else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsWithBottom
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formElementArrayTop={formElementArrayTop}
                                formIsValid={formIsValid}
                                alert={alert}
                                colTop={10}
                                colOffset="col-md-offset-1"
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.order.item,
        customers: state.customer.list,
        products: state.product.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateOrderRequest: (id, data) => { dispatch(updateOrderRequest(id, data)) },
        onFetchOrderRequest: id => { dispatch(fetchOrderRequest(id)) },
        onSearchCustomersRequest: params => { dispatch(searchCustomersRequest(params)) },
        onSearchProductsRequest: params => { dispatch(searchProductsRequest(params)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ORDER_UPDATE)(OrderEdit));