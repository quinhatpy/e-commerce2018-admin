import { Icon, Popconfirm, TreeSelect } from 'antd';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteProductRequest, fetchCategoriesRequest, fetchProductsRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';
import SearchBox from '../../../components/SearchBox/SearchBox';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { PRODUCT_DETAIL, PRODUCT_EDIT, PRODUCT_LIST } from "../../../constants/link";
import { PRODUCT_DELETE, PRODUCT_INDEX, PRODUCT_SHOW, PRODUCT_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';

class ProductList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        params: {},
        categorySelect: '',
        pageTitle: 'List Product',
        breadcrumbItems: [
            {
                name: 'Product management',
                link: PRODUCT_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'code',
                label: 'Code',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'thumbnail',
                label: 'Thumbnail',
                sortable: false
            },
            {
                name: 'category_id',
                label: 'Category',
                sortable: true
            },
            {
                name: 'brand_id',
                label: 'Brand',
                sortable: true
            },
            {
                name: 'user_id',
                label: 'User',
                sortable: true
            },
            {
                name: 'is_show',
                label: 'Status',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchCategoriesRequest();
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
            this.setState({ categorySelect: '' })
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let category = nextProps.categories.find(category => {
                return category.id === parseInt(queryParam.category_id);
            });

            this.setState({ categorySelect: category ? category.name : '' })
        }
    }


    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);
        stateClone.keyword = stateClone.params.keyword ? stateClone.params.keyword : '';

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;
        this.setState(stateClone);

        this.props.onFetchProductsRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();

    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: PRODUCT_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: PRODUCT_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSelectCategory = (value, node, extra) => {
        let params = { ...this.state.params };
        params.category_id = node.props.eventKey;
        // let stateClone = { ...this.state };

        // this.setState(stateClone);

        // let params = { ...this.state.params };
        // params.category_id = node.props.eventKey;
        let queryParam = queryString.stringify(params);

        this.props.history.push({
            pathname: PRODUCT_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnChangeTreeSelect = (value, label, extra) => {
        if (value === undefined) {
            let params = { ...this.state.params };
            delete params.category_id;
            let queryParam = queryString.stringify(params);

            this.props.history.push({
                pathname: PRODUCT_LIST,
                search: '?' + queryParam
            });
            this.handlePageChange(1, queryParam);
        }
    }

    renderSelectTree(categories, parentId = null) {
        let out = [];
        const TreeNode = TreeSelect.TreeNode;
        for (let i in categories) {
            let itemParentId = categories[i].parent ? categories[i].parent.id : null;
            if (itemParentId === parentId) {
                out.push(
                    <TreeNode value={categories[i].name} title={categories[i].name} key={categories[i].id}>
                        {this.renderSelectTree(categories, categories[i].id)}
                    </TreeNode>
                )
            }
        }
        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteProductRequest } = this.props;
        return data.length === 0 ? <tr><td colSpan="7" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.code}</td>
                    <td>{item.name}</td>
                    <td>{item.thumbnail ? <img src={item.thumbnail} alt="" width="80" onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }} /> : <img src={NoImage} alt="avatar" width="50" />}</td>
                    <td>{item.category ? item.category.name : null}</td>
                    <td>{item.brand ? item.brand.name : null}</td>
                    <td>{item.user ? item.user.name : null}</td>
                    <td align="center">{item.is_show === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td width="150" align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                PRODUCT_SHOW,
                                userAuth,
                                <Link to={PRODUCT_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                PRODUCT_UPDATE,
                                userAuth,
                                <Link to={PRODUCT_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                PRODUCT_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this product?"
                                    onConfirm={() => onDeleteProductRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy,
            categorySelect
        } = this.state;
        const {
            totalRows,
            categories,
            products,
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                        <b>Category: </b><TreeSelect
                                            showSearch
                                            style={{ width: 300 }}
                                            value={categorySelect}
                                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                            placeholder="Please select category"
                                            allowClear
                                            onSelect={this.handleOnSelectCategory}
                                            onChange={this.handleOnChangeTreeSelect}>
                                            <TreeSelect.TreeNode value=" " title="Show all" key={' '} />
                                            {this.renderSelectTree(categories)}
                                        </TreeSelect>
                                    </h3>
                                    <div className="box-tools">

                                        <SearchBox
                                            placeholder="Search by name"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>
                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(products)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={PRODUCT_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        products: state.product.list,
        categories: state.category.list,
        totalRows: state.product.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchProductsRequest: (offset, limit, params) => { dispatch(fetchProductsRequest(offset, limit, params)) },
        onDeleteProductRequest: (id, offset, limit) => { dispatch(deleteProductRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(PRODUCT_INDEX)(ProductList));