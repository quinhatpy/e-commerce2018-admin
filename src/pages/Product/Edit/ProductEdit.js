import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import _ from 'lodash/array';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { deleteProductImageRequest, fetchAttributesRequest, fetchBranchesRequest, fetchBrandsRequest, fetchCategoriesRequest, fetchProductRequest, resetAttributes, resetBranches, resetBrands, resetCategories, resetProduct, updateProductRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree/FormTwoColumnsHasCheckBoxTree';
import { PRODUCT_LIST } from '../../../constants/link';
import { PRODUCT_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren, getParents, getSelectValues, toSlug } from '../../../utils/helper';

class ProductEdit extends Component {
    state = {
        pageTitle: 'Edit Product',
        breadcrumbItems: [
            {
                name: 'Product management',
                link: PRODUCT_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            code: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'code',
                    placeholder: 'Enter code'
                },
                label: 'Code',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            slug: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'slug',
                    placeholder: 'Enter slug may not be greater than 150 characters'
                },
                label: 'Slug',
                value: '',
                validation: {
                    required: true,
                    maxLength: 150,
                    unique: {
                        key: 'slug',
                        data: [],
                        exclude: 0
                    }
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            price: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'price',
                    placeholder: 'Enter price'
                },
                label: 'Price',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            description: {
                elementType: 'richText',
                elementConfig: {
                    name: 'description',
                    placeholder: 'Enter description',
                    contentStyle:
                    {
                        height: 210,
                        boxShadow: 'inset 0 1px 3px rgba(0,0,0,.1)'
                    }
                },
                label: 'Description',
                value: BraftEditor.createEditorState(null),
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            content: {
                elementType: 'richText',
                elementConfig: {
                    name: 'content',
                    placeholder: 'Enter content'
                },
                label: 'Content',
                value: BraftEditor.createEditorState(null),
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            category: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'category_id',
                    data: [],
                    checked: [''],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            brand: {
                elementType: 'select',
                elementConfig: {
                    name: 'brand_id',
                    options: []
                },
                label: 'Brand',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            newPrice: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'new_price',
                    placeholder: 'Enter new price'
                },
                label: 'Promotion price',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            promotionPeriod: {
                elementType: 'dateTimeRange',
                elementConfig: {
                    defaultValue: []
                },
                label: 'Promotion period',
                value: [
                    moment(),
                    moment()
                ],
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            thumbnail: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'thumbnail',
                    data: ''
                },
                label: 'Thumbnail',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            images: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'images[]',
                    multiple: true,
                    data: ''
                },
                label: 'Images',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            currentImages: {
                elementType: 'imageList',
                elementConfig: {
                    data: []
                },
                label: 'Current Images',
                value: '',
                validation: {},
                valid: true,
                touched: true,
            },
            seoTitle: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'seo_title',
                    placeholder: 'Enter SEO title may not be greater than 60 characters'
                },
                label: 'SEO Title',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'seo_description',
                    placeholder: 'Enter SEO description'
                },
                label: 'SEO Description',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            branches: {
                elementType: 'select',
                elementConfig: {
                    multiple: true,
                    options: []
                },
                label: 'Branch',
                value: [],
                validation: {},
                valid: true,
                touched: false,
            },
            quantities: {
                elementType: 'multiInput',
                elementConfig: {
                    children: [],
                    placeholder: "Please select a branch to display it's quantity"
                },
                label: 'Quantity',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            attributes: {
                elementType: 'multiInput',
                elementConfig: {
                    children: [],
                    placeholder: "Please select a category to display it's attributes"
                },
                label: 'Attribute',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const productId = this.props.match.params.id;
        this.props.onFetchProductRequest(productId);
        this.props.onFetchCategoriesRequest();
        this.props.onFetchBrandsRequest();
        this.props.onFetchBranchesRequest();

        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { item, categories, brands, branches, attributes } = nextProps;
        let dataNested = getNestedChildren(categories, null, parseInt(this.props.match.params.id));
        let dataBrand = [], dataBranch = [];
        for (let brand of brands) {
            dataBrand.push({
                value: brand.id,
                displayValue: brand.name
            });
        }

        for (let branch of branches) {
            dataBranch.push({
                value: branch.id,
                displayValue: branch.name
            });
        }

        if (item) {
            stateClone.formElements.code.value = item.code;
            stateClone.formElements.name.value = item.name;
            stateClone.formElements.slug.value = item.slug;
            stateClone.formElements.price.value = item.price;
            stateClone.formElements.thumbnail.elementConfig.data = item.thumbnail;
            stateClone.formElements.description.value = BraftEditor.createEditorState(item.description);
            stateClone.formElements.content.value = BraftEditor.createEditorState(item.content);
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.seoTitle.value = item.seo_title;
            stateClone.formElements.seoDescription.value = item.seo_description;
            stateClone.formElements.brand.value = item.brand ? item.brand.id : '';
            stateClone.formElements.category.elementConfig.data = dataNested;
            stateClone.formElements.brand.elementConfig.options = dataBrand;
            stateClone.formElements.branches.elementConfig.options = dataBranch;

            if (item.promotion) {
                stateClone.formElements.newPrice.value = item.promotion ? item.promotion.new_price : '';
                stateClone.formElements.promotionPeriod.value = [
                    moment(item.promotion.start),
                    moment(item.promotion.expire)
                ]
            }
            if (item.category) {
                let dataExpanded = getParents(categories, item.category.id);

                if (this.props.item !== item) {
                    stateClone.formElements.category.elementConfig.checked = [item.category.id];
                    stateClone.formElements.category.elementConfig.expanded = dataExpanded;
                }

                if (isEmpty(this.props.attributes) && item.attributes.length !== 0)
                    this.props.onFetchAttributesRequest(0, Math.pow(2, 31) - 1, { category_id: item.category.id })
            }

            if (item.quantities) {
                stateClone.formElements.quantities.elementConfig.children = [];
                item.quantities.forEach(quantity => {
                    stateClone.formElements.branches.value.push(quantity.branch_id);
                    stateClone.formElements.quantities.elementConfig.children[quantity.branch_id] = {
                        type: 'number',
                        name: `quantities[${quantity.branch_id}]`,
                        label: 'Quantity at ' + this.getNameBranchById(branches, quantity.branch_id),
                        value: quantity.quantity,
                        required: true
                    }
                });
            }

            if (item.images) {
                stateClone.formElements.currentImages.elementConfig.data = [];
                item.images.forEach(image => {
                    stateClone.formElements.currentImages.elementConfig.data[image.id] = {
                        id: image.id,
                        data: image.path
                    }
                })
            }
        }

        if (attributes) {
            stateClone.formElements.attributes.elementConfig.children = [];
            attributes.forEach(attribute => {
                let attributeItem = this.getAttribute(item.attributes, attribute.id);
                stateClone.formElements.attributes.elementConfig.children[attribute.id] = {
                    type: attribute.data_type === 'string' ? 'text' : 'number',
                    name: `attributes[${attribute.id}]`,
                    label: attribute.name,
                    value: attributeItem.content,
                    required: attribute.is_required === 1 ? true : false

                }
            });
        }
        this.setState(stateClone);
    }

    componentWillUnmount() {
        this.props.onResetCategories()
        this.props.onResetProduct()
        this.props.onResetBrands()
        this.props.onResetBranches()
        this.props.onResetAttributes()
    }

    getAttribute(attributes, attributeId) {
        let attribute = attributes.find(attribute => {
            return attribute.attribute_id === parseInt(attributeId);
        });

        return attribute ? attribute : {};
    }

    getQuantity(quantities, productId, branchId) {
        let quantity = quantities.find(quantity => {
            return quantity.product_id === parseInt(productId) && quantity.branch_id === parseInt(branchId);
        });

        return quantity ? quantity.quantity : 0;
    }

    getNameBranchById(branches, id) {
        let branch = branches.find(branch => {
            return branch.id === parseInt(id)
        });

        return branch ? branch.name : '';
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };
        if (inputIdentifier === 'currentImages') {
            this.props.onDeleteProductImageRequest(event);
            updatedForm.currentImages.elementConfig.data.splice(event, 1);

        } else if (_.indexOf(['content', 'description', 'promotionPeriod'], inputIdentifier) !== -1) {
            updatedFormElement.value = event;
        } else if (inputIdentifier === 'branches') {
            updatedFormElement.value = getSelectValues(event.target);
            updatedForm.quantities.elementConfig.children = [];
            updatedFormElement.value.forEach(branch => {
                updatedForm.quantities.elementConfig.children[branch] = {
                    type: 'number',
                    name: `quantities[${branch}]`,
                    label: 'Quantity at ' + this.getNameBranchById(this.props.branches, branch),
                    value: this.getQuantity(this.props.item.quantities, this.props.match.params.id, branch),
                    required: true
                }
            });
        } else {
            updatedFormElement.value = event.target.value;

            if (inputIdentifier === 'isShow') {
                updatedFormElement.value = event.target.checked;
            }
            // to slug
            if (inputIdentifier === 'name') {
                let slug = toSlug(event.target.value)
                updatedForm.slug.value = slug;
            }
            // get image preview
            if (inputIdentifier === 'thumbnail' && event.target.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {

                    updatedFormElement.elementConfig.data = e.target.result;
                    const stateClone = { ...this.state };
                    updatedForm[inputIdentifier] = updatedFormElement;
                    stateClone.formElements = updatedForm;
                    this.setState(stateClone);

                }
                reader.readAsDataURL(event.target.files[0]);
            }

            if (inputIdentifier === 'images') {
                updatedFormElement.elementConfig.data = [];

                for (let file of event.target.files) {
                    let reader = new FileReader();
                    reader.onload = (e) => {
                        updatedFormElement.elementConfig.data.push({
                            data: e.target.result
                        });

                        const stateClone = { ...this.state };
                        updatedForm[inputIdentifier] = updatedFormElement;
                        stateClone.formElements = updatedForm;
                        this.setState(stateClone);
                    }
                    reader.readAsDataURL(file);
                }

            }

            // validate
            let checked = checkValidity(updatedForm, inputIdentifier);

            updatedFormElement.valid = checked.isValid;
            updatedFormElement.errorMessage = checked.errorMessage;
        }

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        let dataExpanded = getParents(this.props.categories, stateClone.formElements[inputIdentifier].elementConfig.checked[0]);
        stateClone.formElements.category.elementConfig.expanded = dataExpanded;
        this.setState(stateClone);
        this.props.onFetchAttributesRequest(0, Math.pow(2, 31) - 1, { category_id: checked.value })
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { formElements } = this.state;
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('promotion_start', formElements.promotionPeriod.value[0].format("YYYY-MM-DD HH:mm:ss"));
        formData.set('promotion_expire', formElements.promotionPeriod.value[1].format("YYYY-MM-DD HH:mm:ss"));
        formData.set('content', this.state.formElements.content.value.toHTML())
        formData.set('description', this.state.formElements.description.value.toHTML())

        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateProductRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (key === 'category') {
                stateClone.formElements[key].elementConfig.checked = [];
                stateClone.formElements[key].elementConfig.expanded = [];
            }
            else if (_.indexOf(['quantities', 'attributes'], key) !== -1)
                stateClone.formElements[key].elementConfig.children = [];
            else if (key === 'promotionPeriod')
                stateClone.formElements[key].value = [moment(), moment()];
            else if (_.indexOf(['thumbnail', 'images'], key) !== -1) {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if (_.indexOf(['content', 'description'], key) !== -1)
                stateClone.formElements[key].value = BraftEditor.createEditorState(null)
            else if (key === 'branches')
                stateClone.formElements[key].value = [];
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        const productId = this.props.match.params.id;
        this.props.onFetchProductRequest(productId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['category', 'thumbnail', 'brand', 'isShow', 'images', 'currentImages'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.product.item,
        categories: state.category.list,
        brands: state.brand.list,
        attributes: state.attribute.list,
        branches: state.branch.list,
        alert: state.alert,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateProductRequest: (id, data) => { dispatch(updateProductRequest(id, data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchProductRequest: id => { dispatch(fetchProductRequest(id)) },
        onFetchBrandsRequest: () => { dispatch(fetchBrandsRequest()) },
        onFetchBranchesRequest: () => { dispatch(fetchBranchesRequest()) },
        onFetchAttributesRequest: (offset, limit, params) => { dispatch(fetchAttributesRequest(offset, limit, params)) },
        onDeleteProductImageRequest: id => { dispatch(deleteProductImageRequest(id)) },
        onResetCategories: () => { dispatch(resetCategories()) },
        onResetProduct: () => { dispatch(resetProduct()) },
        onResetBrands: () => { dispatch(resetBrands()) },
        onResetBranches: () => { dispatch(resetBranches()) },
        onResetAttributes: () => { dispatch(resetAttributes()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(PRODUCT_UPDATE)(ProductEdit));