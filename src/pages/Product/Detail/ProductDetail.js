import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchAttributesRequest, fetchBranchesRequest, fetchProductRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { PRODUCT_LIST } from '../../../constants/link';
import { PRODUCT_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class ProductDetail extends Component {
    state = {
        pageTitle: 'Product Detail',
        breadcrumbItems: [
            {
                name: 'Product management',
                link: PRODUCT_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchProductRequest,
            fetchBranchesRequest
        } = this.props;

        fetchProductRequest(match.params.id);
        fetchBranchesRequest();
    }

    componentWillReceiveProps(nextProps) {
        if (!isEmpty(nextProps.product) && isEmpty(this.props.attributes) && nextProps.product.attributes.length !== 0)
            this.props.fetchAttributesRequest(0, Math.pow(2, 31) - 1, { category_id: nextProps.product.category.id })
    }

    getNameBranchById(branches, id) {
        let branch = branches.find(branch => {
            return branch.id === parseInt(id)
        });

        return branch ? branch.name : '';
    }

    getAttribute(attributes, attributeId) {
        let attribute = attributes.find(attribute => {
            return attribute.id === parseInt(attributeId);
        });

        return attribute ? attribute : {};
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const {
            product,
            branches,
            attributes
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{product.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Code</th>
                                                        <td>{product.code}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Slug</th>
                                                        <td>{product.slug}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Price</th>
                                                        <td>{product.price ? product.price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thumbnail</th>
                                                        <td>{product.thumbnail ? <img src={product.thumbnail} alt="avatar" width="100" onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }} /> : <img src={NoImage} alt="avatar" width="100" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Description</th>
                                                        <td dangerouslySetInnerHTML={{ __html: product.description }}></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Content</th>
                                                        <td dangerouslySetInnerHTML={{ __html: product.content }}></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{product.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>View</th>
                                                        <td>{product.view}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sold</th>
                                                        <td>{product.sold}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Title</th>
                                                        <td>{product.seo_title}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Description</th>
                                                        <td>{product.seo_description}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Rating</th>
                                                        <td>{product.rating}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>User Posted</th>
                                                        <td>{product.user ? product.user.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Category</th>
                                                        <td>{product.category ? product.category.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Brand</th>
                                                        <td>{product.brand ? product.brand.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Images</th>
                                                        <td>
                                                            {
                                                                product.images ?
                                                                    product.images.map(image => (
                                                                        <img key={image.id} src={image.path} alt="avatar" width="100" onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }} />
                                                                    )) : null
                                                            }
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Promotion</th>
                                                        <td>{product.promotion ? (
                                                            <Fragment>
                                                                <p>New price: {product.promotion.new_price}</p>
                                                                <p>Status: {product.promotion.is_show === 1 ? 'Show' : 'Hide'}</p>
                                                                <p>Start: {moment(product.promotion.start, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</p>
                                                                <p>Expire:{moment(product.promotion.expire, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</p>
                                                            </Fragment>
                                                        ) : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Attributes</th>
                                                        <td>
                                                            {
                                                                product.attributes ? (
                                                                    product.attributes.map(item => (
                                                                        <p key={item.id}>{this.getAttribute(attributes, item.attribute_id).name}: {item.content}</p>
                                                                    ))
                                                                ) : null
                                                            }
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Quantities</th>
                                                        <td>
                                                            {
                                                                product.quantities ? (
                                                                    product.quantities.map(item => (
                                                                        <p key={item.id}>At {this.getNameBranchById(branches, item.branch_id)}: {item.quantity}</p>
                                                                    ))
                                                                ) : null
                                                            }
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(product.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(product.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        product: state.product.item,
        branches: state.branch.list,
        attributes: state.attribute.list
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchProductRequest: id => { dispatch(fetchProductRequest(id)) },
        fetchBranchesRequest: () => { dispatch(fetchBranchesRequest()) },
        fetchAttributesRequest: (offset, limit, params) => { dispatch(fetchAttributesRequest(offset, limit, params)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(PRODUCT_SHOW)(ProductDetail));