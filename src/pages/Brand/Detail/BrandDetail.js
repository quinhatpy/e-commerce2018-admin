import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBrandRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { BRAND_LIST } from '../../../constants/link';
import { BRAND_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class BrandDetail extends Component {
    state = {
        pageTitle: 'Brand Detail',
        breadcrumbItems: [
            {
                name: 'Brand management',
                link: BRAND_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchBrandRequest
        } = this.props;

        fetchBrandRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { brand } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{brand.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Slug</th>
                                                        <td>{brand.slug}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Logo</th>
                                                        <td>{brand.logo ? <img src={brand.logo} alt="avatar" width="200" onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}} /> : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{brand.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Title</th>
                                                        <td>{brand.seo_title}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Description</th>
                                                        <td>{brand.seo_description}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(brand.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(brand.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        brand: state.brand.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchBrandRequest: id => { dispatch(fetchBrandRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BRAND_SHOW)(BrandDetail));