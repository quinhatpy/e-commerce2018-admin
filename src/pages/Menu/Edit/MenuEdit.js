import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash/array';
import { MENU_LIST } from '../../../constants/link';
import checkValidity from '../../../utils/checkValidity';
import { updateMenuRequest, fetchMenusRequest, fetchMenuRequest } from '../../../actions/actions';
import { getNestedChildren, getParents } from '../../../utils/helper';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import { CATEGORY_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree/FormTwoColumnsHasCheckBoxTree';

class MenuEdit extends Component {
    state = {
        pageTitle: 'Edit Menu',
        breadcrumbItems: [
            {
                name: 'Menu management',
                link: MENU_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    required: true,
                    // isURL: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            icon: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'icon',
                    data: ''
                },
                label: 'Icon',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            menuParent: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'parent_id',
                    data: [],
                    checked: [''],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Menu Parent',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',

                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const menuId = this.props.match.params.id;
        this.props.onFetchMenuRequest(menuId);
        this.props.onFetchMenusRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { menus, item } = nextProps;
        let dataNested = getNestedChildren(menus, null, parseInt(this.props.match.params.id));

        let dataDefault = {
            value: '',
            label: 'No parent'
        };

        if (item) {
            let dataExpanded = getParents(menus, item.id);

            stateClone.formElements.name.value = item.name;
            stateClone.formElements.link.value = item.link;
            stateClone.formElements.icon.elementConfig.data = item.icon;
            stateClone.formElements.menuParent.elementConfig.checked = item.parent ? [item.parent.id] : [''];
            stateClone.formElements.menuParent.elementConfig.expanded = dataExpanded;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.ordinal.value = item.ordinal;
        }

        stateClone.formElements.menuParent.elementConfig.data = [...dataNested, dataDefault];

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        if (inputIdentifier === 'isShow') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        if (checked.value !== parseInt(this.props.match.params.id))
            stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateMenuRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow' )
            stateClone.formElements[key].value = false;
           else if(key === 'icon'){
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if(key === 'categoryParent'){ 
                stateClone.formElements[key].elementConfig.checked= [''];
                stateClone.formElements[key].elementConfig.expanded= [];
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;
        this.setState(stateClone);

        const menuId = this.props.match.params.id;
        this.props.onFetchMenuRequest(menuId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['menuParent'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.menu.item,
        menus: state.menu.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateMenuRequest: (id, data) => { dispatch(updateMenuRequest(id, data)) },
        onFetchMenusRequest: () => { dispatch(fetchMenusRequest()) },
        onFetchMenuRequest: id => { dispatch(fetchMenuRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_UPDATE)(MenuEdit));