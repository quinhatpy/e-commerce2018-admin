import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchMenuRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { MENU_LIST } from '../../../constants/link';
import { MENU_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class MenuDetail extends Component {
    state = {
        pageTitle: 'Menu Detail',
        breadcrumbItems: [
            {
                name: 'Menu management',
                link: MENU_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchMenuRequest
        } = this.props;

        fetchMenuRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { menu } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{menu.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Link</th>
                                                        <td>{menu.link ? <a href={'https://e2018-nit.herokuapp.com' + menu.link} target="_blank" rel="noopener noreferrer">{menu.link}</a> : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Icon</th>
                                                        <td>{menu.icon ? <img src={menu.icon} alt="avatar" width="100" onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}}/> : <img src={NoImage} alt="avatar" width="100" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{menu.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ordinal</th>
                                                        <td>{menu.ordinal}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Menu Parent</th>
                                                        <td>{menu.parent ? menu.parent.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(menu.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(menu.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        menu: state.menu.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchMenuRequest: id => { dispatch(fetchMenuRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(MENU_SHOW)(MenuDetail));