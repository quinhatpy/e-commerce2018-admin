import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash/array';
import { MENU_LIST } from '../../../constants/link';
import { MENU_CREATE } from '../../../constants/permission';
import { createMenuRequest, fetchMenusRequest } from '../../../actions';
import { getNestedChildren } from '../../../utils/helper';
import checkValidity from '../../../utils/checkValidity';
import CheckPermission from '../../../hoc/CheckPermission';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';

class MenuAdd extends Component {
    state = {
        pageTitle: 'Add Menu',
        breadcrumbItems: [
            {
                name: 'Menu management',
                link: MENU_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    required: true,
                    // isURL: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            icon: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'icon',
                    data: ''
                },
                label: 'Icon',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            menuParent: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'parent_id',
                    data: [],
                    checked: [''],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Menu Parent',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchMenusRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataNested = getNestedChildren(nextProps.menus, null);
        let dataDefault = {
            value: '',
            label: 'No parent'
        };
        
        stateClone.formElements.menuParent.elementConfig.data = [...dataNested, dataDefault];
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateMenuRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (key === 'icon') {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if (key === 'categoryParent') {
                stateClone.formElements[key].elementConfig.checked = [''];
                stateClone.formElements[key].elementConfig.expanded = [];
            }
            else stateClone.formElements[key].value = '';

            if (_.indexOf(['name', 'slug'], key) !== -1)
                stateClone.formElements[key].valid = false;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['menuParent'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">

                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        menus: state.menu.list,
        alert: state.alert
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onCreateMenuRequest: data => { dispatch(createMenuRequest(data)) },
        onFetchMenusRequest: () => { dispatch(fetchMenusRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(MENU_CREATE)(MenuAdd));