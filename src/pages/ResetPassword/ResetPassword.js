import isEmpty from 'lodash/isEmpty';
import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import LoadingBar from 'react-redux-loading-bar';
import { Link, Redirect } from 'react-router-dom';
import * as linkConfig from '../../constants/link';
import { findResetPasswordRequest, resetPasswordRequest } from './../../actions';
import Button from './../../components/UI/Button';
import LoginForm from './../../components/UI/LoginForm';
import checkValidity from './../../utils/checkValidity';

class ResetPassword extends Component {
    state = {
        pageTitle: 'Reset Password E-commerce 2018',
        resetPasswordForm: {
            email: {
                elementType: 'email',
                elementConfig: {
                    name: 'name',
                    placeholder: 'Your Email',
                    readOnly: true
                },
                label: 'Email',
                value: '',
                validation: {},
                valid: true,
                touched: false,
                errorMessage: ''
            },
            password: {
                elementType: 'password',
                elementConfig: {
                    name: 'password',
                    placeholder: 'New password'
                },
                label: 'Password',
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false,
                errorMessage: ''
            },
            password_confirmation: {
                elementType: 'password',
                elementConfig: {
                    name: 'password_confirmation',
                    placeholder: 'Retype new password'
                },
                label: 'Password',
                value: '',
                validation: {
                    required: true,
                    isEqualWith: 'password'
                },
                valid: false,
                touched: false,
                errorMessage: ''
            },
            token: {
                elementType: 'hidden',
                elementConfig: {
                    name: 'token'
                },
                label: 'Email',
                value: '',
                validation: {},
                valid: true,
                touched: false,
                errorMessage: ''
            },
        },
        formIsValid: false,
        loading: false,
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            findResetPasswordRequest
        } = this.props;

        findResetPasswordRequest(match.params.token);
    }

    componentWillReceiveProps(nextProps) {
        const { infoReset } = nextProps;

        if (!isEmpty(infoReset)) {
            const stateClone = { ...this.state };
            stateClone.resetPasswordForm.email.value = infoReset.email;
            stateClone.resetPasswordForm.token.value = infoReset.token;
        }
    }


    inputChangedHandler = (event, inputIdentifier) => {
        const resetPasswordForm = {
            ...this.state.resetPasswordForm
        };
        const updatedFormElement = {
            ...resetPasswordForm[inputIdentifier]
        };

        updatedFormElement.value =  event.target.value;

        let checked = checkValidity(resetPasswordForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;
        updatedFormElement.touched = true;
        resetPasswordForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in resetPasswordForm) {
            formIsValid = resetPasswordForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({ resetPasswordForm: resetPasswordForm, formIsValid: formIsValid });
    }

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.resetPasswordForm) {
            formData[formElementIdentifier] = this.state.resetPasswordForm[formElementIdentifier].value;
        }
        this.props.resetPasswordRequest(formData);
    }

    redirectToTarget = (to) => {
        to.hash = "login"
        // this.props.history.push(to)
        return <Redirect to={to} />

    }

    render() {
        const { isAuthenticated } = this.props;
        const { from } = this.props.location.state || { from: { pathname: '/' } }

        const formElementsArray = [];
        for (let key in this.state.resetPasswordForm) {
            formElementsArray.push({
                id: key,
                config: this.state.resetPasswordForm[key]
            });
        }

        let form = (
            <form onSubmit={this.submitHandler}>
                {formElementsArray.map(formElement => (
                    <LoginForm
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}
                        errorMessage={formElement.config.errorMessage}
                    />
                ))}

                <Button
                    type="submit"
                    disabled={!this.state.formIsValid}
                    classes="btn btn-primary btn-block btn-flat">
                    Update Password
                </Button>
            </form>
        );

        return (
            <Fragment>
                <LoadingBar style={{ backgroundColor: '#f39c12 ', height: '5px', zIndex: 99999999 }} />
                <div className="login-box ">
                    <div className="login-logo">
                        <Link to={linkConfig.LOGIN}><b>E-Commerce </b> 2018</Link>
                        <h2 className="text-center">Reset Password</h2>
                    </div>
                    {/* /.login-logo */}
                    <div className="login-box-body">
                        {form}
                        {(isAuthenticated === true) ? this.redirectToTarget(from) : null}

                        {/* /.social-auth-links */}
                        <Link to={linkConfig.FORGOT_PASSWORD}>I forgot my password</Link><br />

                    </div>
                    {/* /.login-box-body */}
                </div>
            </Fragment>
        )
    }
}

ResetPassword.propTypes = {
    resetPasswordRequest: PropTypes.func.isRequired
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
    infoReset: state.auth.infoReset,
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        findResetPasswordRequest: token => { dispatch(findResetPasswordRequest(token)) },
        resetPasswordRequest: data => { dispatch(resetPasswordRequest(data)) }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);