import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCategoryBannerRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { CATEGORY_BANNER_LIST } from '../../../constants/link';
import { CATEGORY_BANNER_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class CategoryBannerDetail extends Component {
    state = {
        pageTitle: 'Category Banner Detail',
        breadcrumbItems: [
            {
                name: 'Category banner management',
                link: CATEGORY_BANNER_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchCategoryBannerRequest
        } = this.props;

        fetchCategoryBannerRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { categoryBanner } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Image</th>
                                                        <td>{categoryBanner.image ? <img src={categoryBanner.image} alt="avatar" height="200" /> : <img src={NoImage} alt="avatar"  height="200" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Link</th>
                                                        <td>{categoryBanner.link}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ordinal</th>
                                                        <td>{categoryBanner.ordinal}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{categoryBanner.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Show Home</th>
                                                        <td>{categoryBanner.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Category</th>
                                                        <td>{categoryBanner.category ? categoryBanner.category.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(categoryBanner.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(categoryBanner.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        categoryBanner: state.categoryBanner.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCategoryBannerRequest: id => { dispatch(fetchCategoryBannerRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_BANNER_SHOW)(CategoryBannerDetail));