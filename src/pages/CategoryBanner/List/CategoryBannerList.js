import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { TreeSelect, Popconfirm, Icon } from 'antd';
import { CATEGORY_BANNER_LIST, CATEGORY_BANNER_EDIT, CATEGORY_BANNER_DETAIL } from '../../../constants/link';
import { CATEGORY_BANNER_INDEX, CATEGORY_BANNER_SHOW, CATEGORY_BANNER_UPDATE, CATEGORY_BANNER_DELETE } from '../../../constants/permission';
import { fetchCategoriesRequest, fetchCategoryBannersRequest, deleteCategoryBannerRequest } from '../../../actions/actions';
import scrollTop from '../../../utils/scrollTop';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';

class CategoryBannerList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        categorySelect: '',
        params: {},
        pageTitle: 'List Category Banner',
        breadcrumbItems: [
            {
                name: 'Category banner management',
                link: CATEGORY_BANNER_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'image',
                label: 'Image',
                sortable: false
            },
            {
                name: 'link',
                label: 'Link',
                sortable: false
            },
            {
                name: 'ordinal',
                label: 'Ordinal',
                sortable: true
            },
            {
                name: 'is_show',
                label: 'Status',
                sortable: true
            },
            {
                name: 'is_main',
                label: 'Show Home',
                sortable: true
            },
            {
                name: 'category_id',
                label: 'Category',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchCategoriesRequest();
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let category = nextProps.categories.find(category => {
                return category.id === parseInt(queryParam.category_id);
            });

            this.setState({ categorySelect: category ? category.name : ' ' })
        }
    }

    handlePageChange(pageNumber, queryParams = '') {

        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;
        this.setState(stateClone);
        this.props.onFetchCategoryBannersRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: CATEGORY_BANNER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSelectCategory = (value, node, extra) => {
        let stateClone = { ...this.state };
        stateClone.params.category_id = node.props.eventKey;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: CATEGORY_BANNER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderSelectTree(categories, parentId = null) {
        let out = [];
        const TreeNode = TreeSelect.TreeNode;
        for (let i in categories) {
            let itemParentId = categories[i].parent ? categories[i].parent.id : null;
            if (itemParentId === parentId) {
                out.push(
                    <TreeNode value={categories[i].name} title={categories[i].name} key={categories[i].id}>
                        {this.renderSelectTree(categories, categories[i].id)}
                    </TreeNode>
                )
            }
        }
        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteCategoryBannerRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.image ? <img src={item.image} alt="avatar" width="50" /> : null}</td>
                    <td width="300">{item.link ? <a href={item.link} target="_blank" rel="noopener noreferrer">{item.link}</a> : null}</td>
                    <td>{item.ordinal}</td>
                    <td align="center">{item.is_show === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td align="center">{item.is_main === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td>{item.category ? item.category.name : null}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                CATEGORY_BANNER_SHOW,
                                userAuth,
                                <Link to={CATEGORY_BANNER_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                CATEGORY_BANNER_UPDATE,
                                userAuth,
                                <Link to={CATEGORY_BANNER_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                CATEGORY_BANNER_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this banner?"
                                    onConfirm={() => onDeleteCategoryBannerRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            categorySelect,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            categories,
            categoryBanners
        } = this.props;


        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                    </h3>
                                    <div className="box-tools">
                                        <b>Category: </b><TreeSelect
                                            showSearch
                                            style={{ width: 300 }}
                                            value={categorySelect}
                                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                            placeholder="Please select category"
                                            allowClear
                                            onSelect={this.handleOnSelectCategory}
                                        >
                                            <TreeSelect.TreeNode value=" " title="Show all" key={' '} />
                                            {this.renderSelectTree(categories)}
                                        </TreeSelect>
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(categoryBanners)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={CATEGORY_BANNER_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        categories: state.category.list,
        categoryBanners: state.categoryBanner.list,
        totalRows: state.categoryBanner.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchCategoryBannersRequest: (offset, limit, params) => { dispatch(fetchCategoryBannersRequest(offset, limit, params)) },
        onDeleteCategoryBannerRequest: (id, offset, limit) => { dispatch(deleteCategoryBannerRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_BANNER_INDEX)(CategoryBannerList));