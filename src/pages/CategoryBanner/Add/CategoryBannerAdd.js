import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createCategoryBannerRequest, fetchCategoriesRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';
import { CATEGORY_BANNER_LIST } from '../../../constants/link';
import { CATEGORY_BANNER_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren } from '../../../utils/helper';

class CategoryBannerAdd extends Component {
    state = {
        pageTitle: 'Add Category Banner',
        breadcrumbItems: [
            {
                name: 'Category banner management',
                link: CATEGORY_BANNER_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            image: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'image',
                    data: ''
                },
                label: 'Image',
                value: '',
                validation: {},
                valid: false,
                touched: false,
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    // isURL: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            category: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'category_id',
                    data: [],
                    checked: [],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            isMain: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_main',
                    label: 'Show',
                },
                label: 'Show home',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchCategoriesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataNested = getNestedChildren(nextProps.categories, null);

        stateClone.formElements.category.elementConfig.data = dataNested;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // to slug
        if (inputIdentifier === 'isShow' || inputIdentifier === 'isMain') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview

        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                updatedFormElement.valid = true;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;

        if (event.target.files && event.target.files[0] === undefined) {
            updatedFormElement.elementConfig.data = '';
            updatedFormElement.errorMessage = 'Please select image!';
            updatedFormElement.valid = false;
        }

        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        stateClone.formElements[inputIdentifier].valid = stateClone.formElements[inputIdentifier].elementConfig.checked.length > 0 ? true : false;
        let formIsValid = true;
        for (let inputIdentifier in stateClone.formElements) {
            formIsValid = stateClone.formElements[inputIdentifier].valid && formIsValid;
        }
        stateClone.formIsValid = formIsValid;
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('is_main', formData.get('is_main') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateCategoryBannerRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow' || key === 'isMain')
                stateClone.formElements[key].value = false;
            else if (key === 'image') {
                stateClone.formElements[key].value = '';
                stateClone.formElements[key].elementConfig.data = '';
                stateClone.formElements[key].valid = false;
            }
            else if (key === 'category') {
                stateClone.formElements[key].elementConfig.checked = [];
                stateClone.formElements[key].elementConfig.expanded = [];
                stateClone.formElements[key].valid = false;
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['category'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.category.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateCategoryBannerRequest: data => { dispatch(createCategoryBannerRequest(data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_BANNER_CREATE)(CategoryBannerAdd));