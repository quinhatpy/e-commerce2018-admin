import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCategoriesRequest, fetchCategoryBannerRequest, updateCategoryBannerRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';
import { CATEGORY_BANNER_LIST } from '../../../constants/link';
import { CATEGORY_BANNER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren, getParents } from '../../../utils/helper';

class CategoryBannerEdit extends Component {
    state = {
        pageTitle: 'Edit Category Banner',
        breadcrumbItems: [
            {
                name: 'Category banner management',
                link: CATEGORY_BANNER_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            image: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'image',
                    data: ''
                },
                label: 'Image',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    // isURL: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            category: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'category_id',
                    data: [],
                    checked: [],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            isMain: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_main',
                    label: 'Show',
                },
                label: 'Show home',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: true,

    }

    componentDidMount() {
        const categoryBannerId = this.props.match.params.id;
        this.props.onFetchCategoryBannerRequest(categoryBannerId);
        this.props.onFetchCategoriesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { categories, item } = nextProps;
        let dataNested = getNestedChildren(categories, null);

        if (item) {

            let dataExpanded = item.category ? getParents(categories, item.category.id) : [];
            console.log(dataExpanded);
            stateClone.formElements.image.elementConfig.data = item.image;
            stateClone.formElements.link.value = item.link;
            stateClone.formElements.ordinal.value = item.ordinal;
            stateClone.formElements.category.elementConfig.checked = item.category ? [item.category.id] : [];
            stateClone.formElements.category.elementConfig.expanded = dataExpanded;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.isMain.value = item.is_main === 1 ? true : false;
        }

        stateClone.formElements.category.elementConfig.data = dataNested;

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // to slug
        if (inputIdentifier === 'isShow' || inputIdentifier === 'isMain') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('is_main', formData.get('is_main') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateCategoryBannerRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow' || key === 'isMain')
                stateClone.formElements[key].value = false;
            else if (key === 'image') {
                stateClone.formElements[key].value = '';
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if(key === 'category'){ 
                stateClone.formElements[key].elementConfig.checked= [];
                stateClone.formElements[key].elementConfig.expanded= [];
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;        
        this.setState(stateClone);
        const categoryBannerId = this.props.match.params.id;
        this.props.onFetchCategoryBannerRequest(categoryBannerId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['category'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.categoryBanner.item,
        categories: state.category.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateCategoryBannerRequest: (id, data) => { dispatch(updateCategoryBannerRequest(id, data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchCategoryBannerRequest: id => { dispatch(fetchCategoryBannerRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_BANNER_UPDATE)(CategoryBannerEdit));