import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchArticleRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { ARTICLE_LIST } from '../../../constants/link';
import { ARTICLE_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class ArticleDetail extends Component {
    state = {
        pageTitle: 'Article Detail',
        breadcrumbItems: [
            {
                name: 'Article management',
                link: ARTICLE_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchArticleRequest
        } = this.props;

        fetchArticleRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { article } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{article.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Slug</th>
                                                        <td>{article.slug}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thumbnail</th>
                                                        <td>{article.thumbnail ? <img src={article.thumbnail} alt="avatar" width="200" onError={(e) => { e.target.onerror = null; e.target.src = "/images/no-image.png" }}/> : <img src={NoImage} alt="avatar" width="200" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Short Description</th>
                                                        <td>{article.short_description}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Content</th>
                                                        <td dangerouslySetInnerHTML={{__html: article.content}}></td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{article.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Title</th>
                                                        <td>{article.seo_title}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Description</th>
                                                        <td>{article.seo_description}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Author</th>
                                                        <td>{article.user ? article.user.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(article.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(article.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        article: state.article.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchArticleRequest: id => { dispatch(fetchArticleRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ARTICLE_SHOW)(ArticleDetail));