import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchArticleRequest, fetchArticlesRequest, updateArticleRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns/FormTwoColumns';
import { ARTICLE_LIST } from '../../../constants/link';
import { ARTICLE_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { toSlug } from '../../../utils/helper';

class ArticleEdit extends Component {
    state = {
        pageTitle: 'Edit Article',
        breadcrumbItems: [
            {
                name: 'Article management',
                link: ARTICLE_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            slug: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'slug',
                    placeholder: 'Enter slug may not be greater than 150 characters'
                },
                label: 'Slug',
                value: '',
                validation: {
                    required: true,
                    maxLength: 150,
                    unique: {
                        key: 'slug',
                        data: [],
                        exclude: 0
                    }
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            shortDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'short_description',
                    placeholder: 'Enter description'
                },
                label: 'Description',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            content: {
                elementType: 'richText',
                elementConfig: {
                    name: 'content',
                    placeholder: 'Enter content'
                },
                label: 'Content',
                value:  BraftEditor.createEditorState(null),
                validation: {
                    required: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            thumbnail: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'thumbnail',
                    data: ''
                },
                label: 'Thumbnail',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoTitle: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'seo_title',
                    placeholder: 'Enter SEO title may not be greater than 60 characters'
                },
                label: 'SEO Title',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'seo_description',
                    placeholder: 'Enter SEO description'
                },
                label: 'SEO Description',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const articleId = this.props.match.params.id;
        this.props.onFetchArticleRequest(articleId);
        this.props.onFetchArticlesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { articles, item } = nextProps;

        if (item) {

            stateClone.formElements.name.value = item.name;
            stateClone.formElements.slug.value = item.slug;
            stateClone.formElements.shortDescription.value = item.short_description;
            stateClone.formElements.content.value = BraftEditor.createEditorState(item.content);
            stateClone.formElements.thumbnail.elementConfig.data = item.thumbnail;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.seoTitle.value = item.seo_title;
            stateClone.formElements.seoDescription.value = item.seo_description;
        }

        stateClone.formElements.slug.validation.unique.data = articles;
        stateClone.formElements.slug.validation.unique.exclude = this.props.match.params.id


        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        if(inputIdentifier === 'content') {
            updatedFormElement.value = event;
            updatedFormElement.valid = updatedFormElement.value.toHTML() !== '<p></p>'
        } else {
            updatedFormElement.value = event.target.value;

            // to slug
            if (inputIdentifier === 'name') {
                let slug = toSlug(event.target.value)
                updatedForm.slug.value = slug;
                updatedForm.slug.valid = true;
            }
            if (inputIdentifier === 'isShow') {
                updatedFormElement.value = event.target.checked;
            }

            // get image preview
            if (event.target.files && event.target.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    updatedFormElement.elementConfig.data = e.target.result;
                    const stateClone = { ...this.state };
                    updatedForm[inputIdentifier] = updatedFormElement;
                    stateClone.formElements = updatedForm;
                    this.setState(stateClone);
    
                }
                reader.readAsDataURL(event.target.files[0]);
            }
    
            // validate
            let checked = checkValidity(updatedForm, inputIdentifier);
    
            updatedFormElement.valid = checked.isValid;
            updatedFormElement.errorMessage = checked.errorMessage;
        }

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;
        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('content', this.state.formElements.content.value.toHTML())
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateArticleRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (_.indexOf(['thumbnail'], key) !== -1) {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if (_.indexOf(['content'], key) !== -1)
                stateClone.formElements[key].value = BraftEditor.createEditorState(null)
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        const articleId = this.props.match.params.id;
        this.props.onFetchArticleRequest(articleId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['thumbnail'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.article.item,
        articles: state.article.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateArticleRequest: (id, data) => { dispatch(updateArticleRequest(id, data)) },
        onFetchArticlesRequest: () => { dispatch(fetchArticlesRequest()) },
        onFetchArticleRequest: id => { dispatch(fetchArticleRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ARTICLE_UPDATE)(ArticleEdit));