import BraftEditor from 'braft-editor';
import 'braft-editor/dist/index.css';
import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createArticleRequest, fetchArticlesRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns/FormTwoColumns';
import { ARTICLE_LIST } from '../../../constants/link';
import { ARTICLE_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { toSlug } from '../../../utils/helper';

class ArticleAdd extends Component {
    state = {
        pageTitle: 'Add Article',
        breadcrumbItems: [
            {
                name: 'Article management',
                link: ARTICLE_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            slug: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'slug',
                    placeholder: 'Enter slug may not be greater than 150 characters'
                },
                label: 'Slug',
                value: '',
                validation: {
                    required: true,
                    maxLength: 150,
                    unique: {
                        key: 'slug',
                        data: [],
                        exclude: 0
                    }
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            shortDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'short_description',
                    placeholder: 'Enter description'
                },
                label: 'Description',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            content: {
                elementType: 'richText',
                elementConfig: {
                    name: 'content',
                    placeholder: 'Enter content'
                },
                label: 'Content',
                value: BraftEditor.createEditorState(null),
                validation: {
                    required: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            thumbnail: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'thumbnail',
                    data: ''
                },
                label: 'Thumbnail',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoTitle: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'seo_title',
                    placeholder: 'Enter SEO title may not be greater than 60 characters'
                },
                label: 'SEO Title',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'seo_description',
                    placeholder: 'Enter SEO description'
                },
                label: 'SEO Description',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: false,
    }

    componentDidMount() {
        this.props.onFetchArticlesRequest();
        document.title = this.state.pageTitle;

    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        stateClone.formElements.slug.validation.unique.data = nextProps.categories;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        if (inputIdentifier === 'content') {
            updatedFormElement.value = event;
            updatedFormElement.valid = updatedFormElement.value.toHTML() !== '<p></p>'
        } else {
            updatedFormElement.value = event.target.value;

            // to slug
            if (inputIdentifier === 'name') {
                let slug = toSlug(event.target.value)
                updatedForm.slug.value = slug;
                updatedForm.slug.valid = true;
            }
            // get image preview
            if (event.target.files && event.target.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    updatedFormElement.elementConfig.data = e.target.result;
                    const stateClone = { ...this.state };
                    updatedForm[inputIdentifier] = updatedFormElement;
                    stateClone.formElements = updatedForm;
                    this.setState(stateClone);

                }
                reader.readAsDataURL(event.target.files[0]);
            }

            // validate

            let checked = checkValidity(updatedForm, inputIdentifier);

            updatedFormElement.valid = checked.isValid;
            updatedFormElement.errorMessage = checked.errorMessage;
        }
        if (inputIdentifier !== 'content')
            updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;
        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);
    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('content', this.state.formElements.content.value.toHTML())
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateArticleRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (_.indexOf(['thumbnail'], key) !== -1) {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if (_.indexOf(['content'], key) !== -1)
                stateClone.formElements[key].value = BraftEditor.createEditorState(null)
            else stateClone.formElements[key].value = '';

            if (_.indexOf(['name', 'slug', 'content'], key) !== -1)
                stateClone.formElements[key].valid = false;

            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];
        console.log(formElements.content.value);
        for (let key in formElements) {
            if (_.indexOf(['thumbnail'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }


        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">

                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={9}
                                col2={3} />

                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.article.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateArticleRequest: data => { dispatch(createArticleRequest(data)) },
        onFetchArticlesRequest: () => { dispatch(fetchArticlesRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ARTICLE_CREATE)(ArticleAdd));