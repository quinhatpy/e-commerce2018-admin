import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCategoriesRequest, fetchCategoryRequest, updateCategoryRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';
import { CATEGORY_LIST } from '../../../constants/link';
import { CATEGORY_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren, getParents, toSlug } from '../../../utils/helper';

class CategoryEdit extends Component {
    state = {
        pageTitle: 'Edit Category',
        breadcrumbItems: [
            {
                name: 'Category management',
                link: CATEGORY_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            slug: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'slug',
                    placeholder: 'Enter slug may not be greater than 150 characters'
                },
                label: 'Slug',
                value: '',
                validation: {
                    required: true,
                    maxLength: 150,
                    unique: {
                        key: 'slug',
                        data: [],
                        exclude: 0,
                    }
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            icon: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'icon',
                    data: ''
                },
                label: 'Icon',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            categoryParent: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'parent_id',
                    data: [],
                    checked: [''],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category Parent',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',

                valid: true,
                touched: false
            },
            seoTitle: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'seo_title',
                    placeholder: 'Enter SEO title may not be greater than 60 characters'
                },
                label: 'SEO Title',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            seoDescription: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'seo_description',
                    placeholder: 'Enter SEO description'
                },
                label: 'SEO Description',
                value: '',
                validation: {
                    maxLength: 60
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const categoryId = this.props.match.params.id;
        this.props.onFetchCategoryRequest(categoryId);
        this.props.onFetchCategoriesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { categories, item } = nextProps;
        let dataNested = getNestedChildren(categories, null, parseInt(this.props.match.params.id));

        let dataDefault = {
            value: '',
            label: 'No parent'
        };

        if (item) {
            let dataExpanded = getParents(categories, item.id);

            stateClone.formElements.name.value = item.name;
            stateClone.formElements.slug.value = item.slug;
            stateClone.formElements.icon.elementConfig.data = item.icon;
            stateClone.formElements.categoryParent.elementConfig.checked = item.parent ? [item.parent.id] : [''];
            stateClone.formElements.categoryParent.elementConfig.expanded = dataExpanded;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.seoTitle.value = item.seo_title;
            stateClone.formElements.seoDescription.value = item.seo_description;
        }

        stateClone.formElements.categoryParent.elementConfig.data = [...dataNested, dataDefault];
        stateClone.formElements.slug.validation.unique.data = categories;
        stateClone.formElements.slug.validation.unique.exclude = this.props.match.params.id


        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // to slug
        if (inputIdentifier === 'name') {
            let slug = toSlug(event.target.value)
            updatedForm.slug.value = slug;
        }

        if (inputIdentifier === 'isShow') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        if (checked.value !== parseInt(this.props.match.params.id))
            stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateCategoryRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow' )
            stateClone.formElements[key].value = false;
           else if(key === 'icon'){
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else if(key === 'categoryParent'){ 
                stateClone.formElements[key].elementConfig.checked= [''];
                stateClone.formElements[key].elementConfig.expanded= [];
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;
        this.setState(stateClone);

        const categoryId = this.props.match.params.id;
        this.props.onFetchCategoryRequest(categoryId);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['categoryParent'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.category.item,
        categories: state.category.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateCategoryRequest: (id, data) => { dispatch(updateCategoryRequest(id, data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchCategoryRequest: id => { dispatch(fetchCategoryRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_UPDATE)(CategoryEdit));