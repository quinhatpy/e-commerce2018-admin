import { Icon, Popconfirm } from 'antd';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteCategoryRequest, fetchCategoriesRequest } from '../../../actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader';
import DataTable from '../../../components/DataTable';
import Pagination from '../../../components/Pagination';
import SearchBox from '../../../components/SearchBox';
import Overlay from '../../../components/UI/Overlay';
import { CATEGORY_DETAIL, CATEGORY_EDIT, CATEGORY_LIST } from "../../../constants/link";
import { CATEGORY_DELETE, CATEGORY_INDEX, CATEGORY_SHOW, CATEGORY_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';

class CategoryList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        params: {},
        pageTitle: 'List Category',
        breadcrumbItems: [
            {
                name: 'Category management',
                link: CATEGORY_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'slug',
                label: 'Slug',
                sortable: true
            },
            {
                name: 'icon',
                label: 'Icon',
                sortable: false
            },
            {
                name: 'parent_id',
                label: 'Category Parent',
                sortable: true
            },
            {
                name: 'is_show',
                label: 'Status',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }
    }


    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);
        stateClone.keyword = stateClone.params.keyword ? stateClone.params.keyword : '';

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;
        this.setState(stateClone);

        this.props.onFetchCategoriesRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();

    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: CATEGORY_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: CATEGORY_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteCategoryRequest } = this.props;
        return data.length === 0 ? <tr><td colSpan="7" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.slug}</td>
                    <td>{item.icon ? <img src={item.icon} alt="avatar" width="50"  onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}}/> : <img src={NoImage} alt="avatar" width="50" />}</td>
                    <td>{item.parent ? item.parent.name : null}</td>
                    <td align="center">{item.is_show === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                CATEGORY_SHOW,
                                userAuth,
                                <Link to={CATEGORY_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                CATEGORY_UPDATE,
                                userAuth,
                                <Link to={CATEGORY_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                CATEGORY_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this category?"
                                    onConfirm={() => onDeleteCategoryRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            categories,
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title"> </h3>
                                    <div className="box-tools">
                                        <SearchBox
                                            placeholder="Search by name"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>
                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(categories)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={CATEGORY_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        categories: state.category.list,
        totalRows: state.category.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchCategoriesRequest: (offset, limit, params) => { dispatch(fetchCategoriesRequest(offset, limit, params)) },
        onDeleteCategoryRequest: (id, offset, limit) => { dispatch(deleteCategoryRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_INDEX)(CategoryList));