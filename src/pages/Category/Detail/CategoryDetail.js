import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchCategoryRequest } from '../../../actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader';
import Overlay from '../../../components/UI/Overlay';
import { CATEGORY_LIST } from '../../../constants/link';
import { CATEGORY_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';

class CategoryDetail extends Component {
    state = {
        pageTitle: 'Category Detail',
        breadcrumbItems: [
            {
                name: 'Category management',
                link: CATEGORY_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchCategoryRequest
        } = this.props;

        fetchCategoryRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { category } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{category.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Slug</th>
                                                        <td>{category.slug}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Icon</th>
                                                        <td>{category.icon ? <img src={category.icon} alt="avatar" width="100" onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}} /> : <img src={NoImage} alt="avatar" width="100" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{category.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Title</th>
                                                        <td>{category.seo_title}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>SEO Description</th>
                                                        <td>{category.seo_description}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Category Parent</th>
                                                        <td>{category.parent ? category.parent.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(category.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(category.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        category: state.category.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCategoryRequest: id => { dispatch(fetchCategoryRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(CATEGORY_SHOW)(CategoryDetail));