import moment from 'moment';
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import _ from 'lodash/string';
import { Popconfirm, Icon } from 'antd';
import { ROLE_LIST, ROLE_EDIT } from '../../../constants/link';
import { ROLE_INDEX, ROLE_UPDATE, ROLE_DELETE } from '../../../constants/permission';
import { fetchRolesRequest, deleteRoleRequest } from '../../../actions';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission';
import ContentHeader from '../../../components/ContentHeader';
import Overlay from '../../../components/UI/Overlay';
import DataTable from '../../../components/DataTable';

class RoleList extends Component {
    state = {
        keyword: '',
        pageTitle: 'List Role',
        breadcrumbItems: [
            {
                name: 'Role Management',
                link: ROLE_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: false
            },
            {
                name: 'name',
                label: 'Name',
                sortable: false
            },
            {
                name: 'permissions',
                label: 'Permissions',
                sortable: false
            },
            {
                name: 'updated_at',
                label: 'Updated At',
                sortable: false
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchRolesRequest();
        document.title = this.state.pageTitle;
    }


    handleOnChangeKeyword = (event) => {
        let stateClone = { ...this.state };
        stateClone.keyword = event.target.value;
        this.setState(stateClone);
    }

    renderList(data) {
        const { userAuth, onDeleteRoleRequest } = this.props;
        return data.length === 0 ? <tr><td colSpan="5" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{item.name}</td>
                    <td>
                        <ul className="list-unstyled">
                            {this.showPermission(item.permissions)}
                        </ul>
                    </td>
                    <td>{moment(item.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                    <td>
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                ROLE_UPDATE,
                                userAuth,
                                <Link to={ROLE_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fa fa-edit" /></Link>
                            )}
                            {checkPermission(
                                ROLE_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this role?"
                                    onConfirm={() => onDeleteRoleRequest(item.id)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    showPermission(permissions) {
        let permissionList = {};
        for (let key in permissions) {
            const permissionRow = key.split('.');
            const permissionKey = permissionRow[0];
            const permissionValue = permissionRow[1];

            if (!(permissionKey in permissionList))
                permissionList[permissionKey] = [];

            if (permissions[key])
                permissionList[permissionKey].push([permissionValue]);
        }

        // console.log(permissionList);
        let element = [];
        for (let key in permissionList) {
            element.push(
                <li key={key}><b>{_.capitalize(key)}</b>: {permissionList[key].join(', ')}</li>
            )
        }

        return element;
    }


    render() {
        const {
            pageTitle,
            breadcrumbItems,
            keyword,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            roles
        } = this.props;

        let rolesFilter = roles.filter(role => {
            return role.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1;
        });

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title"> </h3>
                                    <div className="box-tools">
                                        <div className="input-group input-group-sm" style={{ width: 150 }}>
                                            <input type="search" name="keyword" className="form-control pull-right" placeholder="Search name"
                                                value={keyword}
                                                onChange={(event) => this.handleOnChangeKeyword(event)} />
                                        </div>
                                    </div>
                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(rolesFilter)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                            </div>
                            {/* /.box */}
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        roles: state.role.list,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchRolesRequest: () => { dispatch(fetchRolesRequest()) },
        onDeleteRoleRequest: id => { dispatch(deleteRoleRequest(id)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ROLE_INDEX)(RoleList));