import _ from 'lodash/string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createRoleRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsWithTop from '../../../components/Form/TwoColumnsWithTop';
import { ROLE_LIST } from '../../../constants/link';
import { ROLE_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class RoleAdd extends Component {
    state = {
        pageTitle: 'Add Role',
        breadcrumbItems: [
            {
                name: 'Role management',
                link: ROLE_LIST
            },
            {
                name: 'Add new'
            }
        ],
        permissionsModel: ['attribute', 'banner', 'article', 'branch', 'brand', 'category', 'category_banner', 'customer', 'district', 'menu', 'order', 'product', 'province', 'role', 'user', 'ward', 'gallery'],
        permissionAction: ['index', 'show', 'create', 'update', 'delete'],
        roleElements: {},
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Role name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            }
        },

        formIsValid: false,

    }

    componentDidMount() {
        const stateClone = { ...this.state };
        for (let model of stateClone.permissionsModel) {
            stateClone.roleElements[model] = {
                elementType: 'checkboxInline',
                elementConfig: {
                    children: []
                },
                label: _.capitalize(model),
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }

            for (let action of stateClone.permissionAction) {
                stateClone.roleElements[model].elementConfig.children.push(
                    {
                        label: action,
                        elementConfig: {
                            name: `${model}[${action}]`,
                            defaultChecked: false
                        }
                    }
                );
            }
        }
        this.setState(stateClone);

        document.title = this.state.pageTitle;
    }


    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        const stateClone = { ...this.state };

        updatedFormElement.value = event.target.value;

        // validate
        if (inputIdentifier === 'name') {
            let checked = checkValidity(updatedForm, inputIdentifier);

            updatedFormElement.valid = checked.isValid;
            updatedFormElement.errorMessage = checked.errorMessage;

            updatedFormElement.touched = true;
            updatedForm[inputIdentifier] = updatedFormElement;

            let formIsValid = true;
            for (let inputIdentifier in updatedForm) {
                formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
            }

            stateClone.formElements = updatedForm;
            stateClone.formIsValid = formIsValid;
        } else {
            let roleActions = stateClone.roleElements[inputIdentifier].elementConfig.children;

            roleActions.forEach(action => {
                if (action.elementConfig.name === event.target.name)
                    action.active = event.target.checked;
            })
           
        }
        this.setState(stateClone);

    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);

        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateRoleRequest(formData);
    }

    handleReset = (event) => {
        const stateClone = { ...this.state };
        stateClone.formElements.name.value = '';
        stateClone.formElements.name.valid = false;
        stateClone.formElements.name.touched = false;
        stateClone.formElements.name.errorMessage = '';
        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid, roleElements } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [], formElementArrayTop = [];

        for (let key in formElements) {
            formElementArrayTop.push({
                id: key,
                config: formElements[key]
            });
        }

        let count = 0;
        for (let key in roleElements) {
            count++;
            if (count <= 9)
                formElementArrayLeft.push({
                    id: key,
                    config: roleElements[key]
                });
            else formElementArrayRight.push({
                id: key,
                config: roleElements[key]
            });
        }


        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsWithTop
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayTop={formElementArrayTop}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                colTop={6}
                                colOffset="col-md-offset-3"
                                col1={6}
                                col2={6} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert
    }
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        onCreateRoleRequest: data => { dispatch(createRoleRequest(data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ROLE_CREATE)(RoleAdd));