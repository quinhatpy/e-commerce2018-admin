import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createProvinceRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { PROVINCE_LIST } from '../../../constants/link';
import { PROVINCE_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';


class ProvinceAdd extends Component {
    state = {
        pageTitle: 'Add Province',
        breadcrumbItems: [
            {
                name: 'Province management',
                link: PROVINCE_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            type: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'type',
                    placeholder: 'Enter type'
                },
                label: 'Type',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
        },

        formIsValid: false,

    }

    componentDidMount() {
        document.title = this.state.pageTitle;
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateProvinceRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].valid = false;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateProvinceRequest: data => { dispatch(createProvinceRequest(data)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(PROVINCE_CREATE)(ProvinceAdd));