import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createAttributeRequest, fetchCategoriesRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';
import { ATTRIBUTE_LIST } from '../../../constants/link';
import { ATTRIBUTE_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren } from '../../../utils/helper';

class AttributeAdd extends Component {
    state = {
        pageTitle: 'Add Attribute',
        breadcrumbItems: [
            {
                name: 'Attribute management',
                link: ATTRIBUTE_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            dataType: {
                elementType: 'select',
                label: 'Data Type',
                elementConfig: {
                    type: 'select',
                    name: 'data_type',
                    options: [
                        {
                            value: 'string',
                            displayValue: 'String'
                        },
                        {
                            value: 'number',
                            displayValue: 'Number'
                        },
                        {
                            value: 'text',
                            displayValue: 'Text'
                        }
                    ],
                },
                value: '',
                validation: {},
                valid: true,
                touched: false
            },
            isRequired: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_required',
                    label: 'Yes',
                },
                label: 'Required',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            defaultValue: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'default_value',
                    placeholder: 'Enter default value'
                },
                label: 'Default Value',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            category: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'category_id',
                    data: [],
                    checked: [],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },

        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchCategoriesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataNested = getNestedChildren(nextProps.categories, null);

        stateClone.formElements.category.elementConfig.data = dataNested;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        if (inputIdentifier === 'isShow' || inputIdentifier === 'isRequired') {
            updatedFormElement.value = event.target.checked;
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;

        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        stateClone.formElements[inputIdentifier].valid = stateClone.formElements[inputIdentifier].elementConfig.checked.length > 0 ? true : false;
        let formIsValid = true;
        for (let inputIdentifier in stateClone.formElements) {
            formIsValid = stateClone.formElements[inputIdentifier].valid && formIsValid;
        }
        stateClone.formIsValid = formIsValid;
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('is_required', formData.get('is_required') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateAttributeRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow' || key === 'isMain')
                stateClone.formElements[key].value = false;
            else if (key === 'category') {
                stateClone.formElements[key].elementConfig.checked = [];
                stateClone.formElements[key].elementConfig.expanded = [];
                stateClone.formElements[key].valid = false;
            }
            else stateClone.formElements[key].value = '';

            if (key === 'name')
                stateClone.formElements[key].valid = false;

            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['category'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.category.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateAttributeRequest: data => { dispatch(createAttributeRequest(data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ATTRIBUTE_CREATE)(AttributeAdd));