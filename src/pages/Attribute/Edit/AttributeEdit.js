import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchAttributeRequest, fetchCategoriesRequest, updateAttributeRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumnsHasCheckBoxTree from '../../../components/Form/TwoColumnsHasCheckBoxTree';
import { ATTRIBUTE_LIST } from '../../../constants/link';
import { ATTRIBUTE_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';
import { getNestedChildren, getParents } from '../../../utils/helper';

class AttributeEdit extends Component {
    state = {
        pageTitle: 'Edit Attribute',
        breadcrumbItems: [
            {
                name: 'Attribute management',
                link: ATTRIBUTE_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            dataType: {
                elementType: 'select',
                label: 'Data Type',
                elementConfig: {
                    type: 'select',
                    name: 'data_type',
                    options: [
                        {
                            value: 'string',
                            displayValue: 'String'
                        },
                        {
                            value: 'number',
                            displayValue: 'Number'
                        },
                        {
                            value: 'text',
                            displayValue: 'Text'
                        }
                    ],
                },
                value: '',
                validation: {},
                valid: true,
                touched: false
            },
            isRequired: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_required',
                    label: 'Yes',
                },
                label: 'Required',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            defaultValue: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'default_value',
                    placeholder: 'Enter default value'
                },
                label: 'Default Value',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    placeholder: 'Enter ordinal'
                },
                label: 'Ordinal',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            category: {
                elementType: 'radioTree',
                elementConfig: {
                    name: 'category_id',
                    data: [],
                    checked: [''],
                    expanded: [],
                    noCascade: true,
                    showNodeIcon: false
                },
                label: 'Category',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const attributeId = this.props.match.params.id;
        this.props.onFetchAttributeRequest(attributeId);
        this.props.onFetchCategoriesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { categories, item } = nextProps;
        let dataNested = getNestedChildren(categories, null);

        if (item) {
            let dataExpanded = item.category ? getParents(categories, item.category.id) : [];

            stateClone.formElements.name.value = item.name;
            stateClone.formElements.defaultValue.value = item.name;
            stateClone.formElements.dataType.value = item.data_type;
            stateClone.formElements.category.elementConfig.checked = item.category ? [item.category.id] : [];
            stateClone.formElements.category.elementConfig.expanded = dataExpanded;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
            stateClone.formElements.isRequired.value = item.is_required === 1 ? true : false;
            stateClone.formElements.ordinal.value = item.ordinal;
        }

        stateClone.formElements.category.elementConfig.data = dataNested;

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        if (inputIdentifier === 'isShow' || inputIdentifier === 'isRequired') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);


        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            console.log(updatedForm[inputIdentifier].valid);
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }


        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleCheckedCheckboxTree(inputIdentifier, checked) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.checked = [checked.value];
        this.setState(stateClone);
    }

    handleExpandedCheckboxTree(inputIdentifier, expanded) {
        const stateClone = { ...this.state };
        stateClone.formElements[inputIdentifier].elementConfig.expanded = expanded;
        this.setState(stateClone);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        formData.set('is_required', formData.get('is_required') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateAttributeRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;
        this.setState(stateClone);

        const attributeId = this.props.match.params.id;
        this.props.onFetchAttributeRequest(attributeId);
    }


    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['category'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumnsHasCheckBoxTree
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                handleCheckedCheckboxTree={(inputIdentifier, checked) => this.handleCheckedCheckboxTree(inputIdentifier, checked)}
                                handleExpandedCheckboxTree={(inputIdentifier, checked) => this.handleExpandedCheckboxTree(inputIdentifier, checked)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.attribute.item,
        categories: state.category.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateAttributeRequest: (id, data) => { dispatch(updateAttributeRequest(id, data)) },
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchAttributeRequest: id => { dispatch(fetchAttributeRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ATTRIBUTE_UPDATE)(AttributeEdit));