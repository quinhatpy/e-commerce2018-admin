import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { TreeSelect, Popconfirm, Icon } from 'antd';
import { ATTRIBUTE_LIST, ATTRIBUTE_EDIT, ATTRIBUTE_DETAIL } from '../../../constants/link';
import { ATTRIBUTE_INDEX, ATTRIBUTE_SHOW, ATTRIBUTE_UPDATE, ATTRIBUTE_DELETE } from '../../../constants/permission';
import { fetchCategoriesRequest, fetchAttributesRequest, deleteAttributeRequest } from '../../../actions/actions';
import scrollTop from '../../../utils/scrollTop';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission';
import ContentHeader from '../../../components/ContentHeader';
import Overlay from '../../../components/UI/Overlay';
import DataTable from '../../../components/DataTable';
import Pagination from '../../../components/Pagination';

class AttributeList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        categorySelect: '',
        params: {},
        pageTitle: 'List Attribute',
        breadcrumbItems: [
            {
                name: 'Attribute management',
                link: ATTRIBUTE_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'data_type',
                label: 'Data Type',
                sortable: false
            },
            {
                name: 'is_required',
                label: 'Required',
                sortable: true
            },
            {
                name: 'is_show',
                label: 'Status',
                sortable: true
            },
            {
                name: 'ordinal',
                label: 'Ordinal',
                sortable: true
            },
            {
                name: 'category_id',
                label: 'Category',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchCategoriesRequest();
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
            this.setState({ categorySelect: '' })
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let category = nextProps.categories.find(category => {
                return category.id === parseInt(queryParam.category_id);
            });

            this.setState({ categorySelect: category ? category.name : '' })
        }
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);
        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.props.onFetchAttributesRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
        this.setState(stateClone);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: ATTRIBUTE_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSelectCategory = (value, node, extra) => {
        let params = { ...this.state.params };
        params.category_id = node.props.eventKey;
        // let stateClone = { ...this.state };

        // this.setState(stateClone);

        // let params = { ...this.state.params };
        // params.category_id = node.props.eventKey;
        let queryParam = queryString.stringify(params);

        this.props.history.push({
            pathname: ATTRIBUTE_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnChangeTreeSelect = (value, label, extra) => {
        if (value === undefined) {
            let params = { ...this.state.params };
            delete params.category_id;
            let queryParam = queryString.stringify(params);

            this.props.history.push({
                pathname: ATTRIBUTE_LIST,
                search: '?' + queryParam
            });
            this.handlePageChange(1, queryParam);
        }
    }

    renderSelectTree(categories, parentId = null) {
        let out = [];
        const TreeNode = TreeSelect.TreeNode;
        for (let i in categories) {
            let itemParentId = categories[i].parent ? categories[i].parent.id : null;
            if (itemParentId === parentId) {
                out.push(
                    <TreeNode value={categories[i].name} title={categories[i].name} key={categories[i].id}>
                        {this.renderSelectTree(categories, categories[i].id)}
                    </TreeNode>
                )
            }
        }
        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteAttributeRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.data_type}</td>
                    <td align="center">{item.is_required === 1 ? <span className="label label-success">True</span> : <span className="label label-default">False</span>}</td>
                    <td align="center">{item.is_show === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td>{item.ordinal}</td>
                    <td>{item.category ? item.category.name : null}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                ATTRIBUTE_SHOW,
                                userAuth,
                                <Link to={ATTRIBUTE_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                ATTRIBUTE_UPDATE,
                                userAuth,
                                <Link to={ATTRIBUTE_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                ATTRIBUTE_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this attribute?"
                                    onConfirm={() => onDeleteAttributeRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />}
                                >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            categorySelect,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            categories,
            attributes
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                    </h3>
                                    <div className="box-tools">
                                        <b>Category: </b><TreeSelect
                                            showSearch
                                            style={{ width: 300 }}
                                            value={categorySelect}
                                            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
                                            placeholder="Please select category"
                                            allowClear
                                            onSelect={this.handleOnSelectCategory}
                                            onChange={this.handleOnChangeTreeSelect}>
                                            {this.renderSelectTree(categories)}
                                        </TreeSelect>
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(attributes)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={ATTRIBUTE_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        categories: state.category.list,
        attributes: state.attribute.list,
        totalRows: state.attribute.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchCategoriesRequest: () => { dispatch(fetchCategoriesRequest()) },
        onFetchAttributesRequest: (offset, limit, params) => { dispatch(fetchAttributesRequest(offset, limit, params)) },
        onDeleteAttributeRequest: (id, offset, limit) => { dispatch(deleteAttributeRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ATTRIBUTE_INDEX)(AttributeList));