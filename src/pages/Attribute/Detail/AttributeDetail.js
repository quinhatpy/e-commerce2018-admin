import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchAttributeRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { ATTRIBUTE_LIST } from '../../../constants/link';
import { ATTRIBUTE_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class AttributeDetail extends Component {
    state = {
        pageTitle: 'Attribute Detail',
        breadcrumbItems: [
            {
                name: 'Attribute management',
                link: ATTRIBUTE_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchAttributeRequest
        } = this.props;

        fetchAttributeRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { attribute } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{attribute.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Data Type</th>
                                                        <td>{attribute.data_type}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Required</th>
                                                        <td>{attribute.is_show === 1 ? 'Yes' : 'No'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Default Value</th>
                                                        <td>{attribute.default_value}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{attribute.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ordinal</th>
                                                        <td>{attribute.ordinal}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Category</th>
                                                        <td>{attribute.category ? attribute.category.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(attribute.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(attribute.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        attribute: state.attribute.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchAttributeRequest: id => { dispatch(fetchAttributeRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(ATTRIBUTE_SHOW)(AttributeDetail));