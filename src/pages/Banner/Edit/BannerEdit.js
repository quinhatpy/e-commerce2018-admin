import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBannerRequest, updateBannerRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns/FormTwoColumns';
import { BANNER_LIST } from '../../../constants/link';
import { BANNER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class BannerEdit extends Component {
    state = {
        pageTitle: 'Edit Banner',
        breadcrumbItems: [
            {
                name: 'Banner management',
                link: BANNER_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            image: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'image',
                    data: ''
                },
                label: 'Image',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            position: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'position',
                    placeholder: 'Enter position'
                },
                label: 'Position',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    isURL: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    label: 'Ordinal',
                },
                label: 'Status',
                value: 1,
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            }
        },

        formIsValid: true,

    }

    componentDidMount() {
        const bannerId = this.props.match.params.id;
        this.props.onFetchBannerRequest(bannerId);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { item } = nextProps;

        if (item) {
            stateClone.formElements.image.elementConfig.data = item.image;
            stateClone.formElements.position.value = item.position;
            stateClone.formElements.link.value = item.link;
            stateClone.formElements.ordinal.value = item.ordinal;
            stateClone.formElements.isShow.value = item.is_show === 1 ? true : false;
        }

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // to slug
        if (inputIdentifier === 'isShow') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateBannerRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (key === 'image') {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else stateClone.formElements[key].value = '';

            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = true;
        this.setState(stateClone);

        const bannerId = this.props.match.params.id;
        this.props.onFetchBannerRequest(bannerId);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['image'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.banner.item,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateBannerRequest: (id, data) => { dispatch(updateBannerRequest(id, data)) },
        onFetchBannerRequest: id => { dispatch(fetchBannerRequest(id)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BANNER_UPDATE)(BannerEdit));