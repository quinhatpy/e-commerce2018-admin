import _ from 'lodash/array';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createBannerRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns';
import { BANNER_LIST } from '../../../constants/link';
import { BANNER_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class BannerAdd extends Component {
    state = {
        pageTitle: 'Add Banner',
        breadcrumbItems: [
            {
                name: 'Banner management',
                link: BANNER_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            image: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'image',
                    data: ''
                },
                label: 'Image',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
            },
            position: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'position',
                    placeholder: 'Enter position'
                },
                label: 'Position',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            link: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'link',
                    placeholder: 'Enter link'
                },
                label: 'Link',
                value: '',
                validation: {
                    // isURL: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            ordinal: {
                elementType: 'input',
                elementConfig: {
                    type: 'number',
                    name: 'ordinal',
                    label: 'Ordinal',
                },
                label: 'Status',
                value: '',
                validation: {
                    isInt: true
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            isShow: {
                elementType: 'checkbox',
                elementConfig: {
                    name: 'is_show',
                    label: 'Show',
                },
                label: 'Status',
                value: false,
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
        },

        formIsValid: false,

    }

    componentDidMount() {

        document.title = this.state.pageTitle;
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        if (inputIdentifier === 'isShow') {
            updatedFormElement.value = event.target.checked;
        }
        // get image preview

        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (e) => {
                updatedFormElement.elementConfig.data = e.target.result;
                updatedFormElement.valid = true;
                const stateClone = { ...this.state };
                updatedForm[inputIdentifier] = updatedFormElement;
                stateClone.formElements = updatedForm;
                this.setState(stateClone);

            }
            reader.readAsDataURL(event.target.files[0]);
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;

        if (event.target.files && event.target.files[0] === undefined) {
            updatedFormElement.elementConfig.data = '';
            updatedFormElement.errorMessage = 'Please select image!';
            updatedFormElement.valid = false;
        }

        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        formData.set('is_show', formData.get('is_show') === 'on' ? 1 : 0);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateBannerRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'isShow')
                stateClone.formElements[key].value = false;
            else if (key === 'image') {
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else stateClone.formElements[key].value = '';

            if (_.indexOf(['position', 'image'], key) !== -1)
                stateClone.formElements[key].valid = false;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = false;
        this.setState(stateClone);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['image'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateBannerRequest: data => { dispatch(createBannerRequest(data)) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BANNER_CREATE)(BannerAdd));