import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBannerRequest } from '../../../actions/actions';
import NoImage from '../../../assets/no-image.png';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { BANNER_LIST } from '../../../constants/link';
import { BANNER_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class BannerDetail extends Component {
    state = {
        pageTitle: 'Banner Detail',
        breadcrumbItems: [
            {
                name: 'Banner management',
                link: BANNER_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchBannerRequest
        } = this.props;

        fetchBannerRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { banner } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Position</th>
                                                        <td>{banner.position}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Link</th>
                                                        <td>{banner.link ? <a href={'https://e2018-nit.herokuapp.com' + banner.link} target="_blank" rel="noopener noreferrer">{banner.link}</a> : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Image</th>
                                                        <td>{banner.image ? <img src={banner.image} alt="avatar" height="200" onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}}/> : <img src={NoImage} alt="avatar" width="100" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Status</th>
                                                        <td>{banner.is_show === 1 ? 'Show' : 'Hide'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ordinal</th>
                                                        <td>{banner.ordinal}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(banner.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(banner.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        banner: state.banner.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchBannerRequest: id => { dispatch(fetchBannerRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BANNER_SHOW)(BannerDetail));