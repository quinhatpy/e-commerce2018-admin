import { Icon, Popconfirm } from 'antd';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteBannerRequest, fetchBannersRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import DataTable from '../../../components/DataTable';
import Pagination from '../../../components/Pagination';
import SearchBox from '../../../components/SearchBox';
import Overlay from '../../../components/UI/Overlay';
import { BANNER_DETAIL, BANNER_EDIT, BANNER_LIST } from '../../../constants/link';
import { BANNER_DELETE, BANNER_INDEX, BANNER_SHOW, BANNER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';

class BannerList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        params: {},
        pageTitle: 'List Banner',
        breadcrumbItems: [
            {
                name: 'Banner management',
                link: BANNER_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'image',
                label: 'Image',
                sortable: false
            },
            {
                name: 'position',
                label: 'Position',
                sortable: true
            },
            {
                name: 'link',
                label: 'Link',
                sortable: false
            },
            {
                name: 'ordinal',
                label: 'Ordinal',
                sortable: true
            },
            {
                name: 'is_show',
                label: 'Status',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }
    }

    handlePageChange(pageNumber, queryParams = '') {

        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.setState(stateClone);
        this.props.onFetchBannersRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: BANNER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: BANNER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteBannerRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.image ? <img src={item.image} alt="avatar" width="100" onError={(e)=>{e.target.onerror = null; e.target.src="/images/no-image.png"}}/> : null}</td>
                    <td align="center">{item.position}</td>
                    <td width="300">{item.link ? <a href={item.link} target="_blank" rel="noopener noreferrer">{item.link}</a> : null}</td>
                    <td>{item.ordinal}</td>
                    <td align="center">{item.is_show === 1 ? <span className="label label-success">Show</span> : <span className="label label-default">Hide</span>}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                BANNER_SHOW,
                                userAuth,
                                <Link to={BANNER_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                BANNER_UPDATE,
                                userAuth,
                                <Link to={BANNER_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                BANNER_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this banner?"
                                    onConfirm={() => onDeleteBannerRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            banners
        } = this.props;


        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title">
                                    </h3>
                                    <div className="box-tools">
                                        <SearchBox
                                            placeholder="Search by position"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(banners)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={BANNER_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        banners: state.banner.list,
        totalRows: state.banner.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchBannersRequest: (offset, limit, params) => { dispatch(fetchBannersRequest(offset, limit, params)) },
        onDeleteBannerRequest: (id, offset, limit) => { dispatch(deleteBannerRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BANNER_INDEX)(BannerList));