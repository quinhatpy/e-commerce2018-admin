import { Avatar, Icon, Popconfirm } from 'antd';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteUserRequest, fetchUsersRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';
import SearchBox from '../../../components/SearchBox/SearchBox';
import Overlay from '../../../components/UI/Overlay';
import { USER_DETAIL, USER_EDIT, USER_LIST } from '../../../constants/link';
import { USER_DELETE, USER_INDEX, USER_SHOW, USER_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';


class UserList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        params: {},
        pageTitle: 'List User',
        breadcrumbItems: [
            {
                name: 'User management',
                link: USER_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'avatar',
                label: 'Avatar',
                sortable: false
            },
            {
                name: 'email',
                label: 'Email',
                sortable: true
            },
            {
                name: 'phone',
                label: 'Mobile Phone',
                sortable: true
            },
            {
                name: 'gender',
                label: 'Gender',
                sortable: true
            },
            {
                name: 'role_id',
                label: 'Role',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            },
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
        }
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;
        this.setState(stateClone);

        this.props.onFetchUsersRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();

    }

    handleOnSearch = (keyword) => {
        let stateClone = { ...this.state };
        stateClone.params.keyword = keyword;
        this.setState(stateClone);

        let queryParam = queryString.stringify(stateClone.params);
        this.props.history.push({
            pathname: USER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: USER_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderList(data) {
        const { userAuth, onDeleteUserRequest } = this.props;
        const { offset, limit } = this.state;
        return data.map((item, index) => {

            return <tr key={index}>
                <td>{offset + index + 1}</td>
                <td>{item.name}</td>
                <td>{item.avatar ? <img src={item.avatar} alt="avatar" width="50" /> : <Avatar shape="square" size={50} icon="user" />}</td>
                <td>{item.email}</td>
                <td>{item.phone}</td>
                <td>{item.gender === 1 ? 'Male' : 'Female'}</td>
                <td>{item.role.name}</td>
                <td align="center">
                    <div className="btn-group" role="group" aria-label="Basic example">
                        {checkPermission(
                            USER_SHOW,
                            userAuth,
                            <Link to={USER_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                        )}
                        {checkPermission(
                            USER_UPDATE,
                            userAuth,
                            <Link to={USER_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit" /></Link>
                        )}
                        {checkPermission(
                            USER_DELETE,
                            userAuth,
                            <Popconfirm
                                placement="topRight"
                                title="Are you sure to delete this user?"
                                onConfirm={() => onDeleteUserRequest(item.id, offset, limit)}
                                okText="Yes"
                                cancelText="No"
                                okType="danger"
                                icon={<Icon type="question-circle" />} >
                                <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                            </Popconfirm>
                        )}
                    </div>
                </td>
            </tr>
        })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            params,
            limit,
            currentPage,
            offset,
            sortBy,
            tableHeadColumns
        } = this.state;
        const {
            totalRows,
            users
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="box-header">
                                    <h3 className="box-title"> </h3>
                                    <div className="box-tools">
                                        <SearchBox
                                            placeholder="Search by name"
                                            handleOnSearch={(keyword) => this.handleOnSearch(keyword)} />
                                    </div>
                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(users, offset)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={USER_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        users: state.user.list,
        totalRows: state.user.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchUsersRequest: (offset, limit, params) => { dispatch(fetchUsersRequest(offset, limit, params)) },
        onDeleteUserRequest: (id, offset, limit) => { dispatch(deleteUserRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(USER_INDEX)(UserList));