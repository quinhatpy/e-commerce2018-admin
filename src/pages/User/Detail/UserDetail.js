import { Avatar } from 'antd';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchUserRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import Overlay from '../../../components/UI/Overlay';
import { USER_LIST } from '../../../constants/link';
import { USER_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';

class UserDetail extends Component {
    state = {
        pageTitle: 'User Detail',
        breadcrumbItems: [
            {
                name: 'User management',
                link: USER_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchUserRequest
        } = this.props;

        fetchUserRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { userItem } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{userItem.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>{userItem.email}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Avatar</th>
                                                        <td>{userItem.avatar ? <img src={userItem.avatar} alt="avatar" width="200" /> : <Avatar shape="square" size={200} icon="user" />}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Phone Number</th>
                                                        <td>{userItem.phone}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Address</th>
                                                        <td>{userItem.address}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Gender</th>
                                                        <td>{userItem.gender === 1 ? 'Male' : 'Female'}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Birthday</th>
                                                        <td>{moment(userItem.birthday, 'YYYY-MM-DD').format('DD-MM-YYYY')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Role</th>
                                                        <td>{userItem.role ? userItem.role.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(userItem.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(userItem.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        userItem: state.user.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchUserRequest: id => { dispatch(fetchUserRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(USER_SHOW)(UserDetail));