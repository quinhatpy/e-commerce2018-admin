import _ from 'lodash/array';
import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createUserRequest, fetchRolesRequest } from '../../../actions';
import ContentHeader from '../../../components/ContentHeader';
import FormTwoColumns from '../../../components/Form/TwoColumns';
import { USER_LIST } from '../../../constants/link';
import { USER_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class UserAdd extends Component {
    state = {
        pageTitle: 'Add User',
        breadcrumbItems: [
            {
                name: 'User management',
                link: USER_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Full name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    name: 'email',
                    placeholder: 'Enter email'
                },
                label: 'Email address',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password',
                    placeholder: 'Enter password must be at least 6 characters'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                label: 'Password',
                errorMessage: '',
                valid: false,
                touched: false
            },
            password_confirmation: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    name: 'password_confirmation',
                    placeholder: 'Enter password confirmation'
                },
                label: 'Password Confirmation',
                value: '',
                validation: {
                    required: true,
                    isEqualWith: 'password'
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            phone: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'phone',
                    placeholder: 'Enter mobile phone in the format 0xxxxxxxxx'
                },
                label: 'Mobile phone',
                value: '',
                validation: {
                    isMobilePhone: true
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            address: {
                elementType: 'textarea',
                elementConfig: {
                    name: 'address',
                    placeholder: 'Enter address'
                },
                label: 'Address',
                value: '',
                validation: {},
                errorMessage: '',
                valid: true,
                touched: false
            },
            gender: {
                elementType: 'radio',
                label: 'Gender',
                elementConfig: {
                    children: [
                        {
                            label: 'Male',
                            elementConfig: {
                                name: 'gender',
                                value: 1,
                            }
                        },
                        {
                            label: 'Female',
                            elementConfig: {
                                name: 'gender',
                                value: 0
                            }
                        }
                    ],
                },
                value: 1,
                validation: {},
                valid: true,
                touched: true
            },
            birthday: {
                elementType: 'date',
                label: 'Date of birth',
                elementConfig: {
                    name: 'birthday',
                    placeholder: 'Choose date of birth',
                    maxDate: moment(),

                },
                value: moment(),
                validation: {},
                valid: true,
                touched: false
            },
            role: {
                elementType: 'select',
                label: 'Role',
                elementConfig: {
                    type: 'select',
                    name: 'role_id',
                    options: [

                    ],
                },
                value: '',
                validation: {},
                valid: true,
                touched: false
            },
            avatar: {
                elementType: 'inputFileWithPreview',
                elementConfig: {
                    type: 'file',
                    name: 'avatar',
                    data: ''
                },
                label: 'Avatar',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            }
        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchRolesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let roleArray = nextProps.roles.map(role => {
            return {
                value: role.id,
                displayValue: role.name
            }
        });

        stateClone.formElements.role.elementConfig.options = roleArray;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        if (inputIdentifier === 'birthday')
            updatedFormElement.value = event;
        else {
            updatedFormElement.value = event.target.value;
            // get image preview
            if (event.target.files && event.target.files[0]) {
                let reader = new FileReader();
                reader.onload = (e) => {
                    updatedFormElement.elementConfig.data = e.target.result;
                    const stateClone = { ...this.state };
                    updatedForm[inputIdentifier] = updatedFormElement;
                    stateClone.formElements = updatedForm;
                    this.setState(stateClone);

                }
                reader.readAsDataURL(event.target.files[0]);
            }
        }

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }

    handleSubmit = (event) => {
        event.preventDefault();
        const { formElements } = this.state;
        let formData = new FormData(event.target);
        formData.set('birthday', formElements.birthday.value.format("YYYY-MM-DD"));
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateUserRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            if (key === 'gender')
                stateClone.formElements[key].value = 1;
            else if (key === 'birthday')
                stateClone.formElements[key].value = moment();
            else if(key === 'avatar'){
                stateClone.formElements[key].value = ''
                stateClone.formElements[key].elementConfig.data = '';
            }
            else stateClone.formElements[key].value = '';

            if (_.indexOf(['name', 'email', 'password', 'password_confirmation', 'phone'], key) !== -1)
                stateClone.formElements[key].valid = false;

            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }

        stateClone.formIsValid = false;
        this.setState(stateClone);
    }



    render() {
        const { pageTitle, breadcrumbItems, formElements, formIsValid } = this.state;
        const { alert } = this.props;
        const formElementArrayLeft = [], formElementArrayRight = [];

        for (let key in formElements) {
            if (_.indexOf(['avatar', 'role'], key) !== -1) {
                formElementArrayRight.push({
                    id: key,
                    config: formElements[key]
                });
            } else {
                formElementArrayLeft.push({
                    id: key,
                    config: formElements[key]
                });
            }
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormTwoColumns
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArrayLeft={formElementArrayLeft}
                                formElementArrayRight={formElementArrayRight}
                                formIsValid={formIsValid}
                                alert={alert}
                                col1={8}
                                col2={4} />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        roles: state.role.list,
        alert: state.alert
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateUserRequest: data => { dispatch(createUserRequest(data)) },
        onFetchRolesRequest: () => { dispatch(fetchRolesRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(USER_CREATE)(UserAdd));