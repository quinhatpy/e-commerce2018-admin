import { Icon, Popconfirm, Select } from 'antd';
import moment from 'moment';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteWardRequest, fetchDistrictsRequest, fetchProvincesRequest, fetchWardsRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { WARD_CREATE, WARD_EDIT, WARD_LIST } from '../../../constants/link';
import { WARD_DELETE, WARD_INDEX, WARD_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';

class WardList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        provinceSelect: 'all',
        districtSelect: 'all',
        params: {},
        pageTitle: 'List Ward',
        breadcrumbItems: [
            {
                name: 'Ward management',
                link: WARD_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'type',
                label: 'Type',
                sortable: true
            },
            {
                name: 'district_id',
                label: 'District',
                sortable: true
            },
            {
                name: 'updated_at',
                label: 'Updated At',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            }
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchProvincesRequest();
        this.props.onFetchDistrictsRequest();
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
            this.setState({
                provinceSelect: 'all',
                districtSelect: 'all'
            })
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let province = nextProps.provinces.find(province => {
                return province.id === parseInt(queryParam.province_id);
            });

            this.setState({ provinceSelect: province ? province.id : 'all' })

            let district = nextProps.districts.find(district => {
                return district.id === parseInt(queryParam.district_id);
            });

            this.setState({ districtSelect: district ? district.id : 'all' })
        }
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.setState(stateClone);
        this.props.onFetchWardsRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: WARD_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnChangeSelectProvince = (value) => {
        let params = { ...this.state.params };
        params.province_id = value;
        if (value === 'all')
            delete params.province_id;

        delete params.district_id;

        let queryParam = queryString.stringify(params);


        this.props.history.push({
            pathname: WARD_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnChangeSelectDistrict = (value) => {
        let params = { ...this.state.params };
        params.district_id = value;
        if (value === 'all')
            delete params.district_id

        let queryParam = queryString.stringify(params);

        this.props.history.push({
            pathname: WARD_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderSelect(data) {
        let out = [];
        const Option = Select.Option;
        out.push(<Option key="0" value="all">Show all</Option>)

        for (let i in data) {
            out.push(<Option value={data[i].id} key={i}>{data[i].name}</Option>);
        }
        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteWardRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.type}</td>
                    <td>{item.district ? item.district.name : null}</td>
                    <td>{moment(item.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                WARD_UPDATE,
                                userAuth,
                                <Link to={WARD_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                WARD_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this ward?"
                                    onConfirm={() => onDeleteWardRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            provinceSelect,
            districtSelect,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            provinces,
            districts,
            wards
        } = this.props;

        const queryParam = queryString.parse(this.props.location.search);

        let districtsFilter = districts;
        if (queryParam.province_id)
            districtsFilter = districts.filter(district => {
                return district.province.id === parseInt(queryParam.province_id);
            })

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="row">
                                    <div className="col-md-12">
                                        <Link to={WARD_CREATE} className="btn btn-primary" style={{ margin: '5px' }}>Add Ward</Link>
                                    </div>
                                </div>
                                <div className="box-header">
                                    <h3 className="box-title">

                                    </h3>
                                    <div className="box-tools">
                                        <b>Province: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a province"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelectProvince}
                                            value={provinceSelect}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(provinces)}
                                        </Select>
                                        <b> District: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a district"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelectDistrict}
                                            value={districtSelect}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(districtsFilter)}
                                        </Select>
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(wards)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={WARD_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        provinces: state.province.list,
        districts: state.district.list,
        wards: state.ward.list,
        totalRows: state.ward.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: (offset, limit, params) => { dispatch(fetchDistrictsRequest(offset, limit, params)) },
        onFetchWardsRequest: (offset, limit, params) => { dispatch(fetchWardsRequest(offset, limit, params)) },
        onDeleteWardRequest: (id, offset, limit) => { dispatch(deleteWardRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(WARD_INDEX)(WardList));