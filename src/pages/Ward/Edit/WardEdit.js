import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchDistrictsRequest, fetchProvincesRequest, fetchWardRequest, updateWardRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { WARD_LIST } from '../../../constants/link';
import { WARD_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class WardEdit extends Component {
    state = {
        pageTitle: 'Edit Ward',
        breadcrumbItems: [
            {
                name: 'Ward management',
                link: WARD_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            type: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'type',
                    placeholder: 'Enter type'
                },
                label: 'Type',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            province: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'Province',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    name: 'district_id',
                    options: []
                },
                label: 'District',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const wardId = this.props.match.params.id;
        this.props.onFetchWardRequest(wardId);
        this.props.onFetchProvincesRequest();
        this.props.onFetchDistrictsRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataProvince = [], dataDistrict = [];
        const { provinces, item, districts } = nextProps;

        for (let province of provinces) {
            dataProvince.push({
                value: province.id,
                displayValue: province.name
            });
        }

        if (item) {
            stateClone.formElements.name.value = item.name;
            stateClone.formElements.type.value = item.type;
            if (item.district) {
                stateClone.formElements.district.value = item.district.id;

                const districtCurrent = districts.find(district => {
                    return district.id === item.district.id
                });

                if (districtCurrent) {
                    stateClone.formElements.province.value = districtCurrent.province.id;

                    for (let district of districts) {
                        if (district.province.id ===  districtCurrent.province.id)
                            dataDistrict.push({
                                value: district.id,
                                displayValue: district.name
                            });
                    }
                }

            }
        }

        stateClone.formElements.province.elementConfig.options = dataProvince;
        stateClone.formElements.district.elementConfig.options = dataDistrict;

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        // get district
        if (inputIdentifier === 'province') {
            const { districts } = this.props;
            let dataDistrict = [];
            for (let district of districts) {
                if (district.province.id === parseInt(updatedFormElement.value))
                    dataDistrict.push({
                        value: district.id,
                        displayValue: district.name
                    });
            }
            updatedForm.district.elementConfig.options = dataDistrict;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateWardRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        const wardId = this.props.match.params.id;
        this.props.onFetchWardRequest(wardId);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.ward.item,
        alert: state.alert,
        provinces: state.province.list,
        districts: state.district.list,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateWardRequest: (id, data) => { dispatch(updateWardRequest(id, data)) },
        onFetchWardRequest: id => { dispatch(fetchWardRequest(id)) },
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: () => { dispatch(fetchDistrictsRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(WARD_UPDATE)(WardEdit));