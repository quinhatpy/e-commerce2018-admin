import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createWardRequest, fetchDistrictsRequest, fetchProvincesRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { WARD_LIST } from '../../../constants/link';
import { WARD_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';


class WardAdd extends Component {
    state = {
        pageTitle: 'Add Ward',
        breadcrumbItems: [
            {
                name: 'Ward management',
                link: WARD_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            type: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'type',
                    placeholder: 'Enter type'
                },
                label: 'Type',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            province: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'Province',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    name: 'district_id',
                    options: []
                },
                label: 'District',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchProvincesRequest();
        this.props.onFetchDistrictsRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataProvince = [], dataDistrict = [];

        for (let province of nextProps.provinces) {
            dataProvince.push({
                value: province.id,
                displayValue: province.name
            });
        }

        for (let district of nextProps.districts) {
            if (district.province.id === dataProvince[0].value)
                dataDistrict.push({
                    value: district.id,
                    displayValue: district.name
                });
        }

        stateClone.formElements.province.elementConfig.options = dataProvince;
        stateClone.formElements.district.elementConfig.options = dataDistrict;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        // get district
        if(inputIdentifier === 'province') {
            const { districts } = this.props;
            let dataDistrict = [];
            for (let district of districts) {
                if (district.province.id === parseInt(updatedFormElement.value))
                    dataDistrict.push({
                        value: district.id,
                        displayValue: district.name
                    });
            }
            updatedForm.district.elementConfig.options = dataDistrict;
        }


        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);
    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateWardRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            if(key !== 'province' || key !== 'district')
                stateClone.formElements[key].valid = false;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = false;
        this.setState(stateClone);
        this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: this.props.provinces[0].id});
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert,
        provinces: state.province.list,
        districts: state.district.list,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateWardRequest: data => { dispatch(createWardRequest(data)) },
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: () => { dispatch(fetchDistrictsRequest()) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(WARD_CREATE)(WardAdd));