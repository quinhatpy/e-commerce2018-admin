import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { fetchArticleTotalRequest, fetchCustomerTotalRequest, fetchOrderTotalRequest, fetchProductTotalRequest } from '../../actions';
import ContentHeader from '../../components/ContentHeader';
import * as linkConfig from '../../constants/link';

class Dashboard extends Component {
    state = {
        pageTitle: 'Dashboard E-commerce 2018',
        breadcrumbItems: [
            {
                name: 'Dashboard'
            }
        ]
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        this.props.fetchCustomerTotalRequest();
        this.props.fetchProductTotalRequest();
        this.props.fetchOrderTotalRequest();
        this.props.fetchArticleTotalRequest();
    }

    render() {
        const { pageTitle, breadcrumbItems } = this.state;
        const {
            orderCount,
            productCount,
            customerCount,
            articleCount
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                {/* Main content */}
                <section className="content">
                    {/* Small boxes (Stat box) */}
                    <div className="row">
                        <div className="col-lg-3 col-xs-6">
                            {/* small box */}
                            <div className="small-box bg-aqua">
                                <div className="inner">
                                    <h3>{orderCount}</h3>
                                    <p>Orders</p>
                                </div>
                                <div className="icon">
                                    <i className="ion ion-bag" />
                                </div>
                                <Link to={linkConfig.ORDER_LIST} className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></Link>
                            </div>
                        </div>
                        {/* ./col */}
                        <div className="col-lg-3 col-xs-6">
                            {/* small box */}
                            <div className="small-box bg-green">
                                <div className="inner">
                                <h3>{customerCount}</h3>
                                    <p>Customers</p>
                                </div>
                                <div className="icon">
                                    <i className="ion ion-stats-bars" />
                                </div>
                                <Link to={linkConfig.CUSTOMER_LIST} className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></Link>
                            </div>
                        </div>
                        {/* ./col */}
                        <div className="col-lg-3 col-xs-6">
                            {/* small box */}
                            <div className="small-box bg-yellow">
                                <div className="inner">
                                <h3>{productCount}</h3>
                                    <p>Products</p>
                                </div>
                                <div className="icon">
                                    <i className="ion ion-person-add" />
                                </div>
                                <Link to={linkConfig.PRODUCT_LIST} className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></Link>
                            </div>
                        </div>
                        {/* ./col */}
                        <div className="col-lg-3 col-xs-6">
                            {/* small box */}
                            <div className="small-box bg-red">
                                <div className="inner">
                                <h3>{articleCount}</h3>
                                    <p>Articles</p>
                                </div>
                                <div className="icon">
                                    <i className="ion ion-pie-graph" />
                                </div>
                                <Link to={linkConfig.ARTICLE_LIST} className="small-box-footer">More info <i className="fa fa-arrow-circle-right" /></Link>
                            </div>
                        </div>
                        {/* ./col */}
                    </div>
                    {/* /.row */}
                </section>
                {/* /.content */}
            </Fragment>
        )
    }
}


const mapStateToProps = state => {
    return {
        orderCount: state.order.count,
        productCount: state.product.count,
        customerCount: state.customer.count,
        articleCount: state.article.count
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchCustomerTotalRequest: () => { dispatch(fetchCustomerTotalRequest()) },
        fetchProductTotalRequest: () => { dispatch(fetchProductTotalRequest()) },
        fetchOrderTotalRequest: () => { dispatch(fetchOrderTotalRequest()) },
        fetchArticleTotalRequest: () => { dispatch(fetchArticleTotalRequest()) }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);