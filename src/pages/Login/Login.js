import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import LoadingBar from 'react-redux-loading-bar';
import { Link, Redirect } from 'react-router-dom';
import * as linkConfig from '../../constants/link';
import { loginRequest } from './../../actions';
import Button from './../../components/UI/Button';
import LoginForm from './../../components/UI/LoginForm';
import checkValidity from './../../utils/checkValidity';

class Login extends Component {
    state = {
        pageTitle: 'Login E-commerce 2018',
        loginForm: {
            email: {
                elementType: 'email',
                elementConfig: {
                    name: 'name',
                    placeholder: 'Your Email'
                },
                label: 'Email',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                errorMessage: ''
            },
            password: {
                elementType: 'password',
                elementConfig: {
                    name: 'password',
                    placeholder: 'Your Password'
                },
                label: 'Password',
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false,
                errorMessage: ''
            },
            // remember: {
            //     elementType: 'checkbox',
            //     value: false,
            //     validation: {},
            //     valid: true
            // }
        },
        formIsValid: false,
        loading: false,
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
    }


    inputChangedHandler = (event, inputIdentifier) => {
        const updatedLoginForm = {
            ...this.state.loginForm
        };
        const updatedFormElement = {
            ...updatedLoginForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

        let checked = checkValidity(updatedLoginForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;
        updatedFormElement.touched = true;
        updatedLoginForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedLoginForm) {
            formIsValid = updatedLoginForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({ loginForm: updatedLoginForm, formIsValid: formIsValid });
    }

    loginHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.loginForm) {
            formData[formElementIdentifier] = this.state.loginForm[formElementIdentifier].value;
        }
        this.props.onLoginRequest(formData);
    }

    redirectToTarget = (to) => {
        to.hash = "login"
        // this.props.history.push(to)
        return <Redirect to={to} />

    }

    render() {
        const { isAuthenticated } = this.props;
        const { from } = this.props.location.state || { from: { pathname: '/' } }

        const formElementsArray = [];
        for (let key in this.state.loginForm) {
            formElementsArray.push({
                id: key,
                config: this.state.loginForm[key]
            });
        }

        let form = (
            <form onSubmit={this.loginHandler}>
                {formElementsArray.map(formElement => (
                    <LoginForm
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}
                        errorMessage={formElement.config.errorMessage}
                    />
                ))}

                <Button
                    type="submit"
                    disabled={!this.state.formIsValid}
                    classes="btn btn-primary btn-block btn-flat">
                    Sign In
                </Button>
            </form>
        );

        return (
            <Fragment>
                <LoadingBar style={{ backgroundColor: '#f39c12 ', height: '5px', zIndex: 99999999 }} />
                <div className="login-box ">
                    <div className="login-logo">
                        <Link to={linkConfig.LOGIN}><b>E-Commerce </b> 2018</Link>
                        <h2 className="text-center">Login</h2>
                    </div>
                    {/* /.login-logo */}
                    <div className="login-box-body">
                        {form}
                        {(isAuthenticated === true) ? this.redirectToTarget(from) : null}

                        {/* /.social-auth-links */}
                        <Link to={linkConfig.FORGOT_PASSWORD}>I forgot my password</Link><br />

                    </div>
                    {/* /.login-box-body */}
                </div>
            </Fragment>
        )
    }
}

Login.propTypes = {
    onLoginRequest: PropTypes.func.isRequired
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        onLoginRequest: (user) => { dispatch(loginRequest(user)) },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);