import PropTypes from 'prop-types';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import LoadingBar from 'react-redux-loading-bar';
import { Link, Redirect } from 'react-router-dom';
import * as linkConfig from '../../constants/link';
import { forgotPasswordRequest } from './../../actions';
import Button from './../../components/UI/Button';
import LoginForm from './../../components/UI/LoginForm';
import checkValidity from './../../utils/checkValidity';

class ForgotPassword extends Component {
    state = {
        pageTitle: 'Forgot Password E-commerce 2018',
        loginForm: {
            email: {
                elementType: 'email',
                elementConfig: {
                    name: 'name',
                    placeholder: 'Enter email to reset password'
                },
                label: 'Email',
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false,
                errorMessage: ''
            }
        },
        formIsValid: false,
        loading: false,
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
    }


    inputChangedHandler = (event, inputIdentifier) => {
        const updatedLoginForm = {
            ...this.state.loginForm
        };
        const updatedFormElement = {
            ...updatedLoginForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;

        let checked = checkValidity(updatedLoginForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;
        updatedFormElement.touched = true;
        updatedLoginForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedLoginForm) {
            formIsValid = updatedLoginForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({ loginForm: updatedLoginForm, formIsValid: formIsValid });
    }

    submitHandler = (event) => {
        event.preventDefault();
        const formData = {};
        for (let formElementIdentifier in this.state.loginForm) {
            formData[formElementIdentifier] = this.state.loginForm[formElementIdentifier].value;
        }
        this.props.forgotPasswordRequest(formData);
    }

    redirectToTarget = (to) => {
        to.hash = "login"
        // this.props.history.push(to)
        return <Redirect to={to} />

    }

    render() {
        const { isAuthenticated } = this.props;
        const { from } = this.props.location.state || { from: { pathname: '/' } }

        const formElementsArray = [];
        for (let key in this.state.loginForm) {
            formElementsArray.push({
                id: key,
                config: this.state.loginForm[key]
            });
        }

        let form = (
            <form onSubmit={this.submitHandler}>
                {formElementsArray.map(formElement => (
                    <LoginForm
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}
                        errorMessage={formElement.config.errorMessage}
                    />
                ))}

                <Button
                    type="submit"
                    disabled={!this.state.formIsValid}
                    classes="btn btn-primary btn-block btn-flat">
                    Reset
                </Button>
            </form>
        );

        return (
            <Fragment>
                <LoadingBar style={{ backgroundColor: '#f39c12 ', height: '5px', zIndex: 99999999 }} />
                <div className="login-box ">
                    <div className="login-logo">
                        <Link to={linkConfig.LOGIN}><b>E-Commerce </b> 2018</Link>
                        <h2 className="text-center">Forgot Password</h2>
                    </div>
                    {/* /.login-logo */}
                    <div className="login-box-body">
                        {form}
                        {(isAuthenticated === true) ? this.redirectToTarget(from) : null}

                        {/* /.social-auth-links */}
                        <Link to={linkConfig.LOGIN}>I want to login</Link><br />

                    </div>
                    {/* /.login-box-body */}
                </div>
            </Fragment>
        )
    }
}

ForgotPassword.propTypes = {
    forgotPasswordRequest: PropTypes.func.isRequired
}
const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        forgotPasswordRequest: user => { dispatch(forgotPasswordRequest(user)) },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);