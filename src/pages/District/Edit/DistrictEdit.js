import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchDistrictRequest, fetchProvincesRequest, updateDistrictRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { DISTRICT_LIST } from '../../../constants/link';
import { DISTRICT_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class DistrictEdit extends Component {
    state = {
        pageTitle: 'Edit District',
        breadcrumbItems: [
            {
                name: 'District management',
                link: DISTRICT_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            type: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'type',
                    placeholder: 'Enter type'
                },
                label: 'Type',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            province: {
                elementType: 'select',
                elementConfig: {
                    name: 'province_id',
                    options: []
                },
                label: 'Province',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const districtId = this.props.match.params.id;
        this.props.onFetchDistrictRequest(districtId);
        this.props.onFetchProvincesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        const { provinces, item } = nextProps;
       
        let data = [];
        for(let province of provinces) {
            data.push({
                value: province.id,
                displayValue: province.name
            });
        }

        if (item) {
            stateClone.formElements.name.value = item.name;
            stateClone.formElements.type.value = item.type;
            stateClone.formElements.province.value = item.province ? item.province.id : 1;
        }

        stateClone.formElements.province.elementConfig.options = data;

        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateDistrictRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        const districtId = this.props.match.params.id;
        this.props.onFetchDistrictRequest(districtId);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.district.item,
        alert: state.alert,
        provinces: state.province.list,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateDistrictRequest: (id, data) => { dispatch(updateDistrictRequest(id, data)) },
        onFetchDistrictRequest: id => { dispatch(fetchDistrictRequest(id)) },
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(DISTRICT_UPDATE)(DistrictEdit));