import { Icon, Popconfirm, Select } from 'antd';
import moment from 'moment';
import queryString from 'query-string';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteDistrictRequest, fetchDistrictsRequest, fetchProvincesRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { DISTRICT_CREATE, DISTRICT_EDIT, DISTRICT_LIST } from '../../../constants/link';
import { DISTRICT_DELETE, DISTRICT_INDEX, DISTRICT_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkPermission from '../../../utils/checkPermission';
import scrollTop from '../../../utils/scrollTop';

class DistrictList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        provinceSelect: 'all',
        params: {},
        pageTitle: 'List District',
        breadcrumbItems: [
            {
                name: 'District management',
                link: DISTRICT_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'type',
                label: 'Type',
                sortable: true
            },
            {
                name: 'province_id',
                label: 'Province',
                sortable: true
            },
            {
                name: 'updated_at',
                label: 'Updated At',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            }
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        this.props.onFetchProvincesRequest();
        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
            this.setState({ provinceSelect: 'all' })
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let province = nextProps.provinces.find(province => {
                return province.id === parseInt(queryParam.province_id);
            });

            this.setState({ provinceSelect: province ? province.id : 'all' })
        }
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.setState(stateClone);
        this.props.onFetchDistrictsRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: DISTRICT_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }


    handleOnChangeSelect = (value) => {
        let params = { ...this.state.params };
        params.province_id = value;
        if (value === 'all')
            delete params.province_id

        let queryParam = queryString.stringify(params);

        this.props.history.push({
            pathname: DISTRICT_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderSelect(provinces) {
        let out = [];
        const Option = Select.Option;
        out.push(<Option key="0" value="all">Show all</Option>)

        for (let i in provinces) {
            out.push(<Option value={provinces[i].id} key={i}>{provinces[i].name}</Option>);
        }
        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteDistrictRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.type}</td>
                    <td>{item.province ? item.province.name : null}</td>
                    <td>{moment(item.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                DISTRICT_UPDATE,
                                userAuth,
                                <Link to={DISTRICT_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                DISTRICT_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this district?"
                                    onConfirm={() => onDeleteDistrictRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            provinceSelect,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            provinces,
            districts
        } = this.props;



        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="row">
                                    <div className="col-md-12">
                                        <Link to={DISTRICT_CREATE} className="btn btn-primary" style={{ margin: '5px' }}>Add District</Link>
                                    </div>
                                </div>
                                <div className="box-header">
                                    <h3 className="box-title">

                                    </h3>
                                    <div className="box-tools">
                                        <b>Province: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a province"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelect}
                                            value={provinceSelect}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(provinces)}
                                        </Select>
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(districts)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={DISTRICT_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        districts: state.district.list,
        provinces: state.province.list,
        totalRows: state.district.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: (offset, limit, params) => { dispatch(fetchDistrictsRequest(offset, limit, params)) },
        onDeleteDistrictRequest: (id, offset, limit) => { dispatch(deleteDistrictRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(DISTRICT_INDEX)(DistrictList));