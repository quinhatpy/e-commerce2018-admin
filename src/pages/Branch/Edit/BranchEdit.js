import isEmpty from 'lodash/isEmpty';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBranchRequest, fetchDistrictRequest, fetchDistrictsRequest, fetchProvincesRequest, fetchWardRequest, fetchWardsRequest, setBranch, setDistrict, setDistrictList, setWard, setWardList, updateBranchRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { BRANCH_LIST } from '../../../constants/link';
import { BRANCH_UPDATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';

class BranchEdit extends Component {
    state = {
        pageTitle: 'Edit Branch',
        breadcrumbItems: [
            {
                name: 'Branch management',
                link: BRANCH_LIST
            },
            {
                name: 'Edit'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            address: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'address',
                    placeholder: 'Enter address'
                },
                label: 'Address',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: true,
                touched: false
            },
            province: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'Province',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'District',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            ward: {
                elementType: 'select',
                elementConfig: {
                    name: 'ward_id',
                    options: []
                },
                label: 'Ward',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: true,

    }

    componentDidMount() {
        const branchId = this.props.match.params.id;
        this.props.onFetchBranchRequest(branchId);
        this.props.onFetchProvincesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataProvince = [], dataDistrict = [], dataWard = [];
        const { provinces, item, districts, district, wards, ward } = nextProps;
        
        if (!isEmpty(item) && isEmpty(ward))
            this.props.onFetchWardRequest(item.ward.id);

        if (!isEmpty(ward) && isEmpty(wards))
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: ward.district.id});
       
        if (!isEmpty(district) && isEmpty(districts))
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: district.province.id});

        if(this.props.districts !== nextProps.districts && stateClone.formElements.district.value === 'clicked') {
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: nextProps.districts[0].id});
        }

        if(this.props.districts !== nextProps.districts) {
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: nextProps.districts[0].id});
        }
        for (let province of provinces) {
            dataProvince.push({
                value: province.id,
                displayValue: province.name
            });
        }

        for (let district of districts) {
            dataDistrict.push({
                value: district.id,
                displayValue: district.name
            });
        }

        for (let ward of wards) {
            dataWard.push({
                value: ward.id,
                displayValue: ward.name
            });
        }

        if (item) {
            stateClone.formElements.name.value = item.name;
            stateClone.formElements.address.value = item.address;
            if (item.ward) {
                stateClone.formElements.ward.value = item.ward.id;
                
            }
        }

        stateClone.formElements.province.elementConfig.options = dataProvince;
        stateClone.formElements.district.elementConfig.options = dataDistrict;
        stateClone.formElements.ward.elementConfig.options = dataWard;

        if (!isEmpty(wards) && isEmpty(stateClone.formElements.district.value) && this.props.wards !== wards) {
            stateClone.formElements.district.value = wards[0].district.id;

            this.props.onFetchDistrictRequest(wards[0].district.id);
        }

        if (!isEmpty(districts) && isEmpty(stateClone.formElements.province.value)) {
            stateClone.formElements.province.value = districts[0].province.id;
        }

        this.setState(stateClone);
    }

    componentWillUnmount() {
        this.props.onResetDistrictList();
        this.props.onResetDistrict();
        this.props.onResetWardList();
        this.props.onResetWard();
        this.props.onResetBranch();
        const stateClone = { ...this.state };
        stateClone.formElements.province.value = '';
        stateClone.formElements.province.elementConfig.options = [];
        stateClone.formElements.district.value = '';
        stateClone.formElements.district.elementConfig.options = [];
        stateClone.formElements.ward.value = '';
        stateClone.formElements.ward.elementConfig.options = [];
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        // get district
        if (inputIdentifier === 'province') {
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: updatedFormElement.value});
            updatedForm.district.value = 'clicked';
        }

        if (inputIdentifier === 'district') {
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: updatedFormElement.value});
        }

        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);

    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        formData.set('_method', 'PUT');
        this.props.onUpdateBranchRequest(this.props.match.params.id, formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            stateClone.formElements[key].valid = true;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = true;
        this.setState(stateClone);

        this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: this.props.provinces[0].id});
        const branchId = this.props.match.params.id;
        this.props.onFetchBranchRequest(branchId);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }
      
        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        item: state.branch.item,
        alert: state.alert,
        provinces: state.province.list,
        districts: state.district.list,
        district: state.district.item,
        wards: state.ward.list,
        ward: state.ward.item
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateBranchRequest: (id, data) => { dispatch(updateBranchRequest(id, data)) },
        onFetchBranchRequest: id => { dispatch(fetchBranchRequest(id)) },
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: (offset, limit, params) => { dispatch(fetchDistrictsRequest(offset, limit, params)) },
        onFetchWardsRequest: (offset, limit, params) => { dispatch(fetchWardsRequest(offset, limit, params)) },
        onFetchWardRequest: id => { dispatch(fetchWardRequest(id)) },
        onFetchDistrictRequest: id => { dispatch(fetchDistrictRequest(id)) },
        onResetDistrictList: () => dispatch(setDistrictList({districts: [], totalRows: 0})),
        onResetDistrict: () => dispatch(setDistrict({})),
        onResetWardList: () => dispatch(setWardList({wards: [], totalRows: 0})),
        onResetWard: () => dispatch(setWard({})),
        onResetBranch: () => dispatch(setBranch({})),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BRANCH_UPDATE)(BranchEdit));