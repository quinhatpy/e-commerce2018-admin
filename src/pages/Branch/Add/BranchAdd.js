import isEmpty from 'lodash/isEmpty';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { createBranchRequest, fetchDistrictsRequest, fetchProvincesRequest, fetchWardsRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import FormBasic from '../../../components/Form/Basic/FormBasic';
import { BRANCH_LIST } from '../../../constants/link';
import { BRANCH_CREATE } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import checkValidity from '../../../utils/checkValidity';


class BranchAdd extends Component {
    state = {
        pageTitle: 'Add Branch',
        breadcrumbItems: [
            {
                name: 'Branch management',
                link: BRANCH_LIST
            },
            {
                name: 'Add new'
            }
        ],
        formElements: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'name',
                    placeholder: 'Enter name'
                },
                label: 'Name',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            address: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    name: 'address',
                    placeholder: 'Enter address'
                },
                label: 'Address',
                value: '',
                validation: {
                    required: true,
                },
                errorMessage: '',
                valid: false,
                touched: false
            },
            province: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'Province',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            district: {
                elementType: 'select',
                elementConfig: {
                    options: []
                },
                label: 'District',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
            ward: {
                elementType: 'select',
                elementConfig: {
                    name: 'ward_id',
                    options: []
                },
                label: 'Ward',
                value: '',
                validation: {},
                valid: true,
                touched: false,
            },
        },

        formIsValid: false,

    }

    componentDidMount() {
        this.props.onFetchProvincesRequest();
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        const stateClone = { ...this.state };
        let dataProvince = [], dataDistrict = [], dataWard = [];
        const { provinces, districts } =nextProps;
        
        if (!isEmpty(provinces) && isEmpty(stateClone.formElements.district.elementConfig.options)){
         
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: provinces[0].id});
        }

        if (!isEmpty(districts) && isEmpty(stateClone.formElements.ward.elementConfig.options))
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: districts[0].id});

        if(this.props.districts !== nextProps.districts) {
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: nextProps.districts[0].id});
        }

        for (let province of nextProps.provinces) {
            dataProvince.push({
                value: province.id,
                displayValue: province.name
            });
        }

        for (let district of nextProps.districts) {
            dataDistrict.push({
                value: district.id,
                displayValue: district.name
            });
        }

        for (let ward of nextProps.wards) {
            dataWard.push({
                value: ward.id,
                displayValue: ward.name
            });
        }

        stateClone.formElements.province.elementConfig.options = dataProvince;
        stateClone.formElements.district.elementConfig.options = dataDistrict;
        stateClone.formElements.ward.elementConfig.options = dataWard;
        this.setState(stateClone);
    }

    handleInputChange = (event, inputIdentifier) => {
        const updatedForm = {
            ...this.state.formElements
        };
        const updatedFormElement = {
            ...updatedForm[inputIdentifier]
        };

        updatedFormElement.value = event.target.value;

        // validate
        let checked = checkValidity(updatedForm, inputIdentifier);

        updatedFormElement.valid = checked.isValid;
        updatedFormElement.errorMessage = checked.errorMessage;

        updatedFormElement.touched = true;
        updatedForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedForm) {
            formIsValid = updatedForm[inputIdentifier].valid && formIsValid;
        }

        // get district
        if (inputIdentifier === 'province') {
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: updatedFormElement.value});
        }

        if (inputIdentifier === 'district') {
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, {district_id: updatedFormElement.value});
        }


        const stateClone = { ...this.state };
        stateClone.formElements = updatedForm;
        stateClone.formIsValid = formIsValid;

        this.setState(stateClone);
    }


    handleSubmit = (event) => {
        event.preventDefault();
        let formData = new FormData(event.target);
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }
        this.props.onCreateBranchRequest(formData);
    }

    handleReset = (event) => {
        event.preventDefault();
        const stateClone = { ...this.state };
        for (let key in stateClone.formElements) {
            stateClone.formElements[key].value = '';
            if(key !== 'province' && key !== 'district' && key !== 'ward')
                stateClone.formElements[key].valid = false;
            stateClone.formElements[key].touched = false;
            stateClone.formElements[key].errorMessage = '';
        }
        stateClone.formIsValid = false;
        this.setState(stateClone);

        this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, {province_id: this.props.provinces[0].id});
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            formElements,
            formIsValid
        } = this.state;
        const { alert } = this.props;
        const formElementArray = [];

        for (let key in formElements) {
            formElementArray.push({
                id: key,
                config: formElements[key]
            });
        }

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />

                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <FormBasic
                                handleSubmit={(event) => this.handleSubmit(event)}
                                handleReset={(event) => this.handleReset(event)}
                                handleInputChange={(event, inputIdentifier) => this.handleInputChange(event, inputIdentifier)}
                                formElementArray={formElementArray}
                                formIsValid={formIsValid}
                                alert={alert}
                                col={6}
                                colOffset="col-md-offset-3" />
                        </div>
                    </div>
                </section>
            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        alert: state.alert,
        provinces: state.province.list,
        districts: state.district.list,
        wards: state.ward.list,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onCreateBranchRequest: data => { dispatch(createBranchRequest(data)) },
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: (offset, limit, params) => { dispatch(fetchDistrictsRequest(offset, limit, params)) },
        onFetchWardsRequest: (offset, limit, params) => { dispatch(fetchWardsRequest(offset, limit, params)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BRANCH_CREATE)(BranchAdd));