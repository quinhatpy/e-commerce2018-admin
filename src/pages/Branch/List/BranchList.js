import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { Select, Popconfirm, Icon } from 'antd';
import { BRANCH_LIST, BRANCH_CREATE, BRANCH_EDIT, BRANCH_DETAIL } from '../../../constants/link';
import { BRANCH_INDEX, BRANCH_SHOW, BRANCH_UPDATE, BRANCH_DELETE } from '../../../constants/permission';
import { fetchBranchesRequest, deleteBranchRequest, fetchWardsRequest, fetchDistrictsRequest, fetchProvincesRequest } from '../../../actions/actions';
import scrollTop from '../../../utils/scrollTop';
import checkPermission from '../../../utils/checkPermission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import DataTable from '../../../components/DataTable/DataTable';
import Pagination from '../../../components/Pagination/Pagination';

class BranchList extends Component {
    state = {
        currentPage: 1,
        limit: 10,
        offset: 0,
        provinceSelected: 'all',
        districtSelected: 'all',
        wardSelected: 'all',
        params: {},
        pageTitle: 'List Branch',
        breadcrumbItems: [
            {
                name: 'Branch management',
                link: BRANCH_LIST
            },
            {
                name: 'List'
            }
        ],
        tableHeadColumns: [
            {
                name: 'id',
                label: '#',
                sortable: true
            },
            {
                name: 'name',
                label: 'Name',
                sortable: true
            },
            {
                name: 'address',
                label: 'Address',
                sortable: true
            },
            {
                name: 'ward_id',
                label: 'Ward',
                sortable: true
            },
            {
                name: 'action',
                label: 'Action',
                sortable: false
            }
        ],
        sortBy: '+id'
    }

    componentDidMount() {
        const queryParam = queryString.parse(this.props.location.search);
        this.props.onFetchProvincesRequest();
        if (queryParam.province_id)
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, { province_id: queryParam.province_id });
        if (queryParam.district_id)
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, { district_id: queryParam.district_id });

        this.handlePageChange(parseInt(this.props.match.params.page) || 1, this.props.location.search);
        document.title = this.state.pageTitle;
    }

    componentWillReceiveProps(nextProps) {
        if ((this.state.currentPage !== 1 && !nextProps.match.params.page) || (this.props.location.search !== nextProps.location.search)) {
            this.handlePageChange(1, nextProps.location.search);
            this.setState({
                provinceSelected: 'all',
                districtSelected: 'all',
                wardSelected: 'all'
            });
        }

        if (nextProps.history.location.search) {
            let queryParam = queryString.parse(nextProps.history.location.search);

            let province = nextProps.provinces.find(province => {
                return province.id === parseInt(queryParam.province_id);
            });

            let district = nextProps.districts.find(district => {
                return district.id === parseInt(queryParam.district_id);
            });

            let ward = nextProps.wards.find(ward => {
                return ward.id === parseInt(queryParam.ward_id);
            });

            this.setState({
                wardSelected: ward ? ward.id : 'all',
                districtSelected: district ? district.id : 'all',
                provinceSelected: province ? province.id : 'all'
            })
        }
    }

    handlePageChange(pageNumber, queryParams = '') {
        let stateClone = { ...this.state };
        stateClone.currentPage = pageNumber;
        stateClone.params = queryString.parse(queryParams);

        let offset = (pageNumber - 1) * stateClone.limit;
        stateClone.offset = offset;

        this.setState(stateClone);
        this.props.onFetchBranchesRequest(offset, stateClone.limit, stateClone.params);
        scrollTop();
    }

    handleOnSort = (sort) => {
        let stateClone = { ...this.state };
        stateClone.params.sort_by = sort;
        let queryParam = queryString.stringify(stateClone.params);

        this.setState(stateClone);
        this.props.history.push({
            pathname: BRANCH_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    handleOnChangeSelectProvince = (value) => {
        let params = { ...this.state.params };
        params.province_id = value;
        if (value === 'all')
            delete params.province_id;

        delete params.district_id;
        delete params.ward_id;

        let queryParam = queryString.stringify(params);


        this.props.history.push({
            pathname: BRANCH_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
        if (params.province_id)
            this.props.onFetchDistrictsRequest(0, Math.pow(2, 31) - 1, { province_id: params.province_id });
    }

    handleOnChangeSelectDistrict = (value) => {
        let params = { ...this.state.params };
        params.district_id = value;
        if (value === 'all')
            delete params.district_id;

        delete params.ward_id;

        let queryParam = queryString.stringify(params);


        this.props.history.push({
            pathname: BRANCH_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
        if (params.district_id)
            this.props.onFetchWardsRequest(0, Math.pow(2, 31) - 1, { district_id: params.district_id })
    }

    handleOnChangeSelectWard = (value) => {
        let params = { ...this.state.params };
        params.ward_id = value;
        if (value === 'all')
            delete params.ward_id

        let queryParam = queryString.stringify(params);

        this.props.history.push({
            pathname: BRANCH_LIST,
            search: '?' + queryParam
        });
        this.handlePageChange(1, queryParam);
    }

    renderSelect(data) {
        let out = [];
        const Option = Select.Option;
        out.push(<Option key="0" value="all">Show all</Option>)

        for (let i in data) {
            out.push(<Option value={data[i].id} key={i}>{data[i].name}</Option>);
        }

        return out;
    }

    renderList(data) {
        const { offset, limit } = this.state;
        const { userAuth, onDeleteBranchRequest } = this.props;

        return data.length === 0 ? <tr><td colSpan="8" align="center">No data</td></tr>
            : data.map((item, index) => {

                return <tr key={index}>
                    <td>{offset + index + 1}</td>
                    <td>{item.name}</td>
                    <td>{item.address}</td>
                    <td>{item.ward ? item.ward.name : null}</td>
                    <td align="center">
                        <div className="btn-group" role="group" aria-label="Basic example">
                            {checkPermission(
                                BRANCH_SHOW,
                                userAuth,
                                <Link to={BRANCH_DETAIL + item.id} type="button" className="btn btn-primary" title="view"><i className="fal fa-eye" /></Link>
                            )}
                            {checkPermission(
                                BRANCH_UPDATE,
                                userAuth,
                                <Link to={BRANCH_EDIT + item.id} type="button" className="btn btn-warning" title="edit"><i className="fal fa-edit " /></Link>
                            )}
                            {checkPermission(
                                BRANCH_DELETE,
                                userAuth,
                                <Popconfirm
                                    placement="topRight"
                                    title="Are you sure to delete this branch?"
                                    onConfirm={() => onDeleteBranchRequest(item.id, offset, limit)}
                                    okText="Yes"
                                    cancelText="No"
                                    okType="danger"
                                    icon={<Icon type="question-circle" />} >
                                    <button type="button" className="btn btn-danger" title="delete"><i className="fal fa-trash-alt" /></button>
                                </Popconfirm>
                            )}
                        </div>
                    </td>
                </tr>
            })
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
            provinceSelected,
            districtSelected,
            wardSelected,
            params,
            limit,
            currentPage,
            offset,
            tableHeadColumns,
            sortBy
        } = this.state;
        const {
            totalRows,
            provinces,
            districts,
            wards,
            branches
        } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">
                                <div className="row">
                                    <div className="col-md-12">
                                        <Link to={BRANCH_CREATE} className="btn btn-primary" style={{ margin: '5px' }}>Add Branch</Link>
                                    </div>
                                </div>
                                <div className="box-header">
                                    <h3 className="box-title">

                                    </h3>
                                    <div className="box-tools">
                                        <b>Province: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a province"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelectProvince}
                                            value={provinceSelected}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(provinces)}
                                        </Select>
                                        <b> District: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a district"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelectDistrict}
                                            value={districtSelected}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(districts)}
                                        </Select>
                                        <b> Ward: </b><Select
                                            showSearch
                                            style={{ width: 300 }}
                                            placeholder="Select a district"
                                            optionFilterProp="children"
                                            onChange={this.handleOnChangeSelectWard}
                                            value={wardSelected}
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                        >
                                            {this.renderSelect(wards)}
                                        </Select>
                                    </div>

                                </div>
                                <div className="box-body table-responsive no-padding">
                                    <Overlay />
                                    <DataTable
                                        tableHeadColumns={tableHeadColumns}
                                        sortBy={sortBy}
                                        renderList={() => this.renderList(branches)}
                                        onSort={(sort) => this.handleOnSort(sort)} />
                                </div>
                                <div className="box-footer clearfix">
                                    <Pagination
                                        totalRecord={totalRows}
                                        limit={limit}
                                        currentPage={currentPage}
                                        offset={offset}
                                        link={BRANCH_LIST}
                                        queryParams={params}
                                        onPageChange={(pageNumber, params) => this.handlePageChange(pageNumber, params)} />
                                </div>

                            </div>
                        </div>
                    </div>

                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        userAuth: state.auth.user,
        provinces: state.province.list,
        districts: state.district.list,
        wards: state.ward.list,
        branches: state.branch.list,
        totalRows: state.branch.totalRows
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onFetchProvincesRequest: () => { dispatch(fetchProvincesRequest()) },
        onFetchDistrictsRequest: (offset, limit, params) => { dispatch(fetchDistrictsRequest(offset, limit, params)) },
        onFetchWardsRequest: (offset, limit, params) => { dispatch(fetchWardsRequest(offset, limit, params)) },
        onFetchBranchesRequest: (offset, limit, params) => { dispatch(fetchBranchesRequest(offset, limit, params)) },
        onDeleteBranchRequest: (id, offset, limit) => { dispatch(deleteBranchRequest(id, offset, limit)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BRANCH_INDEX)(BranchList));