import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { fetchBranchRequest } from '../../../actions/actions';
import ContentHeader from '../../../components/ContentHeader/ContentHeader';
import Overlay from '../../../components/UI/Overlay/Overlay';
import { BRANCH_LIST } from '../../../constants/link';
import { BRANCH_SHOW } from '../../../constants/permission';
import CheckPermission from '../../../hoc/CheckPermission/CheckPermission';

class BranchDetail extends Component {
    state = {
        pageTitle: 'Branch Detail',
        breadcrumbItems: [
            {
                name: 'Branch management',
                link: BRANCH_LIST
            },
            {
                name: 'Detail'
            }
        ],
    }

    componentDidMount() {
        document.title = this.state.pageTitle;
        const {
            match,
            fetchBranchRequest
        } = this.props;

        fetchBranchRequest(match.params.id);
    }

    render() {
        const {
            pageTitle,
            breadcrumbItems,
        } = this.state;
        const { branch } = this.props;

        return (
            <Fragment>
                <ContentHeader pageTitle={pageTitle} breadcrumbItems={breadcrumbItems} />
                <section className="content">
                    <div className="row">
                        <div className="col-xs-12">
                            <div className="box">

                                <div className="box-body no-padding">
                                    <Overlay />
                                    <div className="row">
                                        <div className="col-md-8 col-md-offset-2">
                                            <table className="table table-hover table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>{branch.name}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Address</th>
                                                        <td>{branch.address}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Ward</th>
                                                        <td>{branch.ward ? branch.ward.type + ' ' + branch.ward.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>District</th>
                                                        <td>{branch.ward ? branch.ward.district.type + ' ' + branch.ward.district.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Province</th>
                                                        <td>{branch.ward ? branch.ward.district.province.type + ' ' + branch.ward.district.province.name : null}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Create At</th>
                                                        <td>{moment(branch.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Update At</th>
                                                        <td>{moment(branch.updated_at, 'YYYY-MM-DD HH:mm:ss').format('DD-MM-YYYY HH:mm:ss')}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </Fragment >
        )
    }
}

const mapStateToProps = state => {
    return {
        branch: state.branch.item,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        fetchBranchRequest: id => { dispatch(fetchBranchRequest(id)) },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckPermission(BRANCH_SHOW)(BranchDetail));